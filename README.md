﻿IEE1667
=======
 
Progetto universitario che si pone come obbiettivo la progettazione e la realizzazione di un simulatore per lo standard IEEE 1667.
Nella cartella documentazione sarà possibile trovare il documento dei requisiti, il documento di design e il documento di test.

Implementazione
---------------

Per utilizzare l'implementazione è necessario creare un database (chiamato ieee1667) di tipo mysql.
Viene fornito un dump di partenza contenente alcuni dati (`implementazione/db_dump.mysql`).

Per poter modificare le impostazioni è necessario modificare i seguenti files:

* Impostazioni database: `implementazione/src/hibernate.cfg.xml`
* Impostazioni logger: `implementazione/src/logback.xml`. 
  In particolare per abilitare il log sulla console è necessario decommentare la riga `<appender-ref ref="STDOUT" />`


Test
---------------
I test sono contenuti nei package relativi alla classe testata. In particolare i test dell'interazione con l'utente
sono nel package `ieee1667.gui`, mentre i test scenario sono nel package `ieee1667`
