\subsection{Lo standard IEEE 1667}
\label{subsec:standard}

In questa sezione si vogliono riportare tutte le caratteristiche principali del protocollo IEEE 1667 per poter sviluppare poi, nelle sezioni successive, le scelte effettuate e i requisiti del simulatore.

\begin{figure}[htbp]
	\centering
	\includegraphics[width=\columnwidth]{immagini/IEEE1667_general}
	\caption{Schema generale IEEE1667}
	\label{fig:ieee1667_generale}
\end{figure}  

Come è possibile vedere in figura \ref{fig:ieee1667_generale} il protocollo si basa sul layer fisico e di trasporto che variano a secondo della tipologia di host e di dispositivo che si sta prendendo in considerazione.\\
Al di sopra di tali layer, nel TSD, troviamo delle unità logiche chiamate ACT. Utilizzando i comandi del protocollo, l'host è in grado di ottenere una rappresentazione degli ACT presenti sul dispositivo ed essere in grado quindi di conoscere tutte le caratteristiche rilevanti che lo contraddistinguono (funzionalità, criptosistemi supportati \dots).\\
Andando più nello specifico un ACT è composto da un insieme di Silo. Un Silo è una unità che può ricevere dei comandi ed è raggiungibile tramite un indice univoco all'interno dell'ACT. Un silo dovrebbe contenere un proprio processore e uno spazio di memoria privato. Le funzionalità di ogni Silo sono comunicate all'host tramite un identificatore univoco (STID) che viene assegnato dal Working Group dell'IEEE 1667 o da entità esterne (in questo caso si parla di External Silo).

Una delle prime operazioni che viene svolta dopo la connessione tra host e TSD è quindi quella di Discovery. In tale fase l'host ottiene, per ogni ACT presente sul dispositivo, le informazioni sui Silo contenuti utilizzando il comando Probe inviato ad un Silo adibito a tale scopo chiamato Probe Silo. Successivamente host e TSD effettueranno una autenticazione reciproca. Questa è la fase cruciale e di maggior importanza per il protocollo in quanto permette di raggiungere lo scopo che lo standard si prefissa. Le funzionalità di autenticazione sono fornite, sul TSD, da una tipologia di Silo chiamata Authentication Silo.

Vengono ora analizzati più nel dettaglio tutte le componenti che fanno parte del protocollo, i loro scopi e quali sono i requisiti che devono soddisfare.

\subsubsection{Addressable Command Target}
Come specificato in precedenza l'ACT è una unità logica presente sul dispositivo che contiene delle funzionalità, implementate tramite l'uso dei Silo. Il protocollo non specifica il criterio con cui i silo devono essere raggruppati all'interno di diversi ACT. Esso, tuttavia, definisce alcune caratteristiche e alcuni requisiti che ogni ACT deve rispettare:
\begin{itemize}
	\item  Ogni silo all'interno di un ACT deve essere indirizzabile tramite un indice numerico chiamato Silo Index
	\item Ogni ACT contiene uno e un solo Probe Silo
	\item Tutti i TSD devono contenere almeno un ACT con un Authentication Silo. E' possibile che vi siano ulteriori ACT contenenti Silo di questo tipo
	\item Ogni ACT può contenere zero o più External Silo
	\item Ogni Silo deve essere raggiungibile in uno e uno solo ACT
\end{itemize}

\subsubsection{Probe}
Il comando Probe è quel meccanismo che permette agli host di scoprire i Silo implementati in ciascun ACT del dispositivo. Tale comando è implementato dal Probe Silo, il quale deve essere accessibile tramite l'indice 0 di ogni ACT.\\
Il processo di Probing sostanzialmente consiste nell'interrogare, tramite il comando Probe, ogni ACT del dispositivo per poterne determinare le caratteristiche, le funzionalità e ovviamente l'esistenza del Silo di autenticazione. L'host, quando invia il comando Probe, comunica anche informazioni sulle proprie caratteristiche. In particolare viene comunicato il tipo di host e la versione del protocollo IEEE 1667 implementata. Se il comando Probe ha successo l'host ottiene una lista di STID il cui ordine deve rispettare gli indici che verranno poi essere utilizzati per accedere alle funzionalità dei singoli Silo. Se il comando fallisce, invece, l'host ottiene una lista contenente un solo elemento che rappresenta l'STID del Probe Silo.

L'esecuzione del comando Probe è richiesta prima di effettuare qualsiasi operazione sul dispositivo. Se un comando, diverso dal Probe, è inviato al TSD prima di tale passaggio il TSD risponderà con un messaggio di errore.

\subsubsection{Autenticazione}
Come detto in precedenza l'autenticazione è il cuore dello standard IEEE 1667. Tale funzionalità viene implementata dal Certificate Authentication Silo e si basa su algoritmi di cifratura asimmetrica. 

Prima di analizzare nel dettaglio quali sono i requisiti del Silo vediamo quali sono i certificati che vengono coinvolti nel processo di autenticazione. E' importante notare che non sempre abbiamo a che fare con certificati singoli. In alcuni casi è possibile trovare delle catene di certificati. Questo meccanismo permette di creare una serie di certificati al fine di aumentare la sicurezza ottenuta.
\begin{description}

 	\item[Manufacturer Chain, ASCm] è una catena di certificati definita dal costruttore del dispositivo. Questo insieme di certificati deve essere immutabile ad ogni evento che può avvenire sul dispositivo (eg: hard reset)
 	
 	\item[Provisioning Certificate, PCp] è un certificato che viene utilizzato per gestire e amministrare il Silo di autenticazione. L'autenticazione tramite tale certificato è richiesta per effettuare operazioni come la modifica di altri certificati presenti nel Silo
 	
 	\item[Authentication Silo Certificate, ASCh] Questo certificato viene utilizzato per autenticare un dispositivo, o meglio un Authentication Silo, ad un host
 	
 	\item[Host Certificate, HCh] Questo certificato viene utilizzato per autenticare un utente ad un dispositivo, o meglio ad un Authentication Silo

\end{description}

I certificati appena descritti vengono memorizzati in un'area apposita del silo di autenticazione (chiamata Authentication Silo Certificate Store, ASCS). I certificati vengono memorizzati, acceduti e eliminati utilizzando indici zero-based tramite i comandi definiti dallo standard. Vi sono delle posizioni predefinite per alcuni tipi di certificati. In particolare il certificato del costruttore deve essere memorizzato nella posizione zero, mentre il certificato di Provisioning nella posizione uno. E' importante sottolineare che lo standard non definisce come lo store debba essere implementato purchè vengano rispettate le caratteristiche sino ad ora riportate.

Come detto in precedenza il processo di autenticazione si basa su algoritmi a chiave asimmetrica. L'Authentication Silo deve memorizzare la coppia di chiavi utilizzata in tale processo. Per quanto riguarda la chiave pubblica, essa viene memorizzata nell'ASCm mentre la chiave privata è memorizzata nel Silo e non deve essere accedibile dall'esterno. Lo standard non definisce come tale chiave debba essere memorizzata. \\
In un processo di autenticazione, oltre alle chiavi, è fondamentale la scelta dell'algoritmo crittografico utilizzato. Lo standard IEEE 1667 non indica quali algoritmi i TSD e gli host debbano implementare, ne come tali algoritmi debbano essere scelti. Esso si limita a specificare 
che l'Authentication Silo deve fornire almeno un algoritmo crittografico a chiave asimmetrica.
\\
\\
Vediamo ora più nel dettaglio gli stati che può assumere un Authentication Silo e, attraverso di essi, i passaggi che portano ad autenticare un host. E' importante sottolineare che un Authentication Silo può assumere un singolo stato alla volta e che qualsiasi operazione di reset come lo spegnimento del dispositivo o una reinizializzazione portano il Silo nello stato Initialized.


\begin{figure}[htbp]
	\centering
	\includegraphics[width=\columnwidth]{immagini/auth_state}
	\caption{Diagramma degli stati di un authentication silo}
	\label{fig:ieee1667_auth_state}
\end{figure}  

Come si evince dalla figura \ref{fig:ieee1667_auth_state} abbiamo sostanzialmente 5 stati.

\begin{description}

	\item[Initialized] Questo è uno stato transitorio che serve al Silo a porsi in uno degli altri stati a secondo di determinate condizioni. In particolare nel caso in cui  non sia presente alcun certificato di provisioning il Silo passa in uno stato Not provisioned. Nel caso in cui tale certificato sia presente si hanno due possibilità. Se è presente un Host certificate il Silo passa nello stato Not Authenticated in attesa di autenticazione. Al contario se non è presente un Host Certificate il Silo transisce nello stato Authenticated.\\
	Quando il Silo si trova nello stato Initialized non accetta alcun tipo di comando.
	
	\item[Not Provisioned] Questo stato rappresenta il fatto che il Silo non possiede un PCp (certificato di provisioning). L'unico comando accettato in questo stato è quello che permette di impostare il certificato in questione. Quando questo avviene il Silo passa direttamente nello stato Authenticated in modo tale da evitare operazioni di reset
	
	\item[Not Authenticated] In questo stato il Silo è in attesa di una autenticazione dell'host. E' inoltre possibile che l'host autentichi il Silo oppure che venga richiesta una autenticazione di tipo amministrativo
	
	\item[Authenticated] In questo stato il Silo ha autenticato l'host. Possono essere inviati i comandi o l'host può autenticare il Silo
	
	\item[Failed] Questo stato viene raggiunto da qualsiasi altro stato nel caso in cui qualche comando segnali un errore e implica l'impossibilità di eseguire qualsiasi comando. L'unico modo per uscire da tale stato è quello di un reset. 
	

\end{description}




