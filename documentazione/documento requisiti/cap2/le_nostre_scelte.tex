\subsection{Le scelte rispetto al protocollo}
\label{sec:scelte}
Come è stato evidenziato nella sezione \ref{subsec:standard} lo standard IEEE 1667 fornisce una descrizione generica del protocollo. Vi sono una serie di problemi aperti e di decisioni da prendere per poter utilizzare realmente tale protocollo. In questa sezione si vuole descrivere le scelte effettuate dal gruppo di lavoro in merito a questi ambiti.\\
La sezione è divisa in sottosezioni, ognuna contente uno specifico tema.

\subsubsection{Autenticazione TSD}
Identificatore: {\bf Sc1}.\\
Come si è visto una delle operazioni fondamentali per la creazione di trust tra dispositivo e host è la verifica dell'autenticità del TSD da parte dell'host stesso. Questo è possibile tramite il certificato ASCm che è un certificato immutabile e inserito dal costruttore nel dispositivo. L'operazione di autenticazione del TSD è infatti basato sullo schema delle firme digitali a certificati pubblici. Il dispositivo deve quindi contenere una coppia di chiavi, chiamata ASKP, che viene generata tramite un algoritmo RSA. La chiave pubblica viene inclusa nel certificato ASCm mentre quella privata viene memorizzata nel dispositivo e non è accessibile all'esterno.\\
Vediamo ora nel dettaglio il processo di autenticazione del TSD:

\begin{enumerate}

	\item L'host richiede al dispositivo il certificato ASCm. Questo certificato si trova nella posizione zero dello store dell'Authentication Silo (vedi \ref{subsec:standard})
	
	\item L'host richiede al TSD di autenticarsi
	\item Il dispositivo a questo punto crea un messaggio contenente un valore randomico. Il dispositivo, inoltre, firma tale messaggio con la propria chiave privata e invia queste informazioni all'host
	
	\item L'host, utilizzando la chiave pubblica contenuta nel certificato ASCm, stabilisce l'autenticità del dispositivo. Nel caso in cui questa operazione non desse risultato positivo l'host annullerebbe l'operazione di autenticazione del dispositivo.

\end{enumerate}

In questo scambio di passaggi viene sotto inteso quale algoritmo viene utilizzato per firmare il valore numerico. Come riportato nel capitolo delle assunzioni, si assume che vi sia un singolo algoritmo per questo tipo di operazione e sia host che il dispositivo lo utilizzino.


\subsubsection{Autenticazione Authentication Silo}
Identificatore: {\bf Sc2}.\\
L'host ha la necessita di autenticare, oltre al dispositivo stesso, anche l'Authentication Silo. Questa operazione è fondamentale e deve essere eseguita prima che l'host tenti di autenticarsi sul dispositivo. 
La verifica dell'autenticità dell'Authentication Silo si basa sull'uso di un certificato, presente sul dispositivo, chiamato ASCh (Authentication Silo Certificate), che viene ricavato dalla coppia di chiavi ASKP. Questo certificato contiene, in modo simile a quanto avviene per l'ASCm, la chiave pubblica della coppia di chiavi ASKP. La chiave privata, come visto in precedenza, viene memorizzata sul dispositivo e non può essere acceduta dall'esterno.\\
Vediamo ora nel dettaglio i passaggi che portano all'autenticazione dell'Authentication Silo:

\begin{enumerate}

	\item L'host richiede al dispositivo il certificato ASCh
	\item L'host richiede all'Authentication Silo di autenticarsi
	\item L'authentication Silo crea un messaggio contenente un valore randomico. Esso, inoltre, firma tale messaggio con la propria chiave privata e invia tali informazioni all'host
	
	\item L'host verifica, tramite la chiave pubblica contenuta nel certificato ASCh, l'autenticità del Silo. Nel caso in cui l'host non sia in grado di verificare l'autenticità della firma annullerebbe l'operazione di autenticazione dell'Authentication Silo.

\end{enumerate}

Anche in questo caso l'algoritmo di crittografazione utilizzato viene lasciato sottointeso. Come riportato nelle assunzioni si assume che vi sia un unico algoritmo e che sia l'host che il dispositivo lo utilizzino.

\subsubsection{Scelta Metodo crittografico}
\label{subsub:scelta_metodo}
Identificatore: {\bf Sc3}.\\
Come si è visto sia i dispositivi sia gli host possono supportare diversi metodi di crittografia. A causa di ciò il caso in cui l'insieme degli algoritmi supportati sia dal dispositivo che dall'host contenga diversi metodi è piuttosto diffuso. Nasce quindi l'esigenza di dover gestire la scelta dell'algoritmo da utilizzare. Lo standard IEEE 1667 non fa chiarezza su come l'algoritmo crittografico per la firma dei messaggi debba essere deciso e su come host e dispositivo debbano mettersi d'accordo. Uno degli scopi principali del simulatore è proprio quello di permettere all'utente di influenzare le scelte in questo ambito e permettere di testare diversi algoritmi. In particolare si è deciso di introdurre il concetto di politica di scelta: l'utente ha la facoltà di specificare con che criterio l'algoritmo debba essere scelto.\\
Prima di presentare più nel dettaglio come le politiche sono composte viene presentato il modello che è stato scelto per la rappresentazione e definizione dei metodi (Identificatore: {\bf Sc3.1}). Si è deciso di rappresentare ogni tipologia di algoritmo con un identificatore univoco, di cui sia gli host che i dispositivi sono a conoscenza. Ogni algoritmo è definito da una serie di caratteristiche ed ad ognuna di queste viene associato un valore numerico che
va da 0 a 10. Tanto più il valore di un attributo è elevato tanto più il metodo corrispondente è qualitativamente positivo per quella specifica specifica misura di valutazione. 
In particolare le caratteristiche che abbiamo preso in considerazione sono le seguenti:

\begin{itemize}
	\item Complessità computazionale, una misura della complessità e della potenza computazionale richiesta dagli algoritmi usati dal metodo valutato.
	\item Spazio occupato, una misura della quantità di memoria necessaria ed utilizzata dai processi del metodo valutato.
	\item Robustezza, una misura della sicurezza o delle garanzie di sicurezza fornite dal metodo valutato. In questo parametro viene anche valutata
	la lunghezza della chiave privata. Dato uno stesso metodo, se si considerano due versioni con differenti lunghezze delle chiavi private, questo
	valore è uno di quelli che cambia.
	\item Lunghezza della chiave pubblica, una misura della lunghezza della chiave pubblica che viene generata, scelta la lunghezza della chiave privata.
	La lunghezza della chiave pubblica influenza le dimensioni del certificato.
\end{itemize}

Data la definizione di questo modello adottato per rappresentare i metodi, il criterio di decisione che si è scelto di adottare è quello di utilizzare una somma pesata di tutti gli attributi per un particolare metodo
(Identificatore: {\bf Sc3.2}). Il valore che si ottiene dalla somma pesata è quindi il valore che verrà considerato nel processo di decisione dei metodi. Il metodo con il valore risultante più alto corrisponde al metodo scelto.
In assenza di altri elementi, inizialmente il peso associato ad ogni attributo è uguale a $1$. In questo senso, le politiche sono state introdotte come strumento per influenzare il processo di
decisione modificando i diversi pesi. Il metodo scelto per implementare una politica è quello di definire quest'ultima come un elenco ordinato degli attributi dei metodi. L'utente può scegliere un sottoinsieme di
attributi e definire un elenco di questi. In modo tale che, se vengono scelti $n$ attributi, allora al primo elemento verrà dato un peso di $n$, al secondo un peso di $n - 1$ e così via fino all'ultimo
con un peso uguale a $1$. A tutti gli attributi che non sono presenti nella politica e quindi nell'elenco ordinato viene associato un peso di $0$ e per questo motivo vengono completamente ignorati nel
processo di decisione.
L'utente ha, come detto, la possibilità di specificare diverse politiche.  Ad esempio una politica del tipo

\begin{itemize}
	\item Robustezza 
	\item Spazio Occupato
\end{itemize}
 
 indica che l'host dovrà selezionare un algoritmo tenendo sopratutto in considerazione la robustezza e, in un secondo momento, lo spazio occupato. A Robustezza viene assegnato un peso di $2$, mentre allo
 spazio occupato viene assegnato un peso di $1$ e a tutti gli altri viene assegnato un peso $0$. Non viene specificata alcuna richiesta sulla complessità computazionale e sulla lunghezza delle chiavi.\\
 Al contrario una politica del tipo
 \begin{itemize}
	\item Robustezza
\end{itemize}
indica, sostanzialmente, che si richiede di utilizzare l'algoritmo più robusto possibile senza tenere conto degli aspetti di complessità computazionale,di memoria utilizzata e di lunghezza delle chiavi.

L'host non può ovviamente selezionare un algoritmo a propria scelta tra tutti quelli conosciuti ma deve tenere conto dell'insieme degli algoritmi che vengono supportati dal dispositivo. Nel caso estremo, e irrilevante ai fini della simulazione, in cui il dispositivo abbia un solo metodo crittografico le politiche sono, sostanzialmente, inutili.

Di seguito vediamo più nel dettaglio le operazioni che vengono effettuate per la scelta dell'algoritmo da utilizzare:
\begin{enumerate}
	\item L'host recupera tutti gli algoritmi supportati dal dispositivo dalla sua memoria interna. Tali algoritmi sono conosciuti tramite il comando Probe effettuate nel momento del discovery
	
	\item Per ogni algoritmo, l'host calcola un punteggio tenendo conto della politica di scelta indicata dall'utente e dalle valutazioni dell'algoritmo stesso
	
	\item L'host sceglie l'algoritmo con il punteggio più elevato
\end{enumerate}

Utilizzando questa strategia il simulatore permette all'utente di sperimentare diverse tipologie di politiche di scelta dell'algoritmo di crittografia.



\subsubsection{Autenticazione Utente}
Identificatore: {\bf Sc4}.\\
Uno dei punti cruciali del protocollo IEEE 1667 è l'autenticazione dell'utente sul dispositivo. Questo passaggio permette all'utente di andare ad effettuare operazioni di lettura e scrittura nelle aree di memoria in cui è autorizzato. L'autenticazione dell'utente si basa sull'esistenza di un certificato chiamato HCh (Host Certificate) che viene generato a partire da una chiave di cui solo l'utente è a conoscenza e inserito nel dispositivo, tramite gli opportuni meccanismi, da un amministratore. Tale certificato contiene una chiave pubblica.\\
Vediamo ora nel dettaglio le operazioni che portano all'autenticazione dell'utente sul dispositivo:

\begin{enumerate}
	
	\item L'host richiede all'utente di scegliere la politica per determinare quale algoritmo di crittografia utilizzare in fase di autenticazione 
	\item L'host determina l'algoritmo migliore per la scelta appena effettuata (scelta implementativa Sc3)
	\item L'host richiede tramite un opportuno comando la lista dei certificati HCh che l'authentication silo possiede
	\item L'host richiede all'utente la password segreta e l'id dell'utente
	\item L'host trova l'indice del certificato HCh
	\item L'host effettua una richiesta di autenticazione sul dispositivo passando come parametri:
			\begin{itemize}
				\item Un messaggio di autenticazione
				\item Il messaggio di autenticazione firmato con la chiave privata dell'utente (l'algoritmo da utilizzare per firmare il messaggio è stato stabilito in precedenza)
				\item L'indice che permette di reperire il certificato HCh dell'utente nel ASCS (Authentication Silo Certificate Store)
				\item L'algoritmo utilizzato per firmare il messaggio
			\end{itemize}				
	
			Il messaggio di autenticazione contiene:
	 		
	 		\begin{itemize}
	 			\item L'id utente
	 			\item Un valore numerico randomico
	 		\end{itemize}

	 \item Il dispositivo recupera dall' ASCS (Authentication Silo Certificate Store) il certificato dell'utente e ne preleva la chiave pubblica. Tramite essa è in grado di stabilire l'autenticità del messaggio ed effettuare quindi l'autenticazione. Nel caso in cui il dispositivo non sia in grado di verificare l'autenticità della firma fornita dall'host viene tornato un errore e l'Authentication Silo transisce in uno stato Not Authorized (vedi diagramma a stati sezione \ref{subsec:standard}).

\end{enumerate}


\subsubsection{Autenticazione Amministratore}
Identificatore: {\bf Sc5}.\\
Il protocollo IEEE 1667 prevede l'esistenza della figura dell'amministratore. Tale utente ha la possibilità di autenticarsi ed effettuare operazioni di modifica, rimozione e aggiunta dei certificati (fatta eccezione per certificati particolari come l'ASCm). L'autenticazione di tale utente si basa sull'esistenza di un particolare certificato: il PCp (Provisioning Certificate). Anche in questo caso l'algoritmo si basa sull'esistenza di due chiavi: quella pubblica è contenuta nel certificato, mentre quella privata è conosciuta dall'amministratore.\\
Di seguito vengono dettagliate le operazioni svolte per permettere ad un amministratore di autenticarsi su di un dispositivo:

\begin{enumerate}

	\item L'host richiede la password segreta all'amministratore
	\item L'host effettua una richiesta di autenticazione di tipo amministrativo sul dispositivo passando come parametri:
			\begin{itemize}
				\item Un messaggio di autenticazione
				\item Il messaggio di autenticazione firmato con la chiave privata dell'amministratore
			\end{itemize}				
	
			Il messaggio di autenticazione contiene un valore randomico.

	 \item Il dispositivo recupera dall' ASCS (Authentication Silo Certificate Store) il certificato PCp e ne preleva la chiave pubblica. Tramite essa è in grado di stabilire l'autenticità del messaggio ed effettuare quindi l'autenticazione. Nel caso in cui il dispositivo non sia in grado di verificare l'autenticità della firma fornita dall'host viene tornato un errore e l'Authentication Silo transisce in uno stato Not Authorized (vedi diagramma a stati sezione \ref{subsec:standard}).

\end{enumerate}



\subsubsection{Implementazione dell'ASCS, Authentication Silo Certificate Store}
Identificatore: {\bf Sc6}.\\
Uno degli elementi su cui lo standard IEEE 1667 non fa chiarezza è come l'ASCS debba essere implementato. Ricordiamo che questo elemento è sostanzialmente lo storage dell'Authentication Silo in cui vengono memorizzati i certificati. Si è scelto di implementare tale componente con una struttura a lista, ovviamente rispecchiando i requisiti che lo standard specifica (eg: certificato del costruttore nella posizione 0). Si è inoltre deciso di non applicare alcun metodo di protezione a questo store.

\subsubsection{Gestione certificati}
Identificatore: {\bf Sc7}.\\
Un amministratore, dopo essersi autenticato sul dispositivo, può effettuare operazioni sui certificati del dispositivo stesso andando ad aggiungerne di nuovi o ad eliminarne di esistenti.\\
L'operazione preliminare per ognuna delle operazioni relative alla gestione dei certificati è il controllo dell'autenticazione dell'amministratore. Questa operazione è svolta dal dispositivo basandosi sul proprio stato interno. Nel caso in cui l'amministratore risulti non autenticato il dispositivo genera un messaggio di errore e tutte le operazioni vengono interrotte. In caso contrario, invece, il flusso di operazioni viene svolto normalmente. \\
L'amministratore può, in particolare, svolgere tre operazioni che vengono di seguito descritte.

La prima operazione che l'amministratore può svolgere è la visualizzazione dei certificati presenti sul dispositivo:
\begin{enumerate}
	\item L'host richiede al dispositivo l'elenco dei certificati presenti 
	\item Il dispositivo verifica la richiesta e torna la lista dei certificati presenti
	\item L'host visualizza tali certificati all'amministratore
\end{enumerate}

La seconda operazione che l'amministratore può svolgere è l'aggiunta di un nuovo certificato utente (HCh). Questi sono i passaggi che portano ad eseguire questa operazione:
\begin{enumerate}
	\item L'host richiede alcuni dati per la generazione del nuovo certificato. In particolare viene richiesta la password segreta, l'id dell'utente a cui si vuole associare il certificato e la lista delle aree di memoria in cui potrà leggere e/o scrivere
	\item L'host genera un certificato. Si noti che questo passaggio è tipico dell'ambiente di simulazione ed è inserito come una facilitazione per l'utente. In casi reali, infatti, questa operazione non è compito dell'host ma di enti esterni
	
	\item L'host richiede al dispositivo di aggiungere il certificato al proprio store. Per fare ciò viene inviato un messaggio apposito contenente
		\begin{itemize}
			\item Il certificato dell'utente (che contiene anche la chiave pubblica generata)
			\item L'identificatore dell'utente
			\item La lista delle aree in cui l'utente può scrivere e quella in cui può leggere
		\end{itemize}
	\item Il dispositivo aggiunge il certificato al proprio store e invia come risposta alla richiesta di aggiunta l'index del certificato appena aggiunto 
\end{enumerate}

L'ultima operazione che può svolgere l'amministratore, infine, è la cancellazione di un certificato già esistente. Con questa operazione l'amministratore può impedire ad un utente, ad esempio, di autenticarsi sul dispositivo. Le operazioni che vengono svolte per portare a termine questo compito sono:
\begin{enumerate}
	\item L'host invia un messaggio di richiesta di cancellazione di un certificato specificando l'index del certificato da cancellare
	\item Il dispositivo verifica se la richiesta è lecita. Ad esempio l'amministratore non può richiedere la cancellazione del certificato in posizione 0 (che ricordiamo è l'ASCm). Nel caso in cui la richiesta sia ammissibile il certificato viene eliminato e viene tornata una risposta positiva, in caso contrario viene tornato un messaggio di errore
\end{enumerate}

\subsubsection{Trasmissione Dati, Lettura e Scrittura}
Identificatore: {\bf Sc8}.\\
Una delle principali operazioni che possiamo svolgere sui dispositivi è quella di leggere e scrivere le zone di memoria in cui l'utente è abilitato. Il protocollo IEEE 1667 non specifica come queste operazioni debbano essere implementate ne che metodi di crittografia applicare. Il simulatore sfrutta un approccio ibrido per la protezione della trasmissione dati: viene utilizzato un crittosistema asimmetrico (quindi basato su una coppia di chiavi, pubblica e privata) per condividere tra host e dispositivo una chiave segreta utilizzata poi, attraverso un algoritmo di crittografia simmetrica, per cifrare la trasmissione dei dati letti e scritti. Questa scelta è stata fatta in quanto la crittografia a chiave simmetrica è più sicura e meno computazionalmente onerosa rispetto a quella asimmetrica, tuttavia soffre del problema dello scambio delle chiavi. Questo problema viene appunto risolto utilizzando un metodo asimmetrico.\\
Si tenga presente che anche in questo caso il dispositivo TSD può avere a disposizione più metodi che possono essere utilizzati per la trasmissione dati, dove sostanzialmente i diversi metodi utilizzano
diversi crittosistemi simmetrici per la comunicazione sicura. Per questo motivo, anche in questo caso, è stato selezionato uno specifico criterio di decisione dei metodi ed introdotto un sistema a
politiche, equivalente a quello descritto precedentemente per i metodi di autenticazione. Anche in questo caso, il modello che è stato scelto per definire ogni metodo di trasmissione dei dati
è quello di definire un insieme di attributi, che ha le stesse caratteristiche di quello definito per i metodi di autenticazione e cambiano solamente gli attributi definiti (Identificatore: {\bf Sc8.1}), che sono:

\begin{itemize}
	\item Complessità computazionale, una misura della complessità e della potenza computazionale richiesta dagli algoritmi usati dal metodo valutato.
	\item Efficienza, una misura delle prestazioni e dell'efficienza computazionale richiesta. In questo caso, l'efficienza viene espressa a parte perché
	accade non di rado che alcuni crittosistemi simmetrici, nonostante abbiano un elevata complessità computazionale, in pratica dimostrino una buona efficienza
	e delle buone prestazioni.
	\item Spazio occupato, una misura della quantità di memoria necessaria ed utilizzata dai processi del metodo valutato.
	\item Robustezza, una misura della sicurezza o delle garanzie di sicurezza fornite dal metodo valutato. In questo parametro viene anche valutata
	la lunghezza della chiave privata. Dato uno stesso metodo, se si considerano due versioni con differenti lunghezze delle chiavi private, questo
	valore è uno di quelli che cambia.
\end{itemize}

Così come per il modello di rappresentazione anche il sistema delle politiche è identico a quello che viene definito per i metodi di autenticazione (Identificatore: {\bf Sc8.2}). 

Abbiamo quindi due fasi nella trasmissione dei dati:
\begin{itemize}
	\item In una prima fase host e dispositivo dialogano per scambiarsi la chiave condivisa simmetrica. In questa fase viene, come detto, utilizzato un algoritmo a chiave asimmetrica che si assume sia già conosciuto e stabilito da ambo le parti
	
	\item Viene utilizzata la chiave segreta condivisa per la trasmissione dei dati
\end{itemize}

Per una maggiore chiarezza di come questo avviene viene fornito uno schema dei passaggi principali che portano ad avere una chiave privata condivisa per un algoritmo simmetrico (vedi figura \ref{fig:schema_trasm}).

\begin{figure}[htbp]
	\centering
	\includegraphics[width=\columnwidth]{immagini/trasmissione_grafico}
	\caption{Schema di condivisione di una chiave segreta per la trasmissione tra host e TSD}
	\label{fig:schema_trasm}
\end{figure}  

Vediamo ora nel dettaglio le operazioni svolte. I primi passaggi sono comuni sia per quanto riguarda la lettura che per la scrittura e sono di seguito riportati:
\begin{enumerate}
	\item L'host richiede all'utente di scegliere la politica per determinare quale algoritmo simmetrico utilizzare per la crittografia della trasmissione
	\item L'host determina l'algoritmo di crittografia simmetrica migliore per la scelta appena effettuata (scelta implementativa Sc3)	
	\item L'host richiede al dispositivo una chiave pubblica	
	\item Il dispositivo genera una coppia chiave pubblica/privata e comunica all'host la chiave pubblica. In questo modo le due parti possono scambiarsi  la chiave da utilizzare nel crittosistema simmetrico
	\item L'host genera una chiave segreta per l'algoritmo scelto in precedenza e la comunica, criptandolo con la chiave pubblica ricevuta, al dispositivo
	\item Il dispositivo riceve il messaggio dell'host e lo decifra usando la chiave private generata in precedenza. In questo modo host e dispositivo condividono una chiave segreta con cui cifrare la trasmissione
	\item L'host invia una richiesta al dispositivo per ottenere le aree di memoria in cui può leggere o scrivere
	\item Il dispositivo verifica che l'utente sia autenticato tramite il proprio stato interno (vedi diagramma a stati sezione \ref{subsec:standard}). In caso affermativo viene tornata una lista di indici in cui l'utente può scrivere e una lista di indici in cui l'utente può leggere. In caso di problemi nella verifica dell'autenticazione viene tornato un errore.
\end{enumerate}

A questo punto le operazioni si diversificano a secondo se l'utente vuole effettuare una operazione di lettura o di scrittura. In caso di operazione di lettura abbiamo i seguenti passaggi:
\begin{enumerate}
	\item L'host visualizza all'utente le zone di memoria disponibili per la lettura
	\item L'utente seleziona le zone di memoria da leggere
	\item L'host invia al dispositivo una richiesta di lettura, specificando gli indice delle aree da leggere
	\item Il dispositivo verifica che l'utente sia autenticato e che abbia i permessi per leggere le aree di memoria specificate. In caso questo non sia possibile torna un errore. Se il controllo va a buon fine, altrimenti,  il dispositivo cifra con la chiave condivisa ottenuta in precedenza i dati appena letti e li invia all'host
	
	\item L'host visualizza all'utente il valore ottenuto
\end{enumerate}

In caso in cui si voglia effettuare un'operazione di scrittura, invece, abbiamo i seguenti passaggi:
\begin{enumerate}
	\item L'host visualizza all'utente le zone di memoria disponibili per la scrittura
	\item L'utente seleziona le aree di memoria in cui vuole scrivere e il valore da inserire in ognuna delle aree
	\item L'host invia al dispositivo una richiesta di scrittura specificando per ogni cella il valore desiderato. Il messaggio viene cifrato utilizzando la chiave condivisa in precedenza 
	
	\item Il dispositivo decifra il messaggio sfruttando la chiave condivisa in precedenza. A questo punto viene effettuato un controllo se l'utente è autenticato e se ha i permessi per scrivere nelle aree di memoria specificate. Se l'utente non ha i permessi viene tornato un errore, in caso contrario vengono modificate le aree di memoria richieste e tornato un messaggio di conferma
	
	\item l'host comunica all'utente l'esito dell'operazione
\end{enumerate}

\subsubsection{Modifiche e Sviluppi Futuri}

In questa ultima sezione si vuole dare uno sguardo generale a tutte le scelte, identificate da uno specifico nome, che sono state effettuate e per ognuna
si vuole definire quali sono le possibilità di modifiche future e quale è il loro impatto rispetto al sistema
complessivo. Tuttavia, si tenga presente che le scelte che riguardano i criteri di decisione per i metodi di autenticazione e per i metodi di
trasmissione e le corrispondenti politiche corrispondono alle scelte più forti che sono state adottate ed introdotte, infatti il documento di definizione
dello schema IEEE 1667 non dice nulla a riguardo. Inoltre, facendo attenzione a quelli che sono stati definiti come gli obiettivi di questo progetto, queste
scelte hanno un importanza fondamentale e il loro cambiamento o modifica futura è possibile e necessaria in futuro sempre per perseguire gli
obiettivi prefissati.

\paragraph{Sc1:}  In questa scelta si è seguito fedelmente il funzionamento dello schema a firme digitali e le caratteristiche dei certificati
ASCm definite da IEEE 1667. è possibile introdurre leggere modifiche e introdurre la gestione delle catene di certificati.

\paragraph{Sc2:}  In questa scelta si è seguito fedelmente il funzionamento dello schema a firme digitali e le caratteristiche dei certificati
ASCH definite da IEEE 1667. è possibile introdurre leggere modifiche e introdurre la gestione delle catene di certificati.

\paragraph{Sc3:}  Come detto in precedenza rappresenta una delle scelte più importanti, più forti e più influenti rispetto agli obiettivi che è
stata adottata. Questa scelta si suddivide in due sotto scelte.

\subparagraph{Sc3.1:} Viene definito il modello con cui viene rappresentato ogni singolo metodo di autenticazione. In questo caso è possibile
cambiare completamente il modello usato oppure modificare il modello già definito, ad esempio modificando gli insiemi di attributi scelti
per descrivere le caratteristiche e proprietà usati. Questa decisione impatta fortemente sul criterio di decisione dei metodi di autenticazione
usato nello schema di funzionamento.

\subparagraph{Sc3.2:} Viene introdotto e definito il concetto di politica. Anche in questo caso è possibile cambiare completamente il modello di
definizione delle politiche oppure modificare la funzione che determina il criterio di scelta che viene usato per calcolare i valori associati ai
metodi che, quindi, determinano la scelta effettuata.

\paragraph{Sc4:} In questa scelta si è seguito fedelmente il funzionamento dello schema a firme digitali e le caratteristiche dei certificati
degli utenti definite da IEEE 1667. è possibile introdurre leggere modifiche e introdurre la gestione delle catene di certificati.

\paragraph{Sc5:} In questa scelta si è seguito fedelmente il funzionamento dello schema a firme digitali e le caratteristiche dei certificati
PCp definite da IEEE 1667. è possibile introdurre leggere modifiche e introdurre la gestione delle catene di certificati.

\paragraph{Sc6:} In questo caso si è scelto di implementare l'ASCS in un modo ben preciso e si è assunto di non utilizzare nessun meccanismo di
protezione. è possibile modificare la scelta di implementazione ed introdurre un meccanismo di protezione.

\paragraph{Sc7:} In questo caso si è descritto un sistema base di gestione dei certificati, si è descritta una struttura molto semplice per i certificati
e si sono definiti i compiti e le capacità dell'amministratore. è quindi possibile modificare quelle che sono le capacità dell'amministratore, ad esempio
aggiungendo un sistema di diffusione delle chiavi pubbliche o dei certificati in modo tale che l'amministratore non debba conoscere la chiave segreta
di un utente generico per creare un certificato, ed è possibile modificare e complicare la struttura dei certificati.

\paragraph{Sc8:} Come detto in precedenza rappresenta una delle scelte più importanti, più forti e più influenti rispetto agli obiettivi che è
stata adottata. Questa scelta si suddivide in due sotto scelte.

\subparagraph{Sc8.1:} Viene definito il modello con cui viene rappresentato ogni singolo metodo di trasmissione. In questo caso è possibile
cambiare completamente il modello usato oppure modificare il modello già definito, ad esempio modificando gli insiemi di attributi scelti
per descrivere le caratteristiche e proprietà usati. Questa decisione impatta fortemente sul criterio di decisione dei metodi di autenticazione
usato nello schema di funzionamento.

\subparagraph{Sc8.2:} Viene introdotto e definito il concetto di politica. Anche in questo caso è possibile cambiare completamente il modello di
definizione delle politiche oppure modificare la funzione che determina il criterio di scelta che viene usato per calcolare i valori associati ai
metodi che, quindi, determinano la scelta effettuata.
