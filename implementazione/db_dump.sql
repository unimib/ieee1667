-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generato il: Mag 18, 2013 alle 12:23
-- Versione del server: 5.5.27
-- Versione PHP: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ieee1667`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `Certificate`
--

CREATE TABLE IF NOT EXISTS `Certificate` (
  `DTYPE` varchar(31) NOT NULL,
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `userID` varchar(255) DEFAULT NULL,
  `publicKey_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKD7C8E9779810B1C0` (`publicKey_id`),
  KEY `FKD7C8E9772989F23E` (`publicKey_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dump dei dati per la tabella `Certificate`
--

INSERT INTO `Certificate` (`DTYPE`, `id`, `userID`, `publicKey_id`) VALUES
('ASCh', 13, NULL, 1),
('ASCm', 14, NULL, 1),
('PCp', 15, NULL, NULL),
('HCh', 17, 'host1', 3);

-- --------------------------------------------------------

--
-- Struttura della tabella `CryptographicMethod`
--

CREATE TABLE IF NOT EXISTS `CryptographicMethod` (
  `DTYPE` varchar(31) NOT NULL,
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `computationalComplexity` int(11) DEFAULT NULL,
  `memoryComplexity` int(11) DEFAULT NULL,
  `publicKeyLenght` int(11) DEFAULT NULL,
  `strength` int(11) DEFAULT NULL,
  `efficiency` int(11) DEFAULT NULL,
  `keyLenght` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=297 ;

--
-- Dump dei dati per la tabella `CryptographicMethod`
--

INSERT INTO `CryptographicMethod` (`DTYPE`, `id`, `name`, `computationalComplexity`, `memoryComplexity`, `publicKeyLenght`, `strength`, `efficiency`, `keyLenght`) VALUES
('AuthenticationMethod', 5, 'SHA1WITHRSA', 3, 8, 7, 2, NULL, NULL),
('AuthenticationMethod', 9, 'RMD128WITHRSA', 4, 0, 4, 10, NULL, NULL),
('AuthenticationMethod', 11, 'RIPEMD160withRSA/ISO9796-2', 7, 6, 1, 10, NULL, NULL),
('AuthenticationMethod', 12, 'SHA512WITHRSA', 7, 0, 8, 6, NULL, NULL),
('AuthenticationMethod', 16, 'MD4WITHRSA', 0, 7, 6, 5, NULL, NULL),
('AuthenticationMethod', 17, 'MD5withRSA/ISO9796-2', 5, 10, 6, 9, NULL, NULL),
('AuthenticationMethod', 20, 'RMD256WITHRSA', 4, 1, 7, 9, NULL, NULL),
('AuthenticationMethod', 21, 'RMD160WITHRSA', 0, 3, 7, 9, NULL, NULL),
('AuthenticationMethod', 23, 'RIPEMD128WITHRSA', 8, 8, 7, 3, NULL, NULL),
('AuthenticationMethod', 24, 'SHA1withRSA/ISO9796-2', 9, 6, 1, 9, NULL, NULL),
('AuthenticationMethod', 25, 'SHA224withRSA/PSS', 6, 5, 10, 4, NULL, NULL),
('AuthenticationMethod', 30, 'SHA256WITHRSA', 10, 2, 5, 1, NULL, NULL),
('AuthenticationMethod', 32, 'SHA256withRSA/PSS', 2, 6, 3, 5, NULL, NULL),
('AuthenticationMethod', 37, 'SHA384withRSA/PSS', 0, 7, 6, 1, NULL, NULL),
('AuthenticationMethod', 38, 'RIPEMD256WITHRSA', 1, 1, 2, 1, NULL, NULL),
('AuthenticationMethod', 39, 'RIPEMD160WITHRSA', 3, 0, 4, 10, NULL, NULL),
('AuthenticationMethod', 40, 'SHA1withRSA/PSS', 1, 0, 5, 1, NULL, NULL),
('AuthenticationMethod', 41, 'MD2WITHRSA', 5, 9, 3, 5, NULL, NULL),
('AuthenticationMethod', 44, 'SHA512withRSA/PSS', 1, 8, 2, 4, NULL, NULL),
('AuthenticationMethod', 46, 'SHA384WITHRSA', 7, 10, 2, 7, NULL, NULL),
('AuthenticationMethod', 53, 'MD5WITHRSA', 3, 10, 7, 1, NULL, NULL),
('TransmissionMethod', 203, 'PBEWITHMD5AND128BITAES-CBC-OPENSSL', 7, NULL, NULL, 8, 9, 3),
('TransmissionMethod', 202, 'PBEWITHSHA-256AND192BITAES-CBC-BC', 8, NULL, NULL, 10, 8, 9),
('TransmissionMethod', 201, 'AES', 6, NULL, NULL, 6, 10, 0),
('TransmissionMethod', 200, 'PBEWITHSHAAND256BITAES-CBC-BC', 6, NULL, NULL, 6, 0, 9),
('TransmissionMethod', 199, 'RIJNDAEL', 3, NULL, NULL, 4, 1, 5),
('TransmissionMethod', 198, 'PBEWITHSHAAND40BITRC4', 10, NULL, NULL, 8, 6, 8),
('TransmissionMethod', 197, 'NOEKEON', 4, NULL, NULL, 10, 10, 0),
('TransmissionMethod', 196, 'PBEWITHSHA-256AND192BITAES-CBC-BC', 3, NULL, NULL, 1, 8, 9),
('TransmissionMethod', 195, 'PBEWITHMD5AND192BITAES-CBC-OPENSSL', 5, NULL, NULL, 9, 0, 2),
('TransmissionMethod', 194, 'PBEWITHSHA-256AND128BITAES-CBC-BC', 2, NULL, NULL, 3, 5, 3),
('TransmissionMethod', 193, 'NOEKEON', 7, NULL, NULL, 8, 0, 1),
('TransmissionMethod', 192, 'PBEWITHSHA-256AND128BITAES-CBC-BC', 5, NULL, NULL, 7, 5, 1),
('TransmissionMethod', 191, 'PBEWITHSHA-256AND128BITAES-CBC-BC', 5, NULL, NULL, 8, 10, 7),
('TransmissionMethod', 190, 'PBEWITHSHAANDTWOFISH-CBC', 9, NULL, NULL, 5, 5, 4),
('TransmissionMethod', 189, 'RC2', 7, NULL, NULL, 8, 1, 0),
('TransmissionMethod', 188, 'RC5', 7, NULL, NULL, 9, 9, 8),
('TransmissionMethod', 187, 'RC6', 2, NULL, NULL, 8, 4, 10),
('TransmissionMethod', 186, 'PBEWITHSHA1AND40BITRC4', 1, NULL, NULL, 3, 0, 3),
('TransmissionMethod', 185, 'Serpent', 2, NULL, NULL, 4, 5, 1),
('TransmissionMethod', 184, 'XTEA', 10, NULL, NULL, 6, 2, 8),
('TransmissionMethod', 183, 'PBEWITHSHAAND128BITRC4', 2, NULL, NULL, 7, 9, 0),
('TransmissionMethod', 182, 'AES', 2, NULL, NULL, 0, 1, 9),
('TransmissionMethod', 181, 'PBEWITHMD5AND192BITAES-CBC-OPENSSL', 1, NULL, NULL, 5, 1, 9),
('TransmissionMethod', 180, 'PBEWITHSHA1AND128BITAES-CBC-BC', 5, NULL, NULL, 9, 3, 8),
('TransmissionMethod', 179, 'CAST5', 2, NULL, NULL, 3, 9, 7),
('TransmissionMethod', 178, 'CAST6', 7, NULL, NULL, 10, 5, 0),
('TransmissionMethod', 177, 'PBEWITHSHA-1AND192BITAES-CBC-BC', 4, NULL, NULL, 7, 7, 6),
('TransmissionMethod', 176, 'Twofish', 3, NULL, NULL, 9, 3, 7),
('TransmissionMethod', 175, 'RC2', 7, NULL, NULL, 6, 2, 2),
('TransmissionMethod', 174, 'RC5', 9, NULL, NULL, 3, 0, 0),
('TransmissionMethod', 173, 'Twofish', 3, NULL, NULL, 2, 7, 7),
('TransmissionMethod', 172, 'PBEWITHSHA1AND40BITRC2-CBC', 10, NULL, NULL, 0, 8, 1),
('TransmissionMethod', 171, 'PBEWITHSHAAND128BITRC4', 0, NULL, NULL, 0, 9, 8),
('TransmissionMethod', 170, 'IDEA', 7, NULL, NULL, 1, 3, 5),
('TransmissionMethod', 169, 'PBEWITHSHA-256AND192BITAES-CBC-BC', 1, NULL, NULL, 7, 6, 1),
('TransmissionMethod', 168, 'SEED', 6, NULL, NULL, 2, 7, 2),
('TransmissionMethod', 167, 'PBEWITHSHA1ANDRC2', 9, NULL, NULL, 5, 3, 8),
('TransmissionMethod', 166, 'PBEWITHSHAAND40BITRC4', 4, NULL, NULL, 9, 2, 4),
('TransmissionMethod', 165, 'CAMELLIA', 4, NULL, NULL, 4, 7, 8),
('TransmissionMethod', 164, 'IDEA', 5, NULL, NULL, 6, 8, 7),
('TransmissionMethod', 163, 'CAST5', 2, NULL, NULL, 2, 5, 10),
('TransmissionMethod', 162, 'PBEWITHSHAAND40BITRC2-CBC', 3, NULL, NULL, 10, 0, 0),
('TransmissionMethod', 161, 'PBEWITHSHA1AND128BITAES-CBC-BC', 10, NULL, NULL, 7, 1, 5),
('TransmissionMethod', 160, 'BLOWFISH', 10, NULL, NULL, 8, 5, 8),
('TransmissionMethod', 159, 'PBEWITHSHAANDTWOFISH-CBC', 7, NULL, NULL, 8, 5, 6),
('TransmissionMethod', 158, 'PBEWITHSHAAND128BITAES-CBC-BC', 2, NULL, NULL, 2, 10, 7),
('TransmissionMethod', 157, 'PBEWITHSHA1AND192BITAES-CBC-BC', 8, NULL, NULL, 10, 2, 5),
('TransmissionMethod', 156, 'CAMELLIA', 10, NULL, NULL, 6, 6, 0),
('TransmissionMethod', 155, 'NOEKEON', 2, NULL, NULL, 4, 1, 0),
('TransmissionMethod', 154, 'XTEA', 7, NULL, NULL, 3, 4, 9),
('TransmissionMethod', 153, 'PBEWITHSHA-1AND192BITAES-CBC-BC', 8, NULL, NULL, 10, 9, 2),
('TransmissionMethod', 152, 'PBEWITHSHA256AND192BITAES-CBC-BC', 10, NULL, NULL, 10, 0, 3),
('TransmissionMethod', 151, 'PBEWITHSHAANDIDEA-CBC', 6, NULL, NULL, 8, 2, 7),
('TransmissionMethod', 150, 'PBEWITHSHA-1AND128BITAES-CBC-BC', 3, NULL, NULL, 3, 1, 3),
('TransmissionMethod', 149, 'RIJNDAEL', 3, NULL, NULL, 3, 10, 9),
('TransmissionMethod', 148, 'PBEWITHSHAAND40BITRC4', 6, NULL, NULL, 5, 7, 7),
('TransmissionMethod', 147, 'PBEWITHSHA256AND128BITAES-CBC-BC', 10, NULL, NULL, 7, 6, 6),
('TransmissionMethod', 146, 'XTEA', 3, NULL, NULL, 4, 7, 0),
('TransmissionMethod', 145, 'PBEWITHSHA1AND192BITAES-CBC-BC', 2, NULL, NULL, 0, 5, 6),
('TransmissionMethod', 144, 'SKIPJACK', 8, NULL, NULL, 1, 6, 2),
('TransmissionMethod', 143, 'IDEA', 10, NULL, NULL, 2, 8, 8),
('TransmissionMethod', 142, 'NOEKEON', 4, NULL, NULL, 0, 6, 3),
('TransmissionMethod', 141, 'DES', 9, NULL, NULL, 10, 8, 4),
('TransmissionMethod', 204, 'PBEWITHSHA-1AND128BITAES-CBC-BC', 8, NULL, NULL, 1, 9, 9),
('TransmissionMethod', 205, 'CAST6', 9, NULL, NULL, 1, 6, 9),
('TransmissionMethod', 206, 'PBEWITHSHA1AND256BITAES-CBC-BC', 10, NULL, NULL, 8, 9, 1),
('TransmissionMethod', 207, 'CAST5', 9, NULL, NULL, 3, 10, 7),
('TransmissionMethod', 208, 'PBEWITHSHAAND192BITAES-CBC-BC', 0, NULL, NULL, 8, 0, 1),
('TransmissionMethod', 209, 'PBEWITHSHA1AND128BITAES-CBC-BC', 10, NULL, NULL, 6, 0, 10),
('TransmissionMethod', 210, 'TEA', 4, NULL, NULL, 2, 8, 3),
('TransmissionMethod', 211, 'DES', 2, NULL, NULL, 8, 7, 4),
('TransmissionMethod', 212, 'Serpent', 3, NULL, NULL, 5, 5, 0),
('TransmissionMethod', 213, 'PBEWITHSHA256AND256BITAES-CBC-BC', 3, NULL, NULL, 8, 8, 10),
('TransmissionMethod', 214, 'Serpent', 8, NULL, NULL, 7, 9, 0),
('TransmissionMethod', 215, 'PBEWITHSHA1AND192BITAES-CBC-BC', 2, NULL, NULL, 9, 1, 8),
('TransmissionMethod', 216, 'PBEWITHSHA-1AND256BITAES-CBC-BC', 1, NULL, NULL, 6, 7, 7),
('TransmissionMethod', 217, 'IDEA', 6, NULL, NULL, 5, 3, 3),
('TransmissionMethod', 218, 'PBEWITHSHAAND128BITRC4', 9, NULL, NULL, 0, 8, 0),
('TransmissionMethod', 219, 'RIJNDAEL', 5, NULL, NULL, 8, 0, 6),
('TransmissionMethod', 220, 'PBEWITHMD5AND128BITAES-CBC-OPENSSL', 10, NULL, NULL, 0, 10, 4),
('TransmissionMethod', 221, 'RC4', 5, NULL, NULL, 3, 2, 7),
('TransmissionMethod', 222, 'SEED', 10, NULL, NULL, 3, 8, 7),
('TransmissionMethod', 223, 'PBEWITHMD5AND256BITAES-CBC-OPENSSL', 2, NULL, NULL, 3, 6, 9),
('TransmissionMethod', 224, 'PBEWITHSHA1AND256BITAES-CBC-BC', 7, NULL, NULL, 2, 1, 0),
('TransmissionMethod', 225, 'DES', 0, NULL, NULL, 7, 5, 6),
('TransmissionMethod', 226, 'AES', 5, NULL, NULL, 10, 2, 2),
('TransmissionMethod', 227, 'PBEWITHMD5ANDDES', 8, NULL, NULL, 10, 4, 9),
('TransmissionMethod', 228, 'PBEWITHSHAAND128BITAES-CBC-BC', 5, NULL, NULL, 1, 10, 3),
('TransmissionMethod', 229, 'SEED', 6, NULL, NULL, 0, 3, 0),
('TransmissionMethod', 230, 'PBEWITHSHAAND192BITAES-CBC-BC', 0, NULL, NULL, 2, 4, 10),
('TransmissionMethod', 231, 'BLOWFISH', 4, NULL, NULL, 10, 3, 0),
('TransmissionMethod', 232, 'PBEWITHMD5AND256BITAES-CBC-OPENSSL', 3, NULL, NULL, 7, 10, 1),
('TransmissionMethod', 233, 'PBEWITHSHA-256AND256BITAES-CBC-BC', 1, NULL, NULL, 8, 7, 10),
('TransmissionMethod', 234, 'PBEWITHMD5ANDDES', 5, NULL, NULL, 0, 3, 4),
('TransmissionMethod', 235, 'PBEWITHSHA-1AND192BITAES-CBC-BC', 1, NULL, NULL, 1, 0, 4),
('TransmissionMethod', 236, 'PBEWITHSHA-1AND256BITAES-CBC-BC', 9, NULL, NULL, 0, 0, 8),
('TransmissionMethod', 237, 'TEA', 5, NULL, NULL, 3, 10, 4),
('TransmissionMethod', 238, 'BLOWFISH', 1, NULL, NULL, 2, 4, 1),
('TransmissionMethod', 239, 'DES', 10, NULL, NULL, 8, 8, 9),
('TransmissionMethod', 240, 'PBEWITHSHA256AND128BITAES-CBC-BC', 8, NULL, NULL, 3, 9, 1),
('TransmissionMethod', 241, 'PBEWITHSHA256AND128BITAES-CBC-BC', 6, NULL, NULL, 4, 5, 0),
('TransmissionMethod', 242, 'PBEWITHSHAAND40BITRC2-CBC', 6, NULL, NULL, 9, 2, 5),
('TransmissionMethod', 243, 'PBEWITHSHA256AND192BITAES-CBC-BC', 5, NULL, NULL, 4, 2, 1),
('TransmissionMethod', 244, 'PBEWITHSHA256AND192BITAES-CBC-BC', 1, NULL, NULL, 5, 4, 7),
('TransmissionMethod', 245, 'CAMELLIA', 9, NULL, NULL, 0, 6, 4),
('TransmissionMethod', 246, 'PBEWITHSHAAND128BITAES-CBC-BC', 8, NULL, NULL, 1, 9, 9),
('TransmissionMethod', 247, 'SKIPJACK', 2, NULL, NULL, 5, 9, 3),
('TransmissionMethod', 248, 'RC5-32', 5, NULL, NULL, 10, 2, 4),
('TransmissionMethod', 249, 'CAST5', 2, NULL, NULL, 3, 1, 10),
('TransmissionMethod', 250, 'ARCFOUR', 4, NULL, NULL, 8, 2, 7),
('TransmissionMethod', 251, 'PBEWITHSHA-256AND256BITAES-CBC-BC', 4, NULL, NULL, 9, 6, 1),
('TransmissionMethod', 252, 'RC6', 1, NULL, NULL, 5, 10, 8),
('TransmissionMethod', 253, 'RC5', 2, NULL, NULL, 8, 6, 6),
('TransmissionMethod', 254, 'RC2', 1, NULL, NULL, 0, 4, 9),
('TransmissionMethod', 255, 'PBEWITHSHA1ANDDES', 7, NULL, NULL, 1, 0, 10),
('TransmissionMethod', 256, 'PBEWITHSHA-1AND128BITAES-CBC-BC', 1, NULL, NULL, 4, 5, 8),
('TransmissionMethod', 257, 'PBEWITHSHA-256AND256BITAES-CBC-BC', 7, NULL, NULL, 10, 7, 8),
('TransmissionMethod', 258, 'ARC4', 8, NULL, NULL, 0, 4, 10),
('TransmissionMethod', 259, 'PBEWITHSHAAND128BITRC2-CBC', 8, NULL, NULL, 4, 4, 1),
('TransmissionMethod', 260, 'PBEWITHSHAAND256BITAES-CBC-BC', 3, NULL, NULL, 7, 2, 9),
('TransmissionMethod', 261, 'SEED', 0, NULL, NULL, 0, 4, 1),
('TransmissionMethod', 262, 'PBEWITHSHA1ANDRC2', 6, NULL, NULL, 8, 2, 8),
('TransmissionMethod', 263, 'RC5-32', 0, NULL, NULL, 6, 6, 5),
('TransmissionMethod', 264, 'PBEWITHSHAANDIDEA-CBC', 3, NULL, NULL, 3, 1, 1),
('TransmissionMethod', 265, 'PBEWITHSHAAND128BITRC2-CBC', 6, NULL, NULL, 8, 6, 6),
('TransmissionMethod', 266, 'PBEWITHSHA1AND256BITAES-CBC-BC', 6, NULL, NULL, 0, 1, 4),
('TransmissionMethod', 267, 'ARC4', 10, NULL, NULL, 5, 8, 3),
('TransmissionMethod', 268, 'RC4', 2, NULL, NULL, 8, 10, 1),
('TransmissionMethod', 269, 'RC2', 1, NULL, NULL, 6, 5, 2),
('TransmissionMethod', 270, 'PBEWITHSHAAND128BITRC2-CBC', 7, NULL, NULL, 4, 3, 5),
('TransmissionMethod', 271, 'PBEWITHMD5ANDRC2', 4, NULL, NULL, 5, 3, 8),
('TransmissionMethod', 272, 'PBEWITHSHAAND40BITRC2-CBC', 7, NULL, NULL, 2, 8, 7),
('TransmissionMethod', 273, 'PBEWITHMD5ANDRC2', 2, NULL, NULL, 10, 3, 9),
('TransmissionMethod', 274, 'PBEWITHSHAANDTWOFISH-CBC', 1, NULL, NULL, 7, 9, 2),
('TransmissionMethod', 275, 'PBEWITHSHAAND256BITAES-CBC-BC', 6, NULL, NULL, 4, 5, 0),
('TransmissionMethod', 276, 'CAMELLIA', 4, NULL, NULL, 3, 10, 2),
('TransmissionMethod', 277, 'PBEWITHSHAANDIDEA-CBC', 0, NULL, NULL, 0, 10, 10),
('TransmissionMethod', 278, 'PBEWITHSHA1AND128BITRC2-CBC', 9, NULL, NULL, 3, 4, 2),
('TransmissionMethod', 279, 'PBEWITHSHA1ANDDES', 10, NULL, NULL, 7, 4, 2),
('TransmissionMethod', 280, 'AES', 6, NULL, NULL, 3, 7, 5),
('TransmissionMethod', 281, 'RC6', 1, NULL, NULL, 9, 0, 0),
('TransmissionMethod', 282, 'RC5', 0, NULL, NULL, 5, 7, 10),
('TransmissionMethod', 283, 'SKIPJACK', 1, NULL, NULL, 0, 7, 10),
('TransmissionMethod', 284, 'RC2', 6, NULL, NULL, 7, 9, 8),
('TransmissionMethod', 285, 'IDEA', 5, NULL, NULL, 4, 4, 9),
('TransmissionMethod', 286, 'PBEWITHSHA1AND128BITRC4', 5, NULL, NULL, 2, 1, 1),
('TransmissionMethod', 287, 'PBEWITHSHA-1AND256BITAES-CBC-BC', 0, NULL, NULL, 0, 6, 0),
('TransmissionMethod', 288, 'DES', 10, NULL, NULL, 1, 0, 5),
('TransmissionMethod', 289, 'SKIPJACK', 3, NULL, NULL, 3, 9, 4),
('TransmissionMethod', 290, 'PBEWITHSHA256AND256BITAES-CBC-BC', 7, NULL, NULL, 10, 2, 2),
('TransmissionMethod', 291, 'PBEWITHSHA256AND256BITAES-CBC-BC', 2, NULL, NULL, 7, 9, 6),
('TransmissionMethod', 292, 'Twofish', 3, NULL, NULL, 1, 4, 1),
('TransmissionMethod', 293, 'PBEWITHSHA1ANDRC2', 2, NULL, NULL, 3, 8, 2),
('TransmissionMethod', 294, 'DES', 3, NULL, NULL, 4, 7, 5),
('TransmissionMethod', 295, 'PBEWITHSHAAND192BITAES-CBC-BC', 9, NULL, NULL, 3, 7, 9),
('TransmissionMethod', 296, 'TEA', 5, NULL, NULL, 5, 9, 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `IndexCertificate`
--

CREATE TABLE IF NOT EXISTS `IndexCertificate` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `asch_id` bigint(20) DEFAULT NULL,
  `ascm_id` bigint(20) DEFAULT NULL,
  `pcp_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKB642AD859905FB76` (`asch_id`),
  KEY `FKB642AD8599084156` (`ascm_id`),
  KEY `FKB642AD85CB18401E` (`pcp_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dump dei dati per la tabella `IndexCertificate`
--

INSERT INTO `IndexCertificate` (`id`, `asch_id`, `ascm_id`, `pcp_id`) VALUES
(5, 13, 14, 15);

-- --------------------------------------------------------

--
-- Struttura della tabella `IndexCertificate_Certificate`
--

CREATE TABLE IF NOT EXISTS `IndexCertificate_Certificate` (
  `IndexCertificate_id` bigint(20) NOT NULL,
  `hostCertificates_id` bigint(20) NOT NULL,
  UNIQUE KEY `UK_8B6ED5BD43C5BD6` (`hostCertificates_id`),
  KEY `FK8B6ED5BDA8E4627` (`hostCertificates_id`),
  KEY `FK8B6ED5BD7FE23696` (`IndexCertificate_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `IndexCertificate_Certificate`
--

INSERT INTO `IndexCertificate_Certificate` (`IndexCertificate_id`, `hostCertificates_id`) VALUES
(5, 17);

-- --------------------------------------------------------

--
-- Struttura della tabella `KeyPair`
--

CREATE TABLE IF NOT EXISTS `KeyPair` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `privateKey_id` bigint(20) DEFAULT NULL,
  `publicKey_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK329BC5B9BC063436` (`privateKey_id`),
  KEY `FK329BC5B92989F23E` (`publicKey_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dump dei dati per la tabella `KeyPair`
--

INSERT INTO `KeyPair` (`id`, `privateKey_id`, `publicKey_id`) VALUES
(1, 1, 1),
(2, 2, 3);

-- --------------------------------------------------------

--
-- Struttura della tabella `PrivateKey`
--

CREATE TABLE IF NOT EXISTS `PrivateKey` (
  `DTYPE` varchar(31) NOT NULL,
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `value` longtext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dump dei dati per la tabella `PrivateKey`
--

INSERT INTO `PrivateKey` (`DTYPE`, `id`, `value`) VALUES
('PrivateASKP', 1, 'MIICgwIBADANBgkqhkiG9w0BAQEFAASCAm0wggJpAgEAAoGEAL1HV3fbo9jhwN0mOoYJo2FyvLViThi8wtkEeMI6Hxf3i4Yy6paz9ej50sQnH5CXLxNWCTeprAdHYBeUynI7+Th6evfwxiFeZioj450qybF/8xv8Ltt/W2nK9JlbCnNLJF0asn5Ch5HQ+ifjY4zSf+vFyUwu6NrjjfM8U81YkeOP/9kfAgMBAAECgYNOu6dRryZnp/huxWEgsk+o0G5Y0OyFc7dlSGvm9wBKGumJyBI23+LRp2GZwGBr58doNqic49IwMOtyw6y2eeYALT8C2cYyFo3bMVHP0IG5D6/dLyDr5Y2uu2ddHNhYnVLKgsDU9bJ5Q6+N0tNRa4+UnB9mff2kRrLYcLdWCDajzewH6QJCD5Jzd4VKBVNlvK1jhbpUnxK84JA2b3zPVo0bQNzqsYCV8P+QoqUhO9Z770qkxX+B/r3f3rMxSQyThPCDyVrMjmXzAkIMJ66PldPm9/mOwk47qKZUEyxfiQFn6vtagUr+4n3UEO6wbOAyG2wrbH4kCAq6oyWZmxORLA9SC/Q6FoqSWYXeryUCQgk0VdAmFzTCsPYvvw4Cr5Y/aymJ1MNdkOjmC1/xtUAqc3a6eXHKDbTi+OrewjqDPgGnljZ60P0O0/vWdu+EHjllhQJCC0owUR2RHM7m9Tp0/+S92QmWBYZzz9nqbT36MpSc2wR/L09xThk6d3rmDwGJNaGjovDvU7/g2wxPZETboOOV/CghAkIG2Pq485qO/y97SlrJlj/Nzgee7+ySwpr6q453hDtYZx1ORwpsTcmpfEN03a52qZzp5MiFWyUKGZZs6zFGtanYU6Y=\r\n'),
('PrivateKey', 2, 'MIICsTAbBgoqhkiG9w0BDAEDMA0ECMNtYq5SXkGmAgEUBIICkE8nuMSoWCeLGP+ULpI3+lgRfMdnKbZWDLEjVWEewYuhG4BpJRYLAiddWpNqS+yAtcnUjtaqNPykAq0/Vy/9LGbkg+HChCi7bBRVgdw+BqJWaCIMcQqjIm3c/dBAShfLQ1ULpHezmughLFddvx+aa2e0tuo/VLTpYIoupihvZ94L/v/W+eg71ctE+7FTlo7ck/E81Jw7PMz8iC+pnr6NBYm3drAagMPoF7knHLhmjr+aozidha6SplyrQvqtXJsgDFJggjmKwHdZZ0+amZKlc+5ruiOWrTJkaurB9b2rqV5kbgv/HoO7DVy3w45VhlrXOz/UyAQk33oOquifNYdCkW41RzZ2GrtRv+KMq03nL/aOD3W8Smed6EIrhhSg4JlgnJ3D67xTpFFZXJ6JcfHqSl5zkvYANSoIwksiEkNY6/SOG2RegSjjcwR38n2LIIyhIPkMoEX1e7A2FeXjvrceOJQ+6q+DygOeg4T4JJnmxiSnnSj42wj/1SIhZAb2ZLv9+ubvodZOnkamkeRcG66QRfHcoKZVRXjXekHvzDMzv/w+nD0c+rrrde+9Zo/77blnUmnlJd2xowd68vZQ5a9YQcWy0A2jgCDrIsa6ghZWNrqu6hJr/lHp6SnGFd2q98UEbnnnXEIrs5hB1GUwgg3cRHdP5ejVDYp8SchN3XLbUlN7ly2okPndpYAApKBRn5tbdEXWTNmp9+GVS0xvCZbTdZ6FHyO60mP0MPzMRw3y/wFu7+g6g64KXQ6sSZ1fVyRR3W0Fkc0t+3J+PsBHAAyDnF4YqhpU5fyslkuLlNixsmR9wbMCcsx02+L+9k0NjpRzt+qL3lmjaxSObriO7eHyNc5CBmS671MlAYeFovxuaeIi');

-- --------------------------------------------------------

--
-- Struttura della tabella `Probe`
--

CREATE TABLE IF NOT EXISTS `Probe` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dump dei dati per la tabella `Probe`
--

INSERT INTO `Probe` (`id`) VALUES
(1);

-- --------------------------------------------------------

--
-- Struttura della tabella `Probe_supportedAuthenticationMethods`
--

CREATE TABLE IF NOT EXISTS `Probe_supportedAuthenticationMethods` (
  `Probe_id` bigint(20) NOT NULL,
  `supportedAuthenticationMethods_id` bigint(20) NOT NULL,
  KEY `FK26FF249B4D23C683` (`supportedAuthenticationMethods_id`),
  KEY `FK26FF249B7E0F261E` (`Probe_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `Probe_supportedAuthenticationMethods`
--

INSERT INTO `Probe_supportedAuthenticationMethods` (`Probe_id`, `supportedAuthenticationMethods_id`) VALUES
(1, 9),
(1, 32),
(1, 37),
(1, 46);

-- --------------------------------------------------------

--
-- Struttura della tabella `Probe_supportedTransmissionMethods`
--

CREATE TABLE IF NOT EXISTS `Probe_supportedTransmissionMethods` (
  `Probe_id` bigint(20) NOT NULL,
  `supportedTransmissionMethods_id` bigint(20) NOT NULL,
  KEY `FK76CBFFEF4C7457DB` (`supportedTransmissionMethods_id`),
  KEY `FK76CBFFEF7E0F261E` (`Probe_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `Probe_supportedTransmissionMethods`
--

INSERT INTO `Probe_supportedTransmissionMethods` (`Probe_id`, `supportedTransmissionMethods_id`) VALUES
(1, 207),
(1, 142),
(1, 238),
(1, 225);

-- --------------------------------------------------------

--
-- Struttura della tabella `PublicKey`
--

CREATE TABLE IF NOT EXISTS `PublicKey` (
  `DTYPE` varchar(31) NOT NULL,
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `value` longtext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dump dei dati per la tabella `PublicKey`
--

INSERT INTO `PublicKey` (`DTYPE`, `id`, `value`) VALUES
('PublicASKP', 1, 'MIGiMA0GCSqGSIb3DQEBAQUAA4GQADCBjAKBhAC9R1d326PY4cDdJjqGCaNhcry1Yk4YvMLZBHjCOh8X94uGMuqWs/Xo+dLEJx+Qly8TVgk3qawHR2AXlMpyO/k4enr38MYhXmYqI+OdKsmxf/Mb/C7bf1tpyvSZWwpzSyRdGrJ+QoeR0Pon42OM0n/rxclMLuja443zPFPNWJHjj//ZHwIDAQAB'),
('PublicKey', 3, 'MIGiMA0GCSqGSIb3DQEBAQUAA4GQADCBjAKBhAC14TbV0mqlO8SlMfgHGgMs/xo9lXOZGtpZ9YCfeUpRxP3nJg8EVKVVT86vVK2YpGnAXXcIwv8Idb3Twz2IFKe5+5usHx04VGOdN/TAEVBNbUPr0V9g7bHzHoFpWK1FOVh5iqzyiLAYWIwP0Os7Wbn2FAhq96rB0VMJyqBQh/um2GEIuwIDAQAB');

-- --------------------------------------------------------

--
-- Struttura della tabella `TSDDescriptor`
--

CREATE TABLE IF NOT EXISTS `TSDDescriptor` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `askpKeyPair_id` bigint(20) DEFAULT NULL,
  `indexCertificate_id` bigint(20) DEFAULT NULL,
  `memory_id` bigint(20) DEFAULT NULL,
  `probe_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK988CEAF4CE200CF5` (`askpKeyPair_id`),
  KEY `FK988CEAF47FE23696` (`indexCertificate_id`),
  KEY `FK988CEAF4658C88A3` (`memory_id`),
  KEY `FK988CEAF47E0F261E` (`probe_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dump dei dati per la tabella `TSDDescriptor`
--

INSERT INTO `TSDDescriptor` (`id`, `name`, `askpKeyPair_id`, `indexCertificate_id`, `memory_id`, `probe_id`) VALUES
(1, 'TSD 1', 1, 5, 1, 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `TSDDescriptor_UserIndexMemory`
--

CREATE TABLE IF NOT EXISTS `TSDDescriptor_UserIndexMemory` (
  `TSDDescriptor_id` bigint(20) NOT NULL,
  `userIndexMemory_id` bigint(20) NOT NULL,
  `userIndexMemory_KEY` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`TSDDescriptor_id`,`userIndexMemory_KEY`),
  UNIQUE KEY `UK_5835FC3DDE779912` (`userIndexMemory_id`),
  KEY `FK5835FC3D62676A1E` (`userIndexMemory_id`),
  KEY `FK5835FC3D30A463BE` (`TSDDescriptor_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `TSDDescriptor_UserIndexMemory`
--

INSERT INTO `TSDDescriptor_UserIndexMemory` (`TSDDescriptor_id`, `userIndexMemory_id`, `userIndexMemory_KEY`) VALUES
(1, 2, 'host1');

-- --------------------------------------------------------

--
-- Struttura della tabella `TSDMemory`
--

CREATE TABLE IF NOT EXISTS `TSDMemory` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dump dei dati per la tabella `TSDMemory`
--

INSERT INTO `TSDMemory` (`id`) VALUES
(1);

-- --------------------------------------------------------

--
-- Struttura della tabella `TSDMemory_elements`
--

CREATE TABLE IF NOT EXISTS `TSDMemory_elements` (
  `id` bigint(20) NOT NULL,
  `memory` varchar(255) DEFAULT NULL,
  `memory_KEY` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`,`memory_KEY`),
  KEY `FK62741A10FEA28CA5` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `TSDMemory_elements`
--

INSERT INTO `TSDMemory_elements` (`id`, `memory`, `memory_KEY`) VALUES
(1, 'ciao sono un nuovo valore', 0),
(1, 'ne', 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `UserIndexMemory`
--

CREATE TABLE IF NOT EXISTS `UserIndexMemory` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dump dei dati per la tabella `UserIndexMemory`
--

INSERT INTO `UserIndexMemory` (`id`) VALUES
(1),
(2);

-- --------------------------------------------------------

--
-- Struttura della tabella `userIndexMemory_read`
--

CREATE TABLE IF NOT EXISTS `userIndexMemory_read` (
  `id` bigint(20) NOT NULL,
  `readColumn` int(11) DEFAULT NULL,
  KEY `FK5027AFCD83EFDE27` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `userIndexMemory_read`
--

INSERT INTO `userIndexMemory_read` (`id`, `readColumn`) VALUES
(1, 0),
(1, 1),
(2, 0),
(2, 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `userIndexMemory_write`
--

CREATE TABLE IF NOT EXISTS `userIndexMemory_write` (
  `id` bigint(20) NOT NULL,
  `writeColumn` int(11) DEFAULT NULL,
  KEY `FKB51AC88883EFDE27` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `userIndexMemory_write`
--

INSERT INTO `userIndexMemory_write` (`id`, `writeColumn`) VALUES
(1, 0),
(2, 0);


CREATE TABLE logging_event 
  (
    timestmp         BIGINT NOT NULL,
    formatted_message  TEXT NOT NULL,
    logger_name       VARCHAR(254) NOT NULL,
    level_string      VARCHAR(254) NOT NULL,
    thread_name       VARCHAR(254),
    reference_flag    SMALLINT,
    arg0              VARCHAR(254),
    arg1              VARCHAR(254),
    arg2              VARCHAR(254),
    arg3              VARCHAR(254),
    caller_filename   VARCHAR(254) NOT NULL,
    caller_class      VARCHAR(254) NOT NULL,
    caller_method     VARCHAR(254) NOT NULL,
    caller_line       CHAR(4) NOT NULL,
    event_id          BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY
  );
COMMIT;

BEGIN;
CREATE TABLE logging_event_property
  (
    event_id	      BIGINT NOT NULL,
    mapped_key        VARCHAR(254) NOT NULL,
    mapped_value      TEXT,
    PRIMARY KEY(event_id, mapped_key),
    FOREIGN KEY (event_id) REFERENCES logging_event(event_id)
  );
COMMIT;

BEGIN;
CREATE TABLE logging_event_exception
  (
    event_id         BIGINT NOT NULL,
    i                SMALLINT NOT NULL,
    trace_line       VARCHAR(254) NOT NULL,
    PRIMARY KEY(event_id, i),
    FOREIGN KEY (event_id) REFERENCES logging_event(event_id)
  );

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
