package ieee1667.AuthenticationSilo;

import ieee1667.Constant;
import ieee1667.Messages;
import ieee1667.AuthenticationSilo.message.AuthSiloAuthenticationMessage;
import ieee1667.AuthenticationSilo.message.HostAuthenticationMessage;
import ieee1667.AuthenticationSilo.message.SignedAuthSiloAuthenticationMessage;
import ieee1667.AuthenticationSilo.message.SignedHostAuthenticationMessage;
import ieee1667.AuthenticationSilo.message.SignedTSDAuthenticationMessage;
import ieee1667.AuthenticationSilo.message.TSDAuthenticationMessage;
import ieee1667.Logger.Logger;
import ieee1667.common.model.ASCh;
import ieee1667.common.model.ASCm;
import ieee1667.common.model.HCh;
import ieee1667.common.model.PrivateKey;
import ieee1667.common.model.TSDDescriptor;
import java.security.Signature;

/**
 * Manager, si occupa della gestione delle autenticazioni
 * @author mauro
 */
public class AuthenticationSilo {
	
	/**
     * TSD utilizzato durante la sessione corrente
     */
    private TSDDescriptor tsd;
    
    /**
     * Indica se il componente � stato inizializzato
     */
    private boolean isInitialized;
    
    /**
     * Costruttore di default
     */
    public AuthenticationSilo () {
        this.isInitialized = false;
    }
    
    /**
     * Inizializza il componente. Questo metodo deve essere chiamato prima di ogni altro metodo
     * @param tsd il tsd utilizzato durante la sessione corrente
     */
    public void initialize(TSDDescriptor tsd) {
        this.tsd = tsd;
        this.isInitialized = true;
    }
    
    /**
     * Resetta il componente
     */
    public void reset() {
        this.tsd = null;
        this.isInitialized = false;
    }
    
    /**
     * Richiede l'autenticazione del tsd.
     * Il componente deve essere inizializzato (vedi {@link #isInitialized()}
     * @param ascm il certificato ASCm da utilizzare per verificare la firma del messaggio di autenticazione
     * @return true se l'autenticazione � andata a buon fine, false altrimenti
     */
    public boolean requestTSDAuthentication(ASCm ascm) {
    	if(!this.isInitialized()) {
    		Logger.log(Messages.getString("AuthenticationSilo.NOT_INITIALIZED")); //$NON-NLS-1$
    		return false;
    	}
        TSDAuthenticationMessage message = this.getTSDAuthenticationMessage();
    	SignedTSDAuthenticationMessage signedMessage = this.signTSDAuthenticationMessage(message);
    	boolean res = this.verifyTSDAuthenticationMessage(signedMessage, ascm);
    	
    	if (res) {
    		Logger.log(Messages.getString("AuthenticationSilo.AUTH_TSD_DONE"));
    	} else {
    		Logger.log(Messages.getString("AuthenticationSilo.AUTH_TSD_FAILED"));
    	}
    	
    	return res;
    }
    
    /**
     * Richiede l'autenticazione dell'authentication silo.
     * Il componente deve essere inizializzato (vedi {@link #isInitialized()}
     * @param asch il certificato ASCh da utilizzare per verificare la firma del messaggio di autenticazione
     * @return true se l'autenticazione � andata a buon fine, false altrimenti
     */
	public boolean requestAuthSiloAuthentication(ASCh asch) {
		if(!this.isInitialized()) {
    		Logger.log(Messages.getString("AuthenticationSilo.NOT_INITIALIZED")); //$NON-NLS-1$
    		return false;
    	}
		AuthSiloAuthenticationMessage message = this.getAuthSiloAuthenticationMessage();
    	SignedAuthSiloAuthenticationMessage signedMessage = this.signAuthSiloAuthenticationMessage(message);
    	boolean res = this.verifyAuthSiloAuthenticationMessage(signedMessage, asch);
    	
    	if (res) {
    		Logger.log(Messages.getString("AuthenticationSilo.AUTH_AUTH_SILO_DONE"));
    	} else {
    		Logger.log(Messages.getString("AuthenticationSilo.AUTH_AUTH_SILO_FAILED"));
    	}
    	
    	return res;
	}
	
	/**
	 * Autentica un utente.
	 * Il componente deve essere inizializzato (vedi {@link #isInitialized()}
	 * @param userID l'userID dell'utente
	 * @param privateKey la chiave segreta da utilizzare per decifrare la chiave privata
	 * @param userHCh il certificato
	 * @param hchIndex l'indice del certificato da utilizzare per validare la richiesta di autenticazione
	 * @param authenticationMethod  il metodo di autenticazione da utilizzare
	 * @return true se l'autenticazione � andata a buon fine, false altrimenti
	 */
	public boolean requestUserAuthentication(String userID, PrivateKey privateKey, HCh userHCh, int hchIndex, String authenticationMethod) {
		if(!this.isInitialized()) {
    		Logger.log(Messages.getString("AuthenticationSilo.NOT_INITIALIZED")); //$NON-NLS-1$
    		return false;
    	}
    	HostAuthenticationMessage message = this.getHostAuthenticationMessage(userID, hchIndex);
        SignedHostAuthenticationMessage signedMessage = this.signHostAuthenticationMessage(message, privateKey, authenticationMethod);
        boolean res = this.verifyHostAuthenticationMessage(signedMessage, hchIndex);
    	
    	if (res) {
    		Logger.log(Messages.getString("AuthenticationSilo.AUTH_USER_DONE"));
    	} else {
    		Logger.log(Messages.getString("AuthenticationSilo.AUTH_USER_FAILED"));
    	}
    	
    	return res;
	}
    
	/**
	 * Metodo utilizzato per verificare se l'authentication silo � inizializzato
	 * @return true se l'authentication silo � inizializzato, false altrimenti
	 */
	public boolean isInitialized() {
		return this.isInitialized;
	}

	/*
     * METODI PRIVATI 
     */

	/**
	 * Genera un messaggio di autenticazione TSD
	 * @return un messaggio di autentcazione del tsd
	 */
    private TSDAuthenticationMessage getTSDAuthenticationMessage() {
    	return new TSDAuthenticationMessage();
    }
    
    /**
     * Genera un messaggio di autenticazione per l'auth silo
     * @return un messagio di autenticazione per l'auth silo
     */
    private AuthSiloAuthenticationMessage getAuthSiloAuthenticationMessage() {
    	return new AuthSiloAuthenticationMessage();
	}
    
    /**
     * Genera un messaggio di autenticazione per l'utente
     * @param userID l'userID dell'utente
     * @return un messaggio di autenticazione utente
     */
    private HostAuthenticationMessage getHostAuthenticationMessage(String userID, int hchIndex) {
		return new HostAuthenticationMessage(hchIndex, userID);
	}

    
    /**
     * Firma un messaggio di autenticazione TSD con la chiave privata ASKP contenuta nel tsd corrente
     * @param message il messaggio da firmare
     * @return il messaggio firmato
     */
    private SignedTSDAuthenticationMessage signTSDAuthenticationMessage(TSDAuthenticationMessage message) {
        /*
         * Posso accedere alla chiave private perch� � come se fossi nel TSD
         */
        // creo la chiave privata a partire dal dato (stringa) persistito
        java.security.PrivateKey pk = this.tsd.getAskpKeyPair().getPrivateKey().getJavaVersion();
            
        SignedTSDAuthenticationMessage signedMessage = null;
        
        // firmo il messaggio
        try {
            // inizializzo il signer e inserisco i dati da firmare
            Signature sig = Signature.getInstance(Constant.TSD_AUTH_METHOD, Constant.PROVIDER); 
            sig.initSign(pk);
            sig.update(message.getMessage().getBytes());
            
            // ottengo la firma
            byte[] signature = sig.sign();
            
            // creo il messaggio firmato
            signedMessage = new SignedTSDAuthenticationMessage(message);
            signedMessage.setSign(signature);
            
        
        } catch (Exception ex) {
            Logger.log(Messages.getString("AuthenticationSilo.TSD_AUTH_MESSAGE_ERROR"), ex.getMessage()); 
            return null;
        }
        
        return signedMessage;
    }
    

    /**
     * Firma un messaggio di autenticazione AuthSilo con la chiave privata ASKP contenuta nel tsd corrente
     * @param message il messaggio da firmare
     * @return il messaggio firmato
     */
	private SignedAuthSiloAuthenticationMessage signAuthSiloAuthenticationMessage(AuthSiloAuthenticationMessage message) {
        /*
         * Posso accedere alla chiave private perch� � come se fossi nel TSD
         */
        // creo la chiave privata a partire dal dato (stringa) persistito
        java.security.PrivateKey pk = this.tsd.getAskpKeyPair().getPrivateKey().getJavaVersion();
            
        SignedAuthSiloAuthenticationMessage signedMessage = null;
        
        // firmo il messaggio
        try {
            // inizializzo il signer e inserisco i dati da firmare
            Signature sig = Signature.getInstance(Constant.TSD_AUTH_METHOD, Constant.PROVIDER); //$NON-NLS-1$
            sig.initSign(pk);
            sig.update(message.getMessage().getBytes());
            
            // ottengo la firma
            byte[] signature = sig.sign();
            
            // creo il messaggio firmato
            signedMessage = new SignedAuthSiloAuthenticationMessage(message);
            signedMessage.setSign(signature);
            
        
        } catch (Exception ex) {
            Logger.log(Messages.getString("AuthenticationSilo.AUTH_SILO_AUTH_MESSAGE_ERROR"), ex.getMessage()); //$NON-NLS-1$
            return null;
        }
        
        return signedMessage;
	}
	
    /**
     * Firma il messaggio di autenticazione utente con la chiave privata dell'utente
     * @param message il messaggio da firmare
     * @param privateKey la chiave privata dell'utente
     * @param authenticationMethod il metodo di autenticazione da usare per firmare il messaggio
     * @return un messaggio di autenticazione utente firmato
     */
	private SignedHostAuthenticationMessage signHostAuthenticationMessage(
			HostAuthenticationMessage message, PrivateKey privateKey, String authenticationMethod) {
        
		
		SignedHostAuthenticationMessage signedMessage = null;
        
        // firmo il messaggio
        try {
            // inizializzo il signer e inserisco i dati da firmare
            Signature sig = Signature.getInstance(authenticationMethod, Constant.PROVIDER); //$NON-NLS-1$
            sig.initSign(privateKey.getJavaVersion());
            sig.update(message.getMessage().getBytes());
            
            // ottengo la firma
            byte[] signature = sig.sign();
            
            // creo il messaggio firmato
            signedMessage = new SignedHostAuthenticationMessage(message, authenticationMethod);
            signedMessage.setSign(signature);
            
        
        } catch (Exception ex) {
            Logger.log(Messages.getString("AuthenticationSilo.USER_AUTH_MESSAGE_ERROR"), ex.getMessage()); //$NON-NLS-1$
            return null;
        }
        
        return signedMessage;
	}
    
	
	/**
	 * Verifica che il messaggio di autenticazione del TSD sia valido attraverso l'autenticit� della firma
	 * @param signedMessage il messaggio da controllare 
	 * @param ascm il certificato ASCm da utilizzare per verificare la firma
	 * @return true se il messaggio � autentico, false altrimenti
	 */
    private boolean verifyTSDAuthenticationMessage(SignedTSDAuthenticationMessage signedMessage, ASCm ascm) {
        
        if (signedMessage == null) {
            Logger.log(Messages.getString("AuthenticationSilo.CANT_AUTH_TSD")); //$NON-NLS-1$
            return false;
        }
        
        // verifico firma messaggio
        // creo chiave pubblica a partire dalla versione persistita
        java.security.PublicKey pk = ascm.getPublicKey().getJavaVersion();
        
        if (pk == null) {
            return false;
        }
        
        try {    
        
            Signature sig = Signature.getInstance(Constant.TSD_AUTH_METHOD, Constant.PROVIDER); //$NON-NLS-1$
            sig.initVerify(pk);
            sig.update(signedMessage.getMessage().getBytes());
            return sig.verify(signedMessage.getSign());
        
        } catch (Exception ex) {
            Logger.log(Messages.getString("AuthenticationSilo.TSD_AUTH_SIGN_VERIFICATION_FAILED"), ex); //$NON-NLS-1$
            return false;
        }   	
    }
    


	/**
	 * Verifica che il messaggio di autenticazione dell'AuthSilo sia valido attraverso l'autenticit� della firma
	 * @param signedMessage il messaggio da controllare 
	 * @param asch il certificato ASCh da utilizzare per verificare la firma
	 * @return true se il messaggio � autentico, false altrimenti
	 */
	private boolean verifyAuthSiloAuthenticationMessage(SignedAuthSiloAuthenticationMessage signedMessage, ASCh asch) {
        if (signedMessage == null) {
            Logger.log(Messages.getString("AuthenticationSilo.CANT_AUTH_AUTH_SILO")); //$NON-NLS-1$
            return false;
        }
        
        // verifico firma messaggio
        // creo chiave pubblica a partire dalla versione persistita
        java.security.PublicKey pk = asch.getPublicKey().getJavaVersion();
        
        if (pk == null) {
            return false;
        }
        
        try {    
        
            Signature sig = Signature.getInstance(Constant.TSD_AUTH_METHOD, Constant.PROVIDER); //$NON-NLS-1$
            sig.initVerify(pk);
            sig.update(signedMessage.getMessage().getBytes());
            return sig.verify(signedMessage.getSign());
        
        } catch (Exception ex) {
            Logger.log(Messages.getString("AuthenticationSilo.AUTH_SILO_AUTH_SIGN_VERIFICATION_FAILED"), ex); //$NON-NLS-1$
            return false;
        }   
	}


	/**
	 * Verifica il messaggio di autenticazione dell'utente
	 * @param signedMessage il messaggio da controllare
	 * @param hchIndex l'indice del certificato HCh da utilizzare
	 * @return true se il messaggio � verificato, falso altrimenti
	 */
	private boolean verifyHostAuthenticationMessage(SignedHostAuthenticationMessage signedMessage, int hchIndex) {
        if (signedMessage == null) {
            Logger.log(Messages.getString("AuthenticationSilo.CANT_AUTH_USER")); //$NON-NLS-1$
            return false;
        }
        
        HCh userHch = null;
        /*
         * Posso accedere alla lista degli HCh, � come se fossi nel TSD
         */
        try {
        	userHch = this.tsd.getIndexCertificate().getHostCertificates().get(hchIndex);
        } catch (Exception e) {
        	Logger.log(Messages.getString("AuthenticationSilo.HCH_NOT_FOUND"), e.getMessage()); //$NON-NLS-1$
        }
        
        java.security.PublicKey pk = userHch.getPublicKey().getJavaVersion();
        
        if (pk == null) {
            Logger.log(Messages.getString("AuthenticationSilo.HCH_WITH_NO_PUB_KEY")); //$NON-NLS-1$
        	return false;
        }
        
        try {    
        
            Signature sig = Signature.getInstance(signedMessage.getUsedCryptographicMethod(), Constant.PROVIDER); //$NON-NLS-1$
            sig.initVerify(pk);
            sig.update(signedMessage.getMessage().getBytes());
            return sig.verify(signedMessage.getSign());
        
        } catch (Exception ex) {
            Logger.log(Messages.getString("AuthenticationSilo.USER_AUTH_SIGN_VERIFICATION_FAILED"), ex); //$NON-NLS-1$
            return false;
        }  
	}
    
    
}
