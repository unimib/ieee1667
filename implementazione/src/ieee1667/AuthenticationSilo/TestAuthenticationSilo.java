package ieee1667.AuthenticationSilo;

import static org.junit.Assert.*;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.*;

import java.security.Security;
import java.util.Arrays;

import ieee1667.Messages;
import ieee1667.CertificateGenerator.CertificateGenerator;
import ieee1667.Logger.Logger;
import ieee1667.common.model.ASCh;
import ieee1667.common.model.ASCm;
import ieee1667.common.model.HCh;
import ieee1667.common.model.IndexCertificate;
import ieee1667.common.model.KeyPair;
import ieee1667.common.model.PublicASKP;
import ieee1667.common.model.TSDDescriptor;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import test.util.MessageMatcher;
import test.util.Utils;

import ch.qos.logback.classic.spi.LoggingEvent;
import ch.qos.logback.core.Appender;

@RunWith( MockitoJUnitRunner.class )
public class TestAuthenticationSilo {
	
	@Mock private Appender<LoggingEvent> mockAppender;
	
	private KeyPair kp;
	
	@Before
	public void setUp() {
		Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider ());
        when(mockAppender.getName()).thenReturn("MOCK");
	    Logger.initializeTestMode(mockAppender);
	    
	    kp = Utils.getKnownKeyPair();
	}
	
	
	@Test
	// 3.1.6
	public void shouldAuthenticateTSD () {
		
		// creo mock tsddescriptor
		TSDDescriptor mockTSD = mock(TSDDescriptor.class);
		when(mockTSD.getAskpKeyPair()).thenReturn(kp);
		
		// creo mock certificato Ascm valido
		ASCm ascm = mock(ASCm.class);
		PublicASKP pk = new PublicASKP();
		pk.setValue(kp.getPublicKey().getValue());
		when(ascm.getPublicKey()).thenReturn(pk);
		
		// Uso spy per far credere all'auth silo di avere un tsd valido anche se il tsd
		// non è completamente validato
		AuthenticationSilo spyAuthSilo = spy(new AuthenticationSilo());
		spyAuthSilo.initialize(mockTSD);
		stub(spyAuthSilo.isInitialized()).toReturn(true);
		boolean result = spyAuthSilo.requestTSDAuthentication(ascm);
		
		// verifico risultato
		assertTrue(result);	
		// verifico stampa su log
		verify(mockAppender).doAppend(argThat(new MessageMatcher(Messages.getString("AuthenticationSilo.AUTH_TSD_DONE"))));
	}
	
	@Test
	// 3.1.7
	public void shouldRefuseTSDAuthentication () {
		
		// creo mock tsddescriptor
		TSDDescriptor mockTSD = mock(TSDDescriptor.class);
		when(mockTSD.getAskpKeyPair()).thenReturn(kp);
		
		// creo mock certificato Ascm non valido usando una chiave pubblica non associata alla chiave privata
		// inserita nel tsd
		KeyPair kp2 = CertificateGenerator.generateAsymmetricKeyPair();
		ASCm ascm = mock(ASCm.class);
		PublicASKP pk = new PublicASKP();
		pk.setValue(kp2.getPublicKey().getValue());
		when(ascm.getPublicKey()).thenReturn(pk);
		
		// Uso spy per far credere all'auth silo di avere un tsd valido anche se il tsd
		// non è completamente validato
		AuthenticationSilo spyAuthSilo = spy(new AuthenticationSilo());
		spyAuthSilo.initialize(mockTSD);
		stub(spyAuthSilo.isInitialized()).toReturn(true);
		boolean result = spyAuthSilo.requestTSDAuthentication(ascm);
		
		// verifico risultato
		assertFalse(result);	
		// verifico stampa su log
		verify(mockAppender).doAppend(argThat(new MessageMatcher(Messages.getString("AuthenticationSilo.AUTH_TSD_FAILED"))));
	}	
	
	
	@Test
	// 3.1.8
	public void shouldAuthenticateAuthSilo () {
		
		// creo mock tsddescriptor
		TSDDescriptor mockTSD = mock(TSDDescriptor.class);
		when(mockTSD.getAskpKeyPair()).thenReturn(kp);
		
		// creo mock certificato Asch valido
		ASCh asch = mock(ASCh.class);
		when(asch.getPublicKey()).thenReturn(Utils.getKnownPublicASKP());
		
		// Uso spy per far credere all'auth silo di avere un tsd valido anche se il tsd
		// non è completamente validato
		AuthenticationSilo spyAuthSilo = spy(new AuthenticationSilo());
		spyAuthSilo.initialize(mockTSD);
		stub(spyAuthSilo.isInitialized()).toReturn(true);
		boolean result = spyAuthSilo.requestAuthSiloAuthentication(asch);
		
		// verifico risultato
		assertTrue(result);	
		// verifico stampa su log
		verify(mockAppender).doAppend(argThat(new MessageMatcher(Messages.getString("AuthenticationSilo.AUTH_AUTH_SILO_DONE"))));
	}
	
	@Test
	// 3.1.9
	public void shouldRefuseAuthSiloAuthentication () {
		// creo mock tsddescriptor
		TSDDescriptor mockTSD = mock(TSDDescriptor.class);
		when(mockTSD.getAskpKeyPair()).thenReturn(kp);
		
		// creo mock certificato Asch non valido usando una chiave pubblica non associata alla chiave privata
		// inserita nel tsd
		KeyPair kp2 = CertificateGenerator.generateAsymmetricKeyPair();
		ASCh asch = mock(ASCh.class);
		PublicASKP pk = new PublicASKP();
		pk.setValue(kp2.getPublicKey().getValue());
		when(asch.getPublicKey()).thenReturn(pk);
		
		// Uso spy per far credere all'auth silo di avere un tsd valido anche se il tsd
		// non è completamente validato
		AuthenticationSilo spyAuthSilo = spy(new AuthenticationSilo());
		spyAuthSilo.initialize(mockTSD);
		stub(spyAuthSilo.isInitialized()).toReturn(true);
		boolean result = spyAuthSilo.requestAuthSiloAuthentication(asch);
		
		// verifico risultato
		assertFalse(result);	
		// verifico stampa su log
		verify(mockAppender).doAppend(argThat(new MessageMatcher(Messages.getString("AuthenticationSilo.AUTH_AUTH_SILO_FAILED"))));
	}
	
	
	@Test
	// 3.1.10
	public void shouldAuthenticateUser () {
		// creo mock tsddescriptor
		TSDDescriptor mockTSD = mock(TSDDescriptor.class);
		
		HCh userHCh = mock(HCh.class);
		when(userHCh.getPublicKey()).thenReturn(kp.getPublicKey());
		when(userHCh.getUserID()).thenReturn("TESTHOST");
		
		IndexCertificate idxCert = mock(IndexCertificate.class);
		when(idxCert.getHostCertificates()).thenReturn(Arrays.asList(userHCh));
		when(mockTSD.getIndexCertificate()).thenReturn(idxCert);
		
		// Uso spy per far credere all'auth silo di avere un tsd valido anche se il tsd
		// non è completamente validato
		AuthenticationSilo spyAuthSilo = spy(new AuthenticationSilo());
		spyAuthSilo.initialize(mockTSD);
		stub(spyAuthSilo.isInitialized()).toReturn(true);
		boolean result = spyAuthSilo.requestUserAuthentication("TESTHOST", kp.getPrivateKey(), userHCh, 0, Utils.getAsymmetricKeyMethod());
		
		assertTrue(result);
		verify(mockAppender).doAppend(argThat(new MessageMatcher(Messages.getString("AuthenticationSilo.AUTH_USER_DONE"))));
	}
	
	@Test
	// 3.1.11
	public void shouldRefuseUserAuthentication () {
		// creo mock tsddescriptor
		TSDDescriptor mockTSD = mock(TSDDescriptor.class);
		
		HCh userHCh = mock(HCh.class);
		when(userHCh.getPublicKey()).thenReturn(kp.getPublicKey());
		when(userHCh.getUserID()).thenReturn("TESTHOST");
		
		IndexCertificate idxCert = mock(IndexCertificate.class);
		when(idxCert.getHostCertificates()).thenReturn(Arrays.asList(userHCh));
		when(mockTSD.getIndexCertificate()).thenReturn(idxCert);
		
		// Uso spy per far credere all'auth silo di avere un tsd valido anche se il tsd
		// non è completamente validato
		AuthenticationSilo spyAuthSilo = spy(new AuthenticationSilo());
		spyAuthSilo.initialize(mockTSD);
		stub(spyAuthSilo.isInitialized()).toReturn(true);
		
		//genero un altra coppia di chiavi asimmetriche
		KeyPair kp2 = CertificateGenerator.generateAsymmetricKeyPair();
		
		boolean result = spyAuthSilo.requestUserAuthentication("TESTHOST", kp2.getPrivateKey(), userHCh, 0, Utils.getAsymmetricKeyMethod());
		
		assertFalse(result);
		verify(mockAppender).doAppend(argThat(new MessageMatcher(Messages.getString("AuthenticationSilo.AUTH_USER_FAILED"))));
	}
	
	
	
	
}
