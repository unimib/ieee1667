package ieee1667.AuthenticationSilo.message;

/**
 * Messaggio di autenticazione per l'utente
 * @author mauro
 *
 */
public class HostAuthenticationMessage extends Message {
	protected int hchIndex;
	protected String userID;
	
	public HostAuthenticationMessage(int hchIndex, String userID) {
		super();
		this.hchIndex = hchIndex;
		this.userID = userID;
	}
	
	public String getMessage() {
		return super.getMessage() + " " + hchIndex + " " + userID;
	}

	public int getHchIndex() {
		return hchIndex;
	}

	public String getUserID() {
		return userID;
	}
	
	
	
	
}
