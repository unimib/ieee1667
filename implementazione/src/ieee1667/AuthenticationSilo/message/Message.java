package ieee1667.AuthenticationSilo.message;

import java.util.UUID;

/**
 * Classe astratta che rappresenta un generico messaggio
 * @author mauro
 */
public abstract class Message {
    
    protected String randomValue;

    /**
     * Costruttore di default, genera valore random
     */
    public Message() {
        this.randomValue = UUID.randomUUID().toString();
    }

    public String getRandomValue() {
        return randomValue;
    }
    
    /**
     * Torna il messaggio come stringa
     * @return il messaggio
     */
    public String getMessage() {
        return this.randomValue;
    }
}
