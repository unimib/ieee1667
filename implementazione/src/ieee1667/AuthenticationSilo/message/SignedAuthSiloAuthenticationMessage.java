package ieee1667.AuthenticationSilo.message;

/**
 * Messaggio firmato di autenticazione dell'Authentication Silo
 * @author mauro
 *
 */
public class SignedAuthSiloAuthenticationMessage extends AuthSiloAuthenticationMessage {
    private byte[] sign;

    
    public SignedAuthSiloAuthenticationMessage(AuthSiloAuthenticationMessage nonSignedMessage) {
        this.randomValue = nonSignedMessage.randomValue;
    }
    
    public byte[] getSign() {
        return sign;
    }

    public void setSign(byte[] sign) {
        this.sign = sign;
    }
}
