package ieee1667.AuthenticationSilo.message;

/**
 * Messaggio firmato di autenticazione dell'Authentication Silo
 * @author mauro
 *
 */
public class SignedHostAuthenticationMessage extends HostAuthenticationMessage {
    
	private byte[] sign;
	private String usedCryptographicMethod;
    
    public SignedHostAuthenticationMessage(HostAuthenticationMessage nonSignedMessage, String usedMethod) {
    	super(nonSignedMessage.hchIndex, nonSignedMessage.userID);
    	this.randomValue = nonSignedMessage.randomValue;
    	this.usedCryptographicMethod = usedMethod;
    }
    
    public byte[] getSign() {
        return sign;
    }

    public void setSign(byte[] sign) {
        this.sign = sign;
    }

	public String getUsedCryptographicMethod() {
		return usedCryptographicMethod;
	}
    
    
}
