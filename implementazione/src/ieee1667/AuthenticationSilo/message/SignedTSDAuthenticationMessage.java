package ieee1667.AuthenticationSilo.message;

/**
 * Messaggio firmato per l'autenticazione TSD
 * @author mauro
 */
public class SignedTSDAuthenticationMessage  extends TSDAuthenticationMessage{
    private byte[] sign;

    
    public SignedTSDAuthenticationMessage(TSDAuthenticationMessage nonSignedMessage) {
        this.randomValue = nonSignedMessage.randomValue;
    }
    
    public byte[] getSign() {
        return sign;
    }

    public void setSign(byte[] sign) {
        this.sign = sign;
    }
    
    
    
}
