package ieee1667.CertificateGenerator;

import java.security.AlgorithmParameters;
import java.security.Key;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

import javax.crypto.Cipher;
import javax.crypto.EncryptedPrivateKeyInfo;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.*;

import org.bouncycastle.util.encoders.Base64;

import ieee1667.Logger.Logger;
import ieee1667.common.model.HCh;
import ieee1667.common.model.KeyPair;
import ieee1667.common.model.PrivateKey;
import ieee1667.common.model.PublicKey;
import ieee1667.Constant;
import ieee1667.Messages;

public class CertificateGenerator {

	/**
	 * Generate a new HCh certificate
	 * 
	 * @param userID
	 *            the userid associated with the certificate
	 * @param password
	 *            the private password of the user
	 * @return un nuovo certificato HCh
	 */
	public static HCh generateHCh(String userID, String password) {

		if (userID == null) {
			Logger.log(Messages.getString("CertificateGenerator.CANT_CREATE_HCH"), "L'user id non pu� essere null"); //$NON-NLS-1$
			return null;
		}

		if (password == null) {
			Logger.log(Messages.getString("CertificateGenerator.CANT_CREATE_HCH"), "La password id non pu� essere null"); //$NON-NLS-1$
			return null;
		}

		// http://stackoverflow.com/questions/5127379/how-to-generate-a-rsa-keypair-with-a-privatekey-encrypted-with-password
		try {
			HCh hch = new HCh();
			hch.setUserID(userID);

			// ottengo una coppia di chiavi
			KeyPair kp = generateAsymmetricKeyPair();

			// ora devo criptare la chiave privata
			byte[] encodedprivkey = kp.getPrivateKey().getJavaVersion()
					.getEncoded();

			// cripto chiave
			Random random = new Random();
			byte[] salt = new byte[8];
			random.nextBytes(salt);

			PBEParameterSpec pbeParamSpec = new PBEParameterSpec(salt, 20);
			PBEKeySpec pbeKeySpec = new PBEKeySpec(password.toCharArray());
			SecretKeyFactory keyFac = SecretKeyFactory
					.getInstance(Constant.CRYPT_HCH_METHOD);
			SecretKey pbeKey = keyFac.generateSecret(pbeKeySpec);

			Cipher pbeCipher = Cipher.getInstance(Constant.CRYPT_HCH_METHOD);

			// Initialize PBE Cipher with key and parameters
			pbeCipher.init(Cipher.ENCRYPT_MODE, pbeKey, pbeParamSpec);

			// Encrypt the encoded Private Key with the PBE key
			byte[] ciphertext = pbeCipher.doFinal(encodedprivkey);

			// Now construct PKCS #8 EncryptedPrivateKeyInfo object
			AlgorithmParameters algparms = AlgorithmParameters
					.getInstance(Constant.CRYPT_HCH_METHOD);
			algparms.init(pbeParamSpec);
			EncryptedPrivateKeyInfo encinfo = new EncryptedPrivateKeyInfo(
					algparms, ciphertext);

			// and here we have it! a DER encoded PKCS#8 encrypted key!
			byte[] encryptedPkcs8 = encinfo.getEncoded();

			String privateString = new String(Base64.encode(encryptedPkcs8));

			kp.getPrivateKey().setValue(privateString);

			hch.setPublicKey(kp.getPublicKey());

			return hch;
		} catch (Exception e) {
			e.printStackTrace();
			Logger.log(
					Messages.getString("CertificateGenerator.CANT_CREATE_HCH"), e.getMessage()); //$NON-NLS-1$
			return null;
		}
	}

	/**
	 * Generate a keypair
	 * 
	 * @return una coppia di chiavi asimmetriche
	 */
	public static KeyPair generateAsymmetricKeyPair() {

		// genero keypair
		KeyPairGenerator kpg = null;

		try {
			kpg = KeyPairGenerator.getInstance("RSA"); //$NON-NLS-1$
		} catch (Exception e) {
			Logger.log(Messages
					.getString("CertificateGenerator.ASYM_KEY_ALGO_NOT_FOUND")); //$NON-NLS-1$
			return null;
		}

		kpg.initialize(1048);
		java.security.KeyPair kp = kpg.genKeyPair();
		Key publicKey = kp.getPublic();
		Key privateKey = kp.getPrivate();

		// trasformo in un formato memorizzabile
		String publicString = new String(Base64.encode(publicKey.getEncoded()));
		String privateString = new String(
				Base64.encode(privateKey.getEncoded()));

		// creo struttura chiave ieee1667
		PublicKey pubKey = new PublicKey();
		PrivateKey privKey = new PrivateKey();

		pubKey.setValue(publicString);
		privKey.setValue(privateString);

		KeyPair kpair = new KeyPair();
		kpair.setPrivateKey(privKey);
		kpair.setPublicKey(pubKey);

		return kpair;

	}

	/**
	 * Genera una chiave simmetrica per l'algoritmo passato come parametro
	 * 
	 * @param methodName
	 *            il nome dell'algoritmo
	 * @return una chiave simmetrica
	 */
	public static SecretKey generateSymmetricKey(String methodName) {
		String keyType = null;
		if (methodName.toLowerCase().contains("des")) { //$NON-NLS-1$
			keyType = "DES"; //$NON-NLS-1$
		} else if (methodName.toLowerCase().contains("ctr")) { //$NON-NLS-1$
			keyType = "CTR"; //$NON-NLS-1$
		} else if (methodName.toLowerCase().contains("pkcs5padding")) { //$NON-NLS-1$
			keyType = "PKCS5PADDING"; //$NON-NLS-1$
		} else if (methodName.toLowerCase().contains("aes")) {
			keyType = "AES"; //$NON-NLS-1$
		} else {
			keyType = methodName;
		}

		KeyGenerator generator;
		try {
			generator = KeyGenerator.getInstance(keyType);
		} catch (NoSuchAlgorithmException e) {
			Logger.log(
					Messages.getString("CertificateGenerator.CANT_GENERATE_SYM_KEY"), e.getMessage()); //$NON-NLS-1$
			return null;
		}

		SecretKey key = generator.generateKey();
		return key;
	}

}
