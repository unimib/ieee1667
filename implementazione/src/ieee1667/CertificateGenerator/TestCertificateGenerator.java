package ieee1667.CertificateGenerator;

import static org.junit.Assert.*;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.*;

import java.security.Security;

import javax.crypto.SecretKey;

import ieee1667.Messages;
import ieee1667.CertificateManager.CertificateManager;
import ieee1667.Logger.Logger;
import ieee1667.common.model.HCh;
import ieee1667.common.model.PrivateKey;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import test.util.MessageMatcher;

import ch.qos.logback.classic.spi.LoggingEvent;
import ch.qos.logback.core.Appender;

@RunWith( MockitoJUnitRunner.class )
public class TestCertificateGenerator {
	
	@Mock private Appender<LoggingEvent> mockAppender;
	
	@Before
	public void setUp() {
		Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider ());
        when(mockAppender.getName()).thenReturn("MOCK");
	    Logger.initializeTestMode(mockAppender);
	}
	
	
	@Test
	// 3.2.0
	public void shouldReturnSymmKey () {
		SecretKey key = CertificateGenerator.generateSymmetricKey("DES");
		assertTrue(key != null);
		assertEquals("DES", key.getAlgorithm());
	}
	
	@Test
	// 3.2.1
	public void shouldNotCreateSymmKey () {
		SecretKey key = CertificateGenerator.generateSymmetricKey("INVALID_METHOD");
		assertEquals(null, key);
		verify(mockAppender).doAppend(argThat(new MessageMatcher(Messages.getString("CertificateGenerator.CANT_GENERATE_SYM_KEY"))));
	}
	
	@Test
	// 3.2.2
	public void shouldCreateHCh () {
		HCh hch = CertificateGenerator.generateHCh("userID", "imapassword");
		// verifico che sia stato creato l'HCh
		assertTrue( hch != null );
		
		// verifico che la chiave privata sia stata effettivamente cifrata con la chiave passata come parametro
		CertificateManager certMan = new CertificateManager();
		certMan.initialize(null); // i metodi che utilizzeremo non hanno bisogno del tsd
		PrivateKey pk = certMan.getPrivateKeyForHCh(hch, "imapassword");
		assertTrue( pk != null );
	}
	
	@Test
	// 3.2.3
	public void shouldRefuseHChCreationOnNullUserID () {
		HCh hch = CertificateGenerator.generateHCh(null, "imapassword");
		assertEquals(null, hch);
		verify(mockAppender).doAppend(argThat(new MessageMatcher(Messages.getString("CertificateGenerator.CANT_CREATE_HCH"))));
	}

	@Test
	// 3.2.4
	public void shouldRefuseHChCreationOnNullPassword () {
		HCh hch = CertificateGenerator.generateHCh("userID", null);
		assertEquals(null, hch);
		verify(mockAppender).doAppend(argThat(new MessageMatcher(Messages.getString("CertificateGenerator.CANT_CREATE_HCH"))));
	}
	
}
