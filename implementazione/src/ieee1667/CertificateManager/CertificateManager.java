package ieee1667.CertificateManager;

import ieee1667.Logger.Logger;
import ieee1667.common.model.ASCh;
import ieee1667.common.model.ASCm;
import ieee1667.common.model.HCh;
import ieee1667.common.model.PrivateKey;
import ieee1667.common.model.PublicKey;
import ieee1667.common.model.TSDDescriptor;
import ieee1667.Constant;
import ieee1667.Messages;

import java.security.InvalidKeyException;
import java.security.Key;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.List;

import javax.crypto.Cipher;
import javax.crypto.EncryptedPrivateKeyInfo;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.*;

import org.bouncycastle.util.encoders.Base64;

/**
 * Manager, si occupa della gestione dei certificati
 * 
 * @author mauro
 */
public class CertificateManager {

	private static final String DELIMITER = "----";

	/**
	 * TSD utilizzato durante la sessione corrente
	 */
	private TSDDescriptor tsd;

	/**
	 * Indica se il componente � stato inizializzato
	 */
	private boolean isInitialized;

	/**
	 * Costruttore di default
	 */
	public CertificateManager() {
		this.isInitialized = false;
	}

	/**
	 * Inizializza il componente. Questo metodo deve essere chiamato prima di
	 * ogni altro metodo
	 * 
	 * @param tsd
	 *            il tsd utilizzato durante la sessione corrente
	 */
	public void initialize(TSDDescriptor tsd) {
		this.tsd = tsd;
		this.isInitialized = true;
	}

	/**
	 * Resetta il componente
	 */
	public void reset() {
		this.tsd = null;
		this.isInitialized = false;
	}

	/**
	 * Metodo utilizzato per verificare se l'authentication silo � inizializzato
	 * 
	 * @return true se l'authentication silo è inizializzato, false altrimenti
	 */
	public boolean isInitialized() {
		return this.isInitialized;
	}

	/**
	 * Torna il certificato ASCm per il tsd corrente.
	 * Il componente deve essere inizializzato (vedi {@link #isInitialized()}
	 * 
	 * @return il certificato ASCm
	 */
	public ASCm getASCm() {
		if (!this.isInitialized()) {
			Logger.log(Messages.getString("CertificateManager.CERTIFICATE_MANAGER_NOT_INITIALIZED")); //$NON-NLS-1$
			return null;
		}
		return this.tsd.getIndexCertificate().getAscm();
	}

	/**
	 * Torna la lista dei certificati HCh per il tsd corrente
	 * 
	 * @return la lista dei certificati HCh
	 */
	public List<HCh> getHChList() {
		if (!this.isInitialized()) {
			Logger.log(Messages.getString("CertificateManager.CERTIFICATE_MANAGER_NOT_INITIALIZED")); //$NON-NLS-1$
			return null;
		}
		return this.tsd.getIndexCertificate().getHostCertificates();
	}

	/**
	 * Torna il certificato ASCh per il tsd corrente.
	 * Il componente deve essere inizializzato (vedi {@link #isInitialized()}
	 * 
	 * @return il certificato ASCh
	 */
	public ASCh getASCh() {
		if (!this.isInitialized()) {
			Logger.log(Messages.getString("CertificateManager.CERTIFICATE_MANAGER_NOT_INITIALIZED")); //$NON-NLS-1$
			return null;
		}
		return this.tsd.getIndexCertificate().getAsch();
	}

	/**
	 * Torna la versione in chiaro della chiave privata legata ad un certificato
	 * HCh
	 * 
	 * @param hch
	 *            il certificato
	 * @param password
	 *            la password con cui decifrare la chiave
	 * @return la chiave in chiaro
	 */
	public PrivateKey getPrivateKeyForHCh(HCh hch, String password) {
		try {

			// ottengo la chiave privata criptata
			PrivateKey pk = hch.getPublicKey().getKeyPair().getPrivateKey();

			// ottengo il suo valore "serializzato"
			byte[] encodedPk = Base64.decode(pk.getValue());

			// lo decifro

			EncryptedPrivateKeyInfo spec = new EncryptedPrivateKeyInfo(
					encodedPk);

			PBEKeySpec pbeKeySpec = new PBEKeySpec(password.toCharArray());
			SecretKeyFactory keyFac = SecretKeyFactory
					.getInstance(Constant.CRYPT_HCH_METHOD);
			SecretKey pbeKey = keyFac.generateSecret(pbeKeySpec);

			PKCS8EncodedKeySpec pvkKeySpec = spec.getKeySpec(pbeKey);

			// creo una nuova chiave privata in chiaro
			String serPrivKey = new String(Base64.encode(pvkKeySpec
					.getEncoded()));

			PrivateKey plainPk = new PrivateKey();
			plainPk.setValue(serPrivKey);

			return plainPk;
		} catch (InvalidKeyException e) {

			Logger.log(
					Messages.getString("CertificateManager.USER_KEY_NOT_VALID"), password); //$NON-NLS-1$
			return null;

		} catch (Exception e) {
			Logger.log(
					Messages.getString("CertificateManager.CANT_KEY_USER_PRIVATE_KEY"), e.getMessage()); //$NON-NLS-1$
			return null;
		}
	}

	/**
	 * Decifra un dato
	 * 
	 * @param data
	 *            il dato da decifrare, codificata in base64
	 * @param key
	 *            la chiave simmetrica da utilizzare
	 * @param method
	 *            il metodo da utilizzare per decifrare la chiave
	 * @return il dato decifrato
	 */
	public String decryptData(String data, Key key, String method) {
		try {
			Logger.log(Messages.getString("CertificateManager.DECRYPT_DATA")); //$NON-NLS-1$
			Cipher cipher = Cipher.getInstance(method, Constant.PROVIDER); //$NON-NLS-1$
			cipher.init(Cipher.DECRYPT_MODE, key);
			byte[] enc = cipher.doFinal(Base64.decode(data));
			return new String(enc);

		} catch (Exception e) {
			Logger.log(
					Messages.getString("CertificateManager.CANT_DECRYPT_DATA"), data, key, method, e.getMessage()); //$NON-NLS-1$
			return null;
		}
	}

	/**
	 * Cifra un dato
	 * 
	 * @param data
	 *            il dato da cifrare
	 * @param key
	 *            la chiave simmetrica da utilizzare
	 * @param method
	 *            il metodo da utilizzare per cifrare la chiave
	 * @return il dato cifrato, codificato in base64
	 */
	public String encryptData(String data, Key key, String method) {
		try {
			Logger.log(
					Messages.getString("CertificateManager.ENCRYPT_DATA"), data); //$NON-NLS-1$
			Cipher cipher = Cipher.getInstance(method, Constant.PROVIDER); //$NON-NLS-1$
			cipher.init(Cipher.ENCRYPT_MODE, key);
			byte[] res = cipher.doFinal(data.getBytes());
			return new String(Base64.encode(res));

		} catch (Exception e) {
			Logger.log(
					Messages.getString("CertificateManager.CANT_CRYPT_DATA"), data, key, method, e.getMessage()); //$NON-NLS-1$
			return null;
		}
	}

	/**
	 * Cifra la chiave simmetrica con una chiave pubblica utilizzando un
	 * algoritmo asimmetrico
	 * 
	 * @param symmetricKey
	 *            la chiave simmetrica
	 * @param publicKey
	 *            la chiave pubblica
	 * @return una stringa che rappresenta la chiave cifrata codificata in base64
	 */
	public String encryptSymmetricKey(Key symmetricKey, PublicKey publicKey) {
		try {
			Logger.log(Messages
					.getString("CertificateManager.ENCRYPT_SYMM_KEY")); //$NON-NLS-1$
			Cipher cipher = Cipher.getInstance(
					Constant.SYMMETRIC_KEY_ENRCYPTION, Constant.PROVIDER);
			cipher.init(Cipher.ENCRYPT_MODE, publicKey.getJavaVersion());
			byte[] enc = cipher.doFinal(symmetricKey.getEncoded());
			// memorizzo nella rappresentazione della chiave per che tipo di
			// algoritmo la chiave è stata creata
			return new String(Base64.encode(enc)) + DELIMITER
					+ symmetricKey.getAlgorithm();

		} catch (Exception e) {
			Logger.log(
					Messages.getString("CertificateManager.CANT_CRYPT_SYMM_KEY"), symmetricKey, publicKey, e.getMessage()); //$NON-NLS-1$
			return null;
		}
	}

	
	/**
	 * Decifra la chiave simmetrica con una chiave privata utilizzando un
	 * algoritmo asimmetrico
	 *
	 * @param keyRepresentation la rappresentazione della chiave (codificata in Base64)
	 * @param privateKey la chiave privata
	 * @return la chiave simmetrica
	 */
	public SecretKey decryptSymmetricKey(String keyRepresentation,
			PrivateKey privateKey) {
		try {
			Logger.log(Messages
					.getString("CertificateManager.DECRYPT_SYMM_KEY")); //$NON-NLS-1$
			String[] pieces = keyRepresentation.split(DELIMITER);

			String cryptedKey = pieces[0];
			String algorithm = pieces[1];

			Cipher cipher = Cipher.getInstance(
					Constant.SYMMETRIC_KEY_ENRCYPTION, Constant.PROVIDER); //$NON-NLS-1$
			cipher.init(Cipher.DECRYPT_MODE, (Key) privateKey.getJavaVersion());
			byte[] res = cipher.doFinal(Base64.decode(cryptedKey));
			return new SecretKeySpec(res, algorithm);

		} catch (Exception e) {
			Logger.log(
					Messages.getString("CertificateManager.CANT_DECRYPT_SYMM_KEY"), keyRepresentation, privateKey, e.getMessage()); //$NON-NLS-1$
			return null;
		}
	}

}
