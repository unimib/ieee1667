package ieee1667.CertificateManager;

import static org.junit.Assert.*;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.*;

import java.security.Security;
import java.util.ArrayList;
import java.util.List;

import javax.crypto.SecretKey;

import ieee1667.Messages;
import ieee1667.CertificateGenerator.CertificateGenerator;
import ieee1667.Logger.Logger;
import ieee1667.common.model.ASCh;
import ieee1667.common.model.ASCm;
import ieee1667.common.model.HCh;
import ieee1667.common.model.IndexCertificate;
import ieee1667.common.model.KeyPair;
import ieee1667.common.model.PrivateKey;
import ieee1667.common.model.PublicASKP;
import ieee1667.common.model.TSDDescriptor;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import test.util.MessageMatcher;
import test.util.Utils;

import ch.qos.logback.classic.spi.LoggingEvent;
import ch.qos.logback.core.Appender;

@RunWith( MockitoJUnitRunner.class )
public class TestCertificateManager {
	
	@Mock private Appender<LoggingEvent> mockAppender;
	private KeyPair kp;
	
	@Before
	public void setUp() {
		Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider ());
        when(mockAppender.getName()).thenReturn("MOCK");
	    Logger.initializeTestMode(mockAppender);
	    kp = Utils.getKnownKeyPair();
	}
	
	@Test
	// 3.3.6
	public void shouldReturnASCh () {
		// creo mock tsddescriptor
		TSDDescriptor mockTSD = mock(TSDDescriptor.class);
		
		// creo mock certificato Asch valido
		ASCh asch = mock(ASCh.class);
		when(asch.getPublicKey()).thenReturn(Utils.getKnownPublicASKP());
		
		IndexCertificate idxCert = mock(IndexCertificate.class);
		when(idxCert.getAsch()).thenReturn(asch);
		when(mockTSD.getIndexCertificate()).thenReturn(idxCert);
		
		// Uso spy per far credere la certificatemanager di avere un tsd valido anche se il tsd
		// non � completamente validato
		CertificateManager spyCertManager = spy(new CertificateManager());
		spyCertManager.initialize(mockTSD);
		stub(spyCertManager.isInitialized()).toReturn(true);
		
		ASCh retASCh = spyCertManager.getASCh();
		
		assertEquals(asch, retASCh);
	}
	
	
	@Test
	// 3.3.7
	public void shouldReturnASCm () {
		// creo mock tsddescriptor
		TSDDescriptor mockTSD = mock(TSDDescriptor.class);
		
		// creo mock certificato Asch valido
		ASCm ascm = mock(ASCm.class);
		when(ascm.getPublicKey()).thenReturn(Utils.getKnownPublicASKP());
		
		IndexCertificate idxCert = mock(IndexCertificate.class);
		when(idxCert.getAscm()).thenReturn(ascm);
		when(mockTSD.getIndexCertificate()).thenReturn(idxCert);
		
		// Uso spy per far credere la certificatemanager di avere un tsd valido anche se il tsd
		// non � completamente validato
		CertificateManager spyCertManager = spy(new CertificateManager());
		spyCertManager.initialize(mockTSD);
		stub(spyCertManager.isInitialized()).toReturn(true);
		
		ASCm retASCm = spyCertManager.getASCm();
		
		assertEquals(ascm, retASCm);
	}

	@Test
	// 3.3.8
	public void shouldReturnHChList () {
		// creo mock tsddescriptor
		TSDDescriptor mockTSD = mock(TSDDescriptor.class);
		
		// creo una lista di HCh fittizzi
		List<HCh> hchList = new ArrayList<HCh>();
		for(int i = 0; i<10; i++) {
			HCh hch = mock(HCh.class);
			when(hch.getUserID()).thenReturn("host"+i);
			when(hch.getPublicKey()).thenReturn(kp.getPublicKey());
		}
		
		IndexCertificate idxCert = mock(IndexCertificate.class);
		when(idxCert.getHostCertificates()).thenReturn(hchList);
		when(mockTSD.getIndexCertificate()).thenReturn(idxCert);
		
		// Uso spy per far credere la certificatemanager di avere un tsd valido anche se il tsd
		// non � completamente validato
		CertificateManager spyCertManager = spy(new CertificateManager());
		spyCertManager.initialize(mockTSD);
		stub(spyCertManager.isInitialized()).toReturn(true);
		
		List<HCh> retHChList = spyCertManager.getHChList();
		assertEquals(hchList, retHChList);
	}
	
	
	@Test
	// 3.3.11
	public void shouldReturnPrivateKeyForHCh () {
		HCh hch = CertificateGenerator.generateHCh("userID", "imapassword");
		
		CertificateManager certManager = new CertificateManager();
		certManager.initialize(null); // il metodo che testeremo non usa il tsd
		
		PrivateKey pk = certManager.getPrivateKeyForHCh(hch, "imapassword");
		
		assertTrue( pk != null );
	}
	
	@Test
	// 3.3.12
	public void shouldFailReturnPrivateKeyForHChOnWrongPassword () {
		HCh hch = CertificateGenerator.generateHCh("userID", "imapassword");
		
		CertificateManager certManager = new CertificateManager();
		certManager.initialize(null); // il metodo che testeremo non usa il tsd
		
		PrivateKey pk = certManager.getPrivateKeyForHCh(hch, "imapassword2");
		
		assertEquals(null, pk);
		verify(mockAppender).doAppend(argThat(new MessageMatcher(Messages.getString("CertificateManager.USER_KEY_NOT_VALID"))));
	}
	
	@Test
	// 3.2.13
	public void shouldDecryptData () {
		// stringa "pre-calcolata" usando la chiave fornita dalla classe utils
		String data = "maLTPm3fmpgOBWEYXDiAluHooJ5UpJhP";
		String originalString = "stringa da criptare";
		SecretKey key = Utils.getKnownSecretKey();
				
		CertificateManager certManager = new CertificateManager();
		certManager.initialize(null); // il metodo che testeremo non usa il tsd
		
		String retStr = certManager.decryptData(data, key, Utils.getSymmetricKeyMethod());
		
		assertEquals(originalString, retStr);
	}
	
	@Test
	// 3.2.14
	public void shouldFailDecryptOnWrongAlghoritm () {
		// stringa "pre-calcolata" usando la chiave fornita dalla classe utils
		String data = "maLTPm3fmpgOBWEYXDiAluHooJ5UpJhP";
		SecretKey key = Utils.getKnownSecretKey();
				
		CertificateManager certManager = new CertificateManager();
		certManager.initialize(null); // il metodo che testeremo non usa il tsd
		
		String retStr = certManager.decryptData(data, key, "AES");
		
		assertEquals(null, retStr);
		verify(mockAppender).doAppend(argThat(new MessageMatcher(Messages.getString("CertificateManager.CANT_DECRYPT_DATA"))));
	}
	
	@Test
	// 3.2.15
	public void shouldEncryptData () {
		// stringa "pre-calcolata" usando la chiave fornita dalla classe utils
		String data = "maLTPm3fmpgOBWEYXDiAluHooJ5UpJhP";
		String originalString = "stringa da criptare";
		SecretKey key = Utils.getKnownSecretKey();
				
		CertificateManager certManager = new CertificateManager();
		certManager.initialize(null); // il metodo che testeremo non usa il tsd
		
		String retStr = certManager.encryptData(originalString, key, Utils.getSymmetricKeyMethod());
		
		assertEquals(data, retStr);
	}
	
	@Test
	// 3.2.16
	public void shouldFailEncrypOnWrongAlgorithm () {
		// stringa "pre-calcolata" usando la chiave fornita dalla classe utils
		String originalString = "stringa da criptare";
		SecretKey key = Utils.getKnownSecretKey();
				
		CertificateManager certManager = new CertificateManager();
		certManager.initialize(null); // il metodo che testeremo non usa il tsd
		
		String retStr = certManager.encryptData(originalString, key, "AES");
		
		assertEquals(null, retStr);
		verify(mockAppender).doAppend(argThat(new MessageMatcher(Messages.getString("CertificateManager.CANT_CRYPT_DATA"))));
	}

	
	@Test
	// 3.2.17
	public void shouldDecryptSymmetricKey () {
		String keyRepresentation = "VVjOAst8JoHzD/BlK3iqrd+idzjscFNzT04YIIMBx5dxCGZseX9H9pohk0OZn47eG1kyuyVzSm7a3qO1vx4LxQQBh2puHsbuZxdxjWVs4dlytKpScH7+j5ksHdUt53gJLPcKF1Q2Bf1kKVU9I1Bon1LWDU3xgweDVtIyOauzBUgUq4s=----DES";
		
		CertificateManager certManager = new CertificateManager();
		certManager.initialize(null); // il metodo che testeremo non usa il tsd
		
		SecretKey symmKey = certManager.decryptSymmetricKey(keyRepresentation, Utils.getKnownPrivateKey());
		
		assertEquals(Utils.getKnownSecretKey(), symmKey);		
	}
	
	@Test
	// 3.2.18
	public void shouldFailsDecryptSymmetricKeyOnWrongKeyRepresentation () {
		String keyRepresentation = "k0OZn47eG1kyuyVzSm7a3qOzBUgUq4s";
		
		CertificateManager certManager = new CertificateManager();
		certManager.initialize(null); // il metodo che testeremo non usa il tsd
		
		SecretKey symmKey = certManager.decryptSymmetricKey(keyRepresentation, Utils.getKnownPrivateKey());
		
		assertEquals(null, symmKey);	
		verify(mockAppender).doAppend(argThat(new MessageMatcher(Messages.getString("CertificateManager.CANT_DECRYPT_SYMM_KEY"))));
	}
	
	@Test
	// 3.2.19
	public void shouldEncryptSymmetricKey () {
		String expectedStr = "VVjOAst8JoHzD/BlK3iqrd+idzjscFNzT04YIIMBx5dxCGZseX9H9pohk0OZn47eG1kyuyVzSm7a3qO1vx4LxQQBh2puHsbuZxdxjWVs4dlytKpScH7+j5ksHdUt53gJLPcKF1Q2Bf1kKVU9I1Bon1LWDU3xgweDVtIyOauzBUgUq4s=----DES";
		
		CertificateManager certManager = new CertificateManager();
		certManager.initialize(null); // il metodo che testeremo non usa il tsd
		
		String retStr = certManager.encryptSymmetricKey(Utils.getKnownSecretKey(), Utils.getKnownPublicKey());
		
		assertEquals(expectedStr, retStr);
	}
	
	@Test
	// 3.2.20
	public void shouldFailEncryptSymmetricKeyOnWrongSymmKey () {
		
		CertificateManager certManager = new CertificateManager();
		certManager.initialize(null); // il metodo che testeremo non usa il tsd
		
		String retStr = certManager.encryptSymmetricKey(null, Utils.getKnownPublicKey());
		
		assertEquals(null, retStr);
	}

	@Test
	// 3.2.21
	public void shouldFailEncryptSymmetricKeyOnWrongPublicKey () {
		CertificateManager certManager = new CertificateManager();
		certManager.initialize(null); // il metodo che testeremo non usa il tsd
		
		String retStr = certManager.encryptSymmetricKey(Utils.getKnownSecretKey(), null);
		
		assertEquals(null, retStr);
	}
}
