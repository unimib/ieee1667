package ieee1667;

public class Constant {

    /**
     * Enumeratore utilizzato per gestire le operazioni della sessione utente
     * @author Mauro
     *
     */
    public static enum USER_OPERATION {
        READ, WRITE, NONE
    }
    
    /**
     * Enumeratore utilizzato per gestire la tipologia di operazione iniziale
     * da svolgere
     * @author Mauro
     *
     */
    public static enum OPERATION_CHOICE {
    	USER_SESSION,
    	ADMIN_SESSION,
    	MANAGE_TSD,
    	NONE
    }
    
    /**
     * Provider dei metodi crittografici
     */
    public static final String PROVIDER = "BC";
    
    /**
     * Metodo crittografico utilizzato per cifrare la chiave simmetrica
     * durante l'inizializzazione del protocollo di scrittura/lettura
     */
	public static final String SYMMETRIC_KEY_ENRCYPTION = "RSA";
	
	/**
	 * Metodo crittografico utilizzato per cifrare la chiave privata dei certificati
	 * HCh
	 */
	public static final String CRYPT_HCH_METHOD = "PBEWithSHA1AndDESede"; //$NON-NLS-1$
	
	/**
	 * Metodo crittografico utilizzato per l'autenticazione del TSD e AuthSilo
	 */
	public static final String TSD_AUTH_METHOD = "SHA224withRSA/PSS"; //$NON-NLS-1$
	
}
