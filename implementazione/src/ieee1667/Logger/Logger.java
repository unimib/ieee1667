package ieee1667.Logger;

import org.slf4j.LoggerFactory;

import ch.qos.logback.core.Appender;

/**
 * Questa classe rappresenta una interfaccia per la creazione dei log
 * @author mauro
 */
public class Logger {
    
    private static org.slf4j.Logger sharedLogger;
    private static final String LOGGER_NAME = "IEEE1667";
    
    
    /**
     * Inizializza il componente logger
     */
    public static void initialize () {
        Logger.sharedLogger = LoggerFactory.getLogger(LOGGER_NAME);        
    }
    

    /**
     * Inizializza il componente logger in una modalità che permette di testare
     * @param appender l'appender (solitamente viene passato un mock)
     */
	public static void initializeTestMode(Appender appender) {
		Logger.sharedLogger = (ch.qos.logback.classic.Logger) LoggerFactory.getLogger("TEST");
		((ch.qos.logback.classic.Logger)Logger.sharedLogger).addAppender(appender);
	}
    
    
    /**
     * Metodo per aggiungere un messaggio al log. E' necessario 
     * chiamare il metodo initialize prima di poter utilizzare tale metodo
     * @param message il messaggio da aggiungere
     * @param params (opzionale) lista di oggetti che vengono inseriti nel messaggio al posto del placeholder "{}"
     */
    public static void log(String message, Object... params) {
        if (Logger.sharedLogger != null) {
            Logger.sharedLogger.info(message, params);
        }
    }
}
