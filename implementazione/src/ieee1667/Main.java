package ieee1667;

import ieee1667.Logger.Logger;
import ieee1667.SimulatorManager.SimulatorManager;
import java.security.Security;

/**
 * Main class dell'applicazione
 * @author mauro
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider ());
        Logger.initialize();
        SimulatorManager simulatorManager = new SimulatorManager();
        new Thread(simulatorManager).start();
    }
}
