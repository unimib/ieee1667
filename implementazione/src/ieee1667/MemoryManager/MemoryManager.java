package ieee1667.MemoryManager;

import ieee1667.Messages;
import ieee1667.Logger.Logger;
import ieee1667.SimulatorManager.SimulatorManager;
import ieee1667.common.model.PrivateKey;
import ieee1667.common.model.TSDDescriptor;
import ieee1667.common.model.UserIndexMemory;

import java.util.List;

import javax.crypto.SecretKey;

/**
 * Manager, si occupa della lettura/scrittura sul dispositivo
 * 
 * @author mauro
 */
public class MemoryManager {

	/**
	 * TSD utilizzato durante la sessione corrente
	 */
	private TSDDescriptor tsd;

	/**
	 * Indica se il componente � stato inizializzato
	 */
	private boolean isInitialized;

	/**
	 * Chiave privata asimmetrica utilizzata per scambiarsi la chiave simmetrica
	 */
	private PrivateKey asymmetricPrivateKey;

	/**
	 * Chiave simmetrica utilizzata per criptare la trasmissione
	 */
	private SecretKey symmetricKey;

	/**
	 * Il metodo crittografico simmetrico da utilizzare per la cifratura della
	 * trasmissione
	 */
	private String symmetricMethod;

	/**
	 * Riferimento al simulator manager. Viene utilizzato ad esempio per
	 * richiedere di decifrare dei dati. MemoryManager CertificateManager non si
	 * conoscono. SimulatorManager fa da "collegamento"
	 */
	private SimulatorManager simulatorManager;

	/**
	 * Costruttore di default
	 */
	public MemoryManager(SimulatorManager simManager) {
		this.isInitialized = false;
		this.simulatorManager = simManager;
	}

	/**
	 * Inizializza il componente. Questo metodo deve essere chiamato prima di
	 * ogni altro metodo
	 * 
	 * @param tsd
	 *            il tsd utilizzato durante la sessione corrente
	 */
	public void initialize(TSDDescriptor tsd) {
		this.tsd = tsd;
		this.isInitialized = true;
	}

	/**
	 * Resetta il componente
	 */
	public void reset() {
		this.tsd = null;
		this.isInitialized = false;
		this.symmetricKey = null;
		this.symmetricMethod = null;
	}

	/**
	 * Metodo utilizzato per verificare se l'authentication silo � inizializzato
	 * 
	 * @return true se l'authentication silo � inizializzato, false altrimenti
	 */
	public boolean isInitialized() {
		return this.isInitialized;
	}

	/**
	 * Legge un dato in memoria.
	 * Il componente deve essere inizializzato (vedi {@link #isInitialized()}
	 * 
	 * @param idx
	 *            l'index della cella di memoria da cui leggere il valore
	 * @param userID
	 *            l'id utente da utilizzare in fase di lettura
	 * @return il valore della cella di memoria
	 * @throws Exception
	 *             viene generata una eccezione se l'utente non ha i permessi
	 *             per leggere una determinata area di memoria
	 */
	public String readMemory(int idx, String userID) throws Exception {

		if (!this.isInitialized()) {
			Logger.log(Messages.getString("MemoryManager.NOT_INITIALIZED")); //$NON-NLS-1$
			throw new Exception("memory manager non inizializzato");
		}

		// 1. controllo se esiste una lista permessi per l'utente
		UserIndexMemory idxMem = this.tsd.getUserIndexMemory().get(userID);

		if (idxMem == null) {
			Logger.log(Messages
					.getString("MemoryManager.USER_PERMISSION_NOT_SET")); //$NON-NLS-1$
			throw new Exception("Questo utente non ha permessi specificati per il tsd"); //$NON-NLS-1$
		}

		// 2. controllo se l'utente � abilitato
		List<Integer> readIdx = idxMem.getRead();

		if (!readIdx.contains(idx)) {
			Logger.log(Messages.getString("MemoryManager.USER_HAS_NOT_READ_PERMISSION"), idx, readIdx); //$NON-NLS-1$
			throw new Exception("utente non abilitato alla lettura dell'indirizzo"); //$NON-NLS-1$
		}
		// 3. prendo valore
		String value = this.tsd.getMemory().getMemory().get(idx);

		// 4. lo cripto
		return this.simulatorManager.requestSymmetricEncryption(value, this.symmetricKey, this.symmetricMethod);

	}

	/**
	 * Scrive un dato in memoria.
	 * Il componente deve essere inizializzato (vedi {@link #isInitialized()}
	 * 
	 * @param idx
	 *            l'indice dell'area di memoria su cui scrivere il nuovo valore
	 * @param cryptedValue
	 *            il nuovo valore da scrivere, cifrato
	 * @param userID
	 *            l'userID da utilizzare in fase di scrittura
	 * @throws Exception
	 *             viene generata una eccezione se l'utente specificato non ha i
	 *             permessi necessari per scrivere nella cella di memoria
	 */
	public void writeMemory(int idx, String cryptedValue, String userID)
			throws Exception {
		if (!this.isInitialized()) {
			Logger.log(Messages.getString("MemoryManager.NOT_INITIALIZED")); //$NON-NLS-1$
			throw new Exception("memory manager non inizializzato");
		}

		// 1. controllo se esiste una lista permessi per l'utente
		UserIndexMemory idxMem = this.tsd.getUserIndexMemory().get(userID);

		if (idxMem == null) {
			Logger.log(Messages.getString("MemoryManager.USER_PERMISSION_NOT_SET")); //$NON-NLS-1$
			throw new Exception("Questo utente non ha permessi specificati per il tsd"); //$NON-NLS-1$
		}

		// 2. controllo se l'utente � abilitato
		List<Integer> writeIdx = idxMem.getWrite();

		if (!writeIdx.contains(idx)) {
			Logger.log(
					Messages.getString("MemoryManager.USER_HAS_NOT_WRITE_PERMISSION"), idx, writeIdx); //$NON-NLS-1$
			throw new Exception("utente non abilitato alla scrittura dell'indirizzo"); //$NON-NLS-1$
		}

		// 3. decifro il valore
		String newValue = this.simulatorManager.requestSymmetricDecrypt(
				cryptedValue, this.symmetricKey, this.symmetricMethod);

		if (newValue == null) {
			Logger.log(Messages.getString("MemoryManager.CANT_DECRYPT_NEW_VALUE")); //$NON-NLS-1$
			throw new Exception("Impossibile decifrare il nuovo valore"); //$NON-NLS-1$
		}

		// 3. scrivo nuovo valore
		this.tsd.getMemory().getMemory().put(idx, newValue);
		Logger.log(Messages.getString("MemoryManager.WRITE_OPERATION_DONE"), this.tsd.getMemory().getMemory()); //$NON-NLS-1$
	}

	/**
	 * Imposta la chiave privata della coppia asimmetrica utilizzata per
	 * scambiarsi la chiave simmetrica
	 * 
	 * @param privateKey
	 */
	public void setPrivateKey(PrivateKey privateKey) {
		this.asymmetricPrivateKey = privateKey;
	}

	/**
	 * Imposta una chiave simmetrica per il futuro scambio di dati
	 * 
	 * @param keyRepresentation
	 *            la rappresentazione della chiave simmetrica, cifrata con la chiave pubblica corrispondente a quella privata impostata nel manager 
	 * @param methodName
	 *            il metodo crittografico da utilizzare con la chiave simmetrica
	 */
	public boolean sendCipherSymmetricKey(String keyRepresentation, String methodName) {
		SecretKey symmKey = this.simulatorManager.DeChiper(keyRepresentation,
				this.asymmetricPrivateKey);
		if (symmKey == null) {
			Logger.log(Messages
					.getString("MemoryManager.CANT_DECRYPT_SYMM_KEY"));
			return false;
		} else {
			this.symmetricKey = symmKey;
			this.symmetricMethod = methodName;
			return true;
		}

	}

}
