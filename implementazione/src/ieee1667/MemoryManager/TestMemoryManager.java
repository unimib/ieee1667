package ieee1667.MemoryManager;

import static org.junit.Assert.*;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.*;

import java.security.Security;
import java.util.Arrays;
import java.util.HashMap;
import java.util.TreeMap;

import javax.crypto.SecretKey;

import ieee1667.Messages;
import ieee1667.CertificateGenerator.CertificateGenerator;
import ieee1667.CertificateManager.CertificateManager;
import ieee1667.Logger.Logger;
import ieee1667.SimulatorManager.SimulatorManager;
import ieee1667.common.model.TSDDescriptor;
import ieee1667.common.model.TSDMemory;
import ieee1667.common.model.UserIndexMemory;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import test.util.MessageMatcher;
import test.util.Utils;

import ch.qos.logback.classic.spi.LoggingEvent;
import ch.qos.logback.core.Appender;

@RunWith(MockitoJUnitRunner.class)
public class TestMemoryManager {

	@Mock
	private Appender<LoggingEvent> mockAppender;

	@Mock
	private TSDDescriptor mockTSD;
	
	@Mock
	private TSDMemory mockTSDMem; // dichiarata variabile d'istanza per poter effetturare i test di scrittura
	
	@Rule
	public ExpectedException expectedEx = ExpectedException.none();
	
	@Before
	public void setUp() {
		Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
		when(mockAppender.getName()).thenReturn("MOCK");
		Logger.initializeTestMode(mockAppender);
		
		
		// creo mock tsd usato nei test lettura/scrittura
		// creo mock permessi utente
		UserIndexMemory mockUsrIdx = mock(UserIndexMemory.class);
		when(mockUsrIdx.getRead()).thenReturn(Arrays.asList(0,1,2));
		when(mockUsrIdx.getWrite()).thenReturn(Arrays.asList(0));
		HashMap<String, UserIndexMemory> userIndexMemMap = new HashMap<String, UserIndexMemory>();
		userIndexMemMap.put("HOST1", mockUsrIdx);
		
		// creo mock memoria
		TreeMap<Integer, String> map = new TreeMap<Integer, String>();
		for(int i=0; i<5; i++) {
			map.put(i, "memoria " + i);
		}
		when(mockTSDMem.getMemory()).thenReturn(map);
		
		// creo mock del tsd
		when(mockTSD.getMemory()).thenReturn(mockTSDMem);
		when(mockTSD.getUserIndexMemory()).thenReturn(userIndexMemMap);
	}

	@Test
	// 3.5.3
	public void shouldSetSymmetricKey() {

		// ottengo una versione cifrata della chiave simmetrica
		CertificateManager certMan = new CertificateManager();
		certMan.initialize(null); // il metodo utilizzato non usa alcun TSD
		String keyRepresentation = certMan.encryptSymmetricKey(
				Utils.getKnownSecretKey(), Utils.getKnownPublicKey());

		SimulatorManager simManager = new SimulatorManager();
		// memoryManager necessita del simulatorManager per decifrare la chiave
		MemoryManager memManager = new MemoryManager(simManager);
		memManager.initialize(null); // non uso il tsd
		memManager.setPrivateKey(Utils.getKnownPrivateKey());

		boolean res = memManager.sendCipherSymmetricKey(keyRepresentation, Utils.getSymmetricKeyMethod());

		assertTrue(res);
	}

	@Test
	// 3.5.4
	public void shouldFailSetSymmetricKeyOnPrivateKeyNotSet() {
		// ottengo una versione cifrata della chiave simmetrica
		CertificateManager certMan = new CertificateManager();
		certMan.initialize(null); // il metodo utilizzato non usa alcun TSD
		String keyRepresentation = certMan.encryptSymmetricKey(Utils.getKnownSecretKey(), Utils.getKnownPublicKey());

		SimulatorManager simManager = new SimulatorManager();
		// memoryManager necessita del simulatorManager per decifrare la chiave
		MemoryManager memManager = new MemoryManager(simManager);
		memManager.initialize(null); // non uso il tsd

		boolean res = memManager.sendCipherSymmetricKey(keyRepresentation, Utils.getSymmetricKeyMethod());
		verify(mockAppender).doAppend(argThat(new MessageMatcher(Messages.getString("MemoryManager.CANT_DECRYPT_SYMM_KEY"))));

		assertFalse(res);
	}

	@Test
	// 3.5.5
	public void shouldReadFromMemory() throws Exception {
		SimulatorManager simManager = new SimulatorManager();
		MemoryManager spyMemManager = spy(new MemoryManager(simManager));
		stub(spyMemManager.isInitialized()).toReturn(true);
		
		// init memory manager
		spyMemManager.initialize(mockTSD);
		
		// simulo l'init del protocollo (testato in altri test)
		spyMemManager.setPrivateKey(Utils.getKnownPrivateKey());
		CertificateManager certMan = new CertificateManager();
		certMan.initialize(null); // il metodo utilizzato non usa alcun TSD
		String keyRepresentation = certMan.encryptSymmetricKey(Utils.getKnownSecretKey(), Utils.getKnownPublicKey());
		spyMemManager.sendCipherSymmetricKey(keyRepresentation, Utils.getSymmetricKeyMethod());
		
		// leggo usando un hostid che ha permessi e in un'area di memoria abilitata
		String cryptedStr = spyMemManager.readMemory(0, "HOST1");
		String str = certMan.decryptData(cryptedStr, Utils.getKnownSecretKey(), Utils.getSymmetricKeyMethod());
		assertEquals("memoria 0", str);
	}
	
	
	@Test
	// 3.5.6
	public void shouldFailReadWhenUserHasNoPermissionForTSD() throws Exception {
		
		//mi aspetto una eccezione 
		expectedEx.expect(Exception.class);
	    expectedEx.expectMessage("Questo utente non ha permessi specificati per il tsd");
		
		SimulatorManager simManager = new SimulatorManager();
		MemoryManager spyMemManager = spy(new MemoryManager(simManager));
		stub(spyMemManager.isInitialized()).toReturn(true);
		
		// init memory manager
		spyMemManager.initialize(mockTSD);
		
		// simulo l'init del protocollo (testato in altri test)
		spyMemManager.setPrivateKey(Utils.getKnownPrivateKey());
		CertificateManager certMan = new CertificateManager();
		certMan.initialize(null); // il metodo utilizzato non usa alcun TSD
		String keyRepresentation = certMan.encryptSymmetricKey(Utils.getKnownSecretKey(), Utils.getKnownPublicKey());
		spyMemManager.sendCipherSymmetricKey(keyRepresentation, Utils.getSymmetricKeyMethod());
		
		// leggo usando un hostid di cui i permessi non sono settati
		spyMemManager.readMemory(0, "HOST2");
		
		// qusto codice non viene eseguito perch� viene lanciata una eccezione, come testare queste condizioni?
		//verify(mockAppender).doAppend(argThat(new MessageMatcher(Messages.getString("MemoryManager.USER_PERMISSION_NOT_SET"))));
	}
	
	
	@Test
	// 3.5.7
	public void shouldFailReadWhenUserHasNoReadPermission() throws Exception {
		
		//mi aspetto una eccezione 
		expectedEx.expect(Exception.class);
	    expectedEx.expectMessage("utente non abilitato alla lettura dell'indirizzo");
		
		SimulatorManager simManager = new SimulatorManager();
		MemoryManager spyMemManager = spy(new MemoryManager(simManager));
		stub(spyMemManager.isInitialized()).toReturn(true);
		
		// init memory manager
		spyMemManager.initialize(mockTSD);
		
		// simulo l'init del protocollo (testato in altri test)
		spyMemManager.setPrivateKey(Utils.getKnownPrivateKey());
		CertificateManager certMan = new CertificateManager();
		certMan.initialize(null); // il metodo utilizzato non usa alcun TSD
		String keyRepresentation = certMan.encryptSymmetricKey(Utils.getKnownSecretKey(), Utils.getKnownPublicKey());
		spyMemManager.sendCipherSymmetricKey(keyRepresentation, Utils.getSymmetricKeyMethod());
		
		// leggo usando un hostid di cui ci sono i permessi ma che non pu� leggere l'area indicata
		spyMemManager.readMemory(4, "HOST1");
		
		// qusto codice non viene eseguito perch� viene lanciata una eccezione, come testare queste condizioni?
		//verify(mockAppender).doAppend(argThat(new MessageMatcher(Messages.getString("MemoryManager.USER_HAS_NOT_READ_PERMISSION"))));
	}
	
	@Test
	// 3.5.8
	public void shouldFailWhenReadWrongMemoryArea() throws Exception {
		
		//mi aspetto una eccezione 
		//supponendo che i dati presenti nel tsd siano corretti un utente non potr� 
		//mai avere i permessi su un'area che non esiste, quindi mi aspetto una 
		//eccezione che dice che l'utente non ha i permessi
		expectedEx.expect(Exception.class);
		expectedEx.expectMessage("utente non abilitato alla lettura dell'indirizzo");
				
		
		SimulatorManager simManager = new SimulatorManager();
		MemoryManager spyMemManager = spy(new MemoryManager(simManager));
		stub(spyMemManager.isInitialized()).toReturn(true);
		
		// init memory manager
		spyMemManager.initialize(mockTSD);
		
		// simulo l'init del protocollo (testato in altri test)
		spyMemManager.setPrivateKey(Utils.getKnownPrivateKey());
		CertificateManager certMan = new CertificateManager();
		certMan.initialize(null); // il metodo utilizzato non usa alcun TSD
		String keyRepresentation = certMan.encryptSymmetricKey(Utils.getKnownSecretKey(), Utils.getKnownPublicKey());
		spyMemManager.sendCipherSymmetricKey(keyRepresentation, Utils.getSymmetricKeyMethod());
		
		// leggo usando un hostid di cui ci sono i permessi ma che non pu� leggere l'area indicata
		spyMemManager.readMemory(10, "HOST1");
		
		// qusto codice non viene eseguito perch� viene lanciata una eccezione, come testare queste condizioni?
		//verify(mockAppender).doAppend(argThat(new MessageMatcher(Messages.getString("MemoryManager.USER_HAS_NOT_READ_PERMISSION"))));
	}
	
	@Test
	// 3.5.9
	public void shouldFailReadOnSymmetricKeyNotSet() throws Exception {
		SimulatorManager simManager = new SimulatorManager();
		MemoryManager spyMemManager = spy(new MemoryManager(simManager));
		stub(spyMemManager.isInitialized()).toReturn(true);
		
		// init memory manager
		spyMemManager.initialize(mockTSD);
		
		
		String cryptedStr = spyMemManager.readMemory(0, "HOST1");
		assertEquals(null , cryptedStr);
	}
	
	@Test
	// 3.5.10
	public void shouldWriteFromMemory() throws Exception {
		SimulatorManager simManager = new SimulatorManager();
		MemoryManager spyMemManager = spy(new MemoryManager(simManager));
		stub(spyMemManager.isInitialized()).toReturn(true);
		
		// init memory manager
		spyMemManager.initialize(mockTSD);
		
		// simulo l'init del protocollo (testato in altri test)
		spyMemManager.setPrivateKey(Utils.getKnownPrivateKey());
		CertificateManager certMan = new CertificateManager();
		certMan.initialize(null); // il metodo utilizzato non usa alcun TSD
		String keyRepresentation = certMan.encryptSymmetricKey(Utils.getKnownSecretKey(), Utils.getKnownPublicKey());
		spyMemManager.sendCipherSymmetricKey(keyRepresentation, Utils.getSymmetricKeyMethod());
		
		// Cifro valore
		String newValueCrypted = certMan.encryptData("Nuovo valore", Utils.getKnownSecretKey(), Utils.getSymmetricKeyMethod());
		
		// Scrivo utilizzando un userID con i permessi impostati nel TSD e 
		// che effettivamente pu� scrivere nell'area di memoria
		spyMemManager.writeMemory(0, newValueCrypted, "HOST1");
		
		// verifico la scrittura
		assertEquals(mockTSDMem.getMemory().get(0), "Nuovo valore");
	}
	
	@Test
	// 3.5.11
	public void shouldFailWriteWhenUserHasNoPermissionForTSD() throws Exception {
		//mi aspetto una eccezione 
		expectedEx.expect(Exception.class);
		expectedEx.expectMessage("Questo utente non ha permessi specificati per il tsd");
				
		
		SimulatorManager simManager = new SimulatorManager();
		MemoryManager spyMemManager = spy(new MemoryManager(simManager));
		stub(spyMemManager.isInitialized()).toReturn(true);
		
		// init memory manager
		spyMemManager.initialize(mockTSD);
		
		// simulo l'init del protocollo (testato in altri test)
		spyMemManager.setPrivateKey(Utils.getKnownPrivateKey());
		CertificateManager certMan = new CertificateManager();
		certMan.initialize(null); // il metodo utilizzato non usa alcun TSD
		String keyRepresentation = certMan.encryptSymmetricKey(Utils.getKnownSecretKey(), Utils.getKnownPublicKey());
		spyMemManager.sendCipherSymmetricKey(keyRepresentation, Utils.getSymmetricKeyMethod());
		
		// Cifro valore
		String newValueCrypted = certMan.encryptData("Nuovo valore", Utils.getKnownSecretKey(), Utils.getSymmetricKeyMethod());
		
		// Scrivo utilizzando un userID senza permessi impostati nel TSD
		spyMemManager.writeMemory(0, newValueCrypted, "HOST2");
		
		// qusto codice non viene eseguito perch� viene lanciata una eccezione, come testare queste condizioni?
		// verifico che non sia stata effettuata la scrittura
		//assertFalse(mockTSDMem.getMemory().get(0).equals("Nuovo valore"));
		//verify(mockAppender).doAppend(argThat(new MessageMatcher(Messages.getString("MemoryManager.USER_PERMISSION_NOT_SET"))));
	}
	
	@Test
	// 3.5.12
	public void shouldFailWriteWhenUserHasNoWritePermission() throws Exception {
		//mi aspetto una eccezione 
		expectedEx.expect(Exception.class);
		expectedEx.expectMessage("utente non abilitato alla scrittura dell'indirizzo");
				
		
		SimulatorManager simManager = new SimulatorManager();
		MemoryManager spyMemManager = spy(new MemoryManager(simManager));
		stub(spyMemManager.isInitialized()).toReturn(true);
		
		// init memory manager
		spyMemManager.initialize(mockTSD);
		
		// simulo l'init del protocollo (testato in altri test)
		spyMemManager.setPrivateKey(Utils.getKnownPrivateKey());
		CertificateManager certMan = new CertificateManager();
		certMan.initialize(null); // il metodo utilizzato non usa alcun TSD
		String keyRepresentation = certMan.encryptSymmetricKey(Utils.getKnownSecretKey(), Utils.getKnownPublicKey());
		spyMemManager.sendCipherSymmetricKey(keyRepresentation, Utils.getSymmetricKeyMethod());
		
		// Cifro valore
		String newValueCrypted = certMan.encryptData("Nuovo valore", Utils.getKnownSecretKey(), Utils.getSymmetricKeyMethod());
		
		// Scrivo utilizzando un userID con permessi impostati nel TSD
		// ma che non pu� scrivere nell'area indicata
		spyMemManager.writeMemory(1, newValueCrypted, "HOST1");
		
		// qusto codice non viene eseguito perch� viene lanciata una eccezione, come testare queste condizioni?
		// verifico che non sia stata effettuata la scrittura
		// assertFalse(mockTSDMem.getMemory().get(0).equals("Nuovo valore"));
		// verify(mockAppender).doAppend(argThat(new MessageMatcher(Messages.getString("MemoryManager.USER_HAS_NOT_WRITE_PERMISSION"))));
	}
	
	@Test
	// 3.5.13
	public void shouldFailWhenWriteWrongMemoryArea() throws Exception {
		
		//mi aspetto una eccezione 
		//supponendo che i dati presenti nel tsd siano corretti un utente non potr� 
		//mai avere i permessi su un'area che non esiste, quindi mi aspetto una 
		//eccezione che dice che l'utente non ha i permessi
		expectedEx.expect(Exception.class);
		expectedEx.expectMessage("utente non abilitato alla scrittura dell'indirizzo");
				
		
		SimulatorManager simManager = new SimulatorManager();
		MemoryManager spyMemManager = spy(new MemoryManager(simManager));
		stub(spyMemManager.isInitialized()).toReturn(true);
		
		// init memory manager
		spyMemManager.initialize(mockTSD);
		
		// simulo l'init del protocollo (testato in altri test)
		spyMemManager.setPrivateKey(Utils.getKnownPrivateKey());
		CertificateManager certMan = new CertificateManager();
		certMan.initialize(null); // il metodo utilizzato non usa alcun TSD
		String keyRepresentation = certMan.encryptSymmetricKey(Utils.getKnownSecretKey(), Utils.getKnownPublicKey());
		spyMemManager.sendCipherSymmetricKey(keyRepresentation, Utils.getSymmetricKeyMethod());
		
		// Cifro valore
		String newValueCrypted = certMan.encryptData("Nuovo valore", Utils.getKnownSecretKey(), Utils.getSymmetricKeyMethod());
		
		// Scrivo utilizzando un userID con permessi impostati nel TSD
		// ma su un'area di memoria inesistente
		spyMemManager.writeMemory(10, newValueCrypted, "HOST1");
		
		// qusto codice non viene eseguito perch� viene lanciata una eccezione, come testare queste condizioni?
		//verify(mockAppender).doAppend(argThat(new MessageMatcher(Messages.getString("MemoryManager.USER_HAS_NOT_WRITE_PERMISSION"))));
	}
	
	@Test
	// 3.5.14
	public void shouldFailWriteMemoryWhenNewValueIsWrongEncrypted() throws Exception {
		
		expectedEx.expect(Exception.class);
		expectedEx.expectMessage("Impossibile decifrare il nuovo valore");
		
		SimulatorManager simManager = new SimulatorManager();
		MemoryManager spyMemManager = spy(new MemoryManager(simManager));
		stub(spyMemManager.isInitialized()).toReturn(true);
		
		// init memory manager
		spyMemManager.initialize(mockTSD);
		
		// simulo l'init del protocollo (testato in altri test)
		spyMemManager.setPrivateKey(Utils.getKnownPrivateKey());
		CertificateManager certMan = new CertificateManager();
		certMan.initialize(null); // il metodo utilizzato non usa alcun TSD
		String keyRepresentation = certMan.encryptSymmetricKey(Utils.getKnownSecretKey(), Utils.getKnownPublicKey());
		spyMemManager.sendCipherSymmetricKey(keyRepresentation, Utils.getSymmetricKeyMethod());
		
		// Cifro valore con una chiave diversa da quella passata al memory manager
		SecretKey key = CertificateGenerator.generateSymmetricKey(Utils.getSymmetricKeyMethod());
		String newValueCrypted = certMan.encryptData("Nuovo valore", key, Utils.getSymmetricKeyMethod());
		
		// Scrivo utilizzando un userID con i permessi impostati nel TSD e 
		// che effettivamente pu� scrivere nell'area di memoria
		spyMemManager.writeMemory(0, newValueCrypted, "HOST1");
		
		// qusto codice non viene eseguito perch� viene lanciata una eccezione, come testare queste condizioni?
		//verify(mockAppender).doAppend(argThat(new MessageMatcher(Messages.getString("MemoryManager.CANT_DECRYPT_NEW_VALUE"))));
		// verifico che non sia avvenuta alcuna scrittura
		//assertEquals(mockTSDMem.getMemory().get(0), "memoria 0");
	}
	
	@Test
	// 3.5.15
	public void shouldFailWriteOnSymmetricKeyNotSet() throws Exception {
		
		expectedEx.expect(Exception.class);
		expectedEx.expectMessage("Impossibile decifrare il nuovo valore");
		
		
		SimulatorManager simManager = new SimulatorManager();
		MemoryManager spyMemManager = spy(new MemoryManager(simManager));
		stub(spyMemManager.isInitialized()).toReturn(true);
		
		// init memory manager
		spyMemManager.initialize(mockTSD);
		spyMemManager.writeMemory(0, "value", "HOST1");
	}

}
