package ieee1667.ProbeSilo;

import java.util.*;

import ieee1667.Messages;
import ieee1667.Logger.Logger;
import ieee1667.common.model.AuthenticationMethod;
import ieee1667.common.model.TSDDescriptor;
import ieee1667.common.model.TransmissionMethod;

/**
 * Manager, si occupa della gestione del probe
 * @author mauro
 */
public class ProbeSilo {
    
    /**
     * TSD utilizzato durante la sessione corrente
     */
    private TSDDescriptor tsd;
    
    /**
     * Indica se il componente � stato inizializzato
     */
    private boolean isInitialized;
    
    /**
     * Costruttore di default
     */
    public ProbeSilo () {
        this.isInitialized = false;
    }
    
    /**
     * Inizializza il componente. Questo metodo deve essere chiamato prima di ogni altro metodo
     * @param tsd il tsd utilizzato durante la sessione corrente
     */
    public void initialize(TSDDescriptor tsd) {
        this.tsd = tsd;
        this.isInitialized = true;
    }
    
    /**
     * Resetta il componente
     */
    public void reset() {
        this.tsd = null;
        this.isInitialized = false;
    }
    
	/**
	 * Metodo utilizzato per verificare se l'authentication silo � inizializzato
	 * @return true se l'authentication silo � inizializzato, false altrimenti
	 */
	public boolean isInitialized() {
		return this.isInitialized;
	}
    
    /**
     * Effettua il discovery sul tsd corrente. Il componente deve essere inizializzato (vedi {@link #isInitialized()}
     * @return true se il discovery � andato a buon fine, false altrimenti
     */
    public boolean discovery() {
    	if(!this.isInitialized()) {
    		Logger.log(Messages.getString("ProbeSilo.NOT_INITIALIZED")); //$NON-NLS-1$
    		return false;
    	}
        
    	List<AuthenticationMethod> authMethods = this.tsd.getProbe().getSupportedAuthenticationMethods();
    	if (authMethods == null || authMethods.size() == 0) {
    		Logger.log(Messages.getString("ProbeSilo.NO_AUTH_METHODS")); //$NON-NLS-1$
    		return false;
    	}
    	
    	List<TransmissionMethod> transMethods = this.tsd.getProbe().getSupportedTransmissionMethods();
    	if (transMethods == null || transMethods.size() == 0) {
    		Logger.log(Messages.getString("ProbeSilo.NO_TRANSMISSION_METHODS")); //$NON-NLS-1$
    		return false;
    	}
    	
    	Logger.log(Messages.getString("ProbeSilo.DISCOVERY_DONE"));
    	return true; 
    }
    
    /**
     * Torna i metodi di crittografia dell'autenticazione supportati dal tsd corrente. 
     * Il componente deve essere inizializzato (vedi {@link #isInitialized()}
     * @return un arraylisti di metodi crittografici per l'autenticazione
     */
	public ArrayList<AuthenticationMethod> getAuthenticationMethods() {
		if(!this.isInitialized()) {
    		Logger.log(Messages.getString("ProbeSilo.NOT_INITIALIZED")); //$NON-NLS-1$
    		return null;
    	}
		List<AuthenticationMethod> methods = this.tsd.getProbe().getSupportedAuthenticationMethods();
		if(methods == null) return new ArrayList<AuthenticationMethod>();
		return new ArrayList<AuthenticationMethod>(methods);
	}

    /**
     * Torna i metodi di crittografia della trasmissione supportati dal tsd corrente.
     * Il componente deve essere inizializzato (vedi {@link #isInitialized()}
     * @return un arraylisti di metodi crittografici per la trasmissione
     */
	public ArrayList<TransmissionMethod> getTransmissionMethod() {
		if(!this.isInitialized()) {
    		Logger.log(Messages.getString("ProbeSilo.NOT_INITIALIZED")); //$NON-NLS-1$
    		return null;
    	}
		List<TransmissionMethod> methods = this.tsd.getProbe().getSupportedTransmissionMethods();
		if(methods == null) return new ArrayList<TransmissionMethod>();
		return new ArrayList<TransmissionMethod>(methods);
	}
    
    
}
