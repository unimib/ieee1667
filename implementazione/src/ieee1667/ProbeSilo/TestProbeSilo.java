package ieee1667.ProbeSilo;

import static org.junit.Assert.*;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.*;

import java.security.Security;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import ieee1667.Messages;
import ieee1667.Logger.Logger;
import ieee1667.common.model.AuthenticationMethod;
import ieee1667.common.model.Probe;
import ieee1667.common.model.TSDDescriptor;
import ieee1667.common.model.TransmissionMethod;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import test.util.MessageMatcher;

import ch.qos.logback.classic.spi.LoggingEvent;
import ch.qos.logback.core.Appender;

@RunWith(MockitoJUnitRunner.class)
public class TestProbeSilo {

	@Mock
	private Appender<LoggingEvent> mockAppender;

	@Mock
	private TSDDescriptor mockTSD;
	
	@Mock
	private Probe mockProbe;
	
	private List<AuthenticationMethod> authMethods;
	private List<TransmissionMethod> transMethods;
	
	@Before
	public void setUp() {
		Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
		when(mockAppender.getName()).thenReturn("MOCK");
		Logger.initializeTestMode(mockAppender);

		// creo mock tsd
		List<String> transmMethodsStr = Arrays.asList("DES", "NOEKEON", "Twofish");
		List<String> authMethodsStr = Arrays.asList("SHA1WITHRSA", "SHA512WITHRSA", "RIPEMD160WITHRSA");

		transMethods = new ArrayList<TransmissionMethod>();
		authMethods = new ArrayList<AuthenticationMethod>();
		
		for(String str : transmMethodsStr) {
			TransmissionMethod method = new TransmissionMethod();
			method.setName(str);
			transMethods.add(method);
		}
		
		for(String str : authMethodsStr) {
			AuthenticationMethod method = new AuthenticationMethod();
			method.setName(str);
			authMethods.add(method);
		}
		
		when(mockProbe.getSupportedAuthenticationMethods()).thenReturn(authMethods);
		when(mockProbe.getSupportedTransmissionMethods()).thenReturn(transMethods);
		
		when(mockTSD.getProbe()).thenReturn(mockProbe);
	}
	
	@Test
	// 3.6.3
	public void shouldCompleteDiscovery () {
		ProbeSilo spyProbeSilo = spy(new ProbeSilo());
		spyProbeSilo.initialize(mockTSD);
		stub(spyProbeSilo.isInitialized()).toReturn(true);
		
		boolean result = spyProbeSilo.discovery();
		assertTrue(result);
		verify(mockAppender).doAppend(argThat(new MessageMatcher(Messages.getString("ProbeSilo.DISCOVERY_DONE"))));
	}
	
	@Test
	// 3.6.5
	public void shouldFailDiscoveryWhenNoAuthMethod () {
		//tolgo tutti i metodi di autenticazione
		when(mockProbe.getSupportedAuthenticationMethods()).thenReturn(new ArrayList<AuthenticationMethod>());
		
		ProbeSilo spyProbeSilo = spy(new ProbeSilo());
		spyProbeSilo.initialize(mockTSD);
		stub(spyProbeSilo.isInitialized()).toReturn(true);
		
		boolean result = spyProbeSilo.discovery();
		assertFalse(result);
		verify(mockAppender).doAppend(argThat(new MessageMatcher(Messages.getString("ProbeSilo.NO_AUTH_METHODS"))));
	}	
	
	@Test
	// 3.6.4
	public void shouldFailDiscoveryWhenNoTransmissionMethod () {
		//tolgo tutti i metodi di autenticazione
		when(mockProbe.getSupportedTransmissionMethods()).thenReturn(new ArrayList<TransmissionMethod>());
		
		ProbeSilo spyProbeSilo = spy(new ProbeSilo());
		spyProbeSilo.initialize(mockTSD);
		stub(spyProbeSilo.isInitialized()).toReturn(true);
		
		boolean result = spyProbeSilo.discovery();
		assertFalse(result);
		verify(mockAppender).doAppend(argThat(new MessageMatcher(Messages.getString("ProbeSilo.NO_TRANSMISSION_METHODS"))));
	}
	
	
	@Test
	// 3.6.6
	public void shouldReturnAuthMethods () {
		ProbeSilo spyProbeSilo = spy(new ProbeSilo());
		spyProbeSilo.initialize(mockTSD);
		stub(spyProbeSilo.isInitialized()).toReturn(true);
		
		List<AuthenticationMethod> returnedMethods = spyProbeSilo.getAuthenticationMethods();
		
		assertEquals(authMethods, returnedMethods);
	}
	
	@Test
	// 3.6.7
	public void shouldReturnTransmissionMethods () {
		ProbeSilo spyProbeSilo = spy(new ProbeSilo());
		spyProbeSilo.initialize(mockTSD);
		stub(spyProbeSilo.isInitialized()).toReturn(true);
		
		List<TransmissionMethod> returnedMethods = spyProbeSilo.getTransmissionMethod();
		
		assertEquals(transMethods, returnedMethods);
	}
}
