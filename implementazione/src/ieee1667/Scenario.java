package ieee1667;

import static org.junit.Assert.*;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.*;

import java.security.Security;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import ieee1667.CertificateGenerator.CertificateGenerator;
import ieee1667.Logger.Logger;
import ieee1667.SimulatorManager.SimulatorManager;
import ieee1667.TSDManager.TSDManager;
import ieee1667.common.model.ASCh;
import ieee1667.common.model.ASCm;
import ieee1667.common.model.AuthenticationMethod;
import ieee1667.common.model.AuthenticationMethod.AuthenticationAttribute;
import ieee1667.common.model.HCh;
import ieee1667.common.model.IndexCertificate;
import ieee1667.common.model.Probe;
import ieee1667.common.model.TSDDescriptor;
import ieee1667.common.model.TSDMemory;
import ieee1667.common.model.TransmissionMethod;
import ieee1667.common.model.TransmissionMethod.TransmissionAttribute;
import ieee1667.common.model.UserIndexMemory;
import ieee1667.gui.GUI;
import ieee1667.gui.IEEE1667Scanner;


import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import ch.qos.logback.classic.spi.LoggingEvent;
import ch.qos.logback.core.Appender;

import test.util.ExactMessageMatcher;
import test.util.MessageMatcher;
import test.util.Utils;

@RunWith( MockitoJUnitRunner.class )
public class Scenario {

	@Mock
	private Appender<LoggingEvent> mockAppender;

	@Mock private IEEE1667Scanner mockScanner;
	private GUI gui;
	
	@Before
	public void setUp() {
		Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider ());
		when(mockAppender.getName()).thenReturn("MOCK");
		Logger.initializeTestMode(mockAppender);
        gui = new GUI(mockScanner);
	}
	
	
	
	@Test
	// Scenario 1 Sessione utente
	public void Scenario1() {
		// mock memoria
		TSDMemory mockTSDMemory = mock(TSDMemory.class);
		TreeMap<Integer, String> memory = new TreeMap<Integer, String>();
		memory.put(0, "");
		memory.put(1, "");
		memory.put(2, "");
		when(mockTSDMemory.getMemory()).thenReturn(memory);
		
		// mock permessi utenti
		UserIndexMemory mockIdxMem = mock(UserIndexMemory.class);
		when(mockIdxMem.getRead()).thenReturn(Arrays.asList(0,1));
		when(mockIdxMem.getWrite()).thenReturn(Arrays.asList(0));
		
		// creo HCh
		HCh userHCh = CertificateGenerator.generateHCh("host1", "ciao");
		
		// mock ASCh
		ASCh asch = mock(ASCh.class);
		when(asch.getPublicKey()).thenReturn(Utils.getKnownPublicASKP());
				
		// mock ASCm 
		ASCm ascm = mock(ASCm.class);
		when(ascm.getPublicKey()).thenReturn(Utils.getKnownPublicASKP());
		
		// mock certificate index
		IndexCertificate idxCert = mock(IndexCertificate.class);
		when(idxCert.getAsch()).thenReturn(asch);
		when(idxCert.getAscm()).thenReturn(ascm);
		when(idxCert.getHostCertificates()).thenReturn(Arrays.asList(userHCh));
		
		// mock metodi di autenticazione
		AuthenticationMethod m1 = mock(AuthenticationMethod.class);
		when(m1.getName()).thenReturn("RMD128WITHRSA");
		when(m1.getScore(AuthenticationAttribute.COMPUTATIONAL_COMPLEXITY)).thenReturn(4);
		when(m1.getScore(AuthenticationAttribute.MEMORY_COMPLEXITY)).thenReturn(0);
		when(m1.getScore(AuthenticationAttribute.PUBLIC_KEY_LENGTH)).thenReturn(4);
		when(m1.getScore(AuthenticationAttribute.STRENGTH)).thenReturn(10);

		AuthenticationMethod m2 = mock(AuthenticationMethod.class);
		when(m2.getName()).thenReturn("SHA256withRSA/PSS");
		when(m2.getScore(AuthenticationAttribute.COMPUTATIONAL_COMPLEXITY)).thenReturn(2);
		when(m2.getScore(AuthenticationAttribute.MEMORY_COMPLEXITY)).thenReturn(6);
		when(m2.getScore(AuthenticationAttribute.PUBLIC_KEY_LENGTH)).thenReturn(3);
		when(m2.getScore(AuthenticationAttribute.STRENGTH)).thenReturn(5);
		
		AuthenticationMethod m3 = mock(AuthenticationMethod.class);
		when(m3.getName()).thenReturn("SHA384withRSA/PSS");
		when(m3.getScore(AuthenticationAttribute.COMPUTATIONAL_COMPLEXITY)).thenReturn(0);
		when(m3.getScore(AuthenticationAttribute.MEMORY_COMPLEXITY)).thenReturn(7);
		when(m3.getScore(AuthenticationAttribute.PUBLIC_KEY_LENGTH)).thenReturn(6);
		when(m3.getScore(AuthenticationAttribute.STRENGTH)).thenReturn(1);
		
		AuthenticationMethod m4 = mock(AuthenticationMethod.class);
		when(m4.getName()).thenReturn("SHA384WITHRSA");
		when(m4.getScore(AuthenticationAttribute.COMPUTATIONAL_COMPLEXITY)).thenReturn(7);
		when(m4.getScore(AuthenticationAttribute.MEMORY_COMPLEXITY)).thenReturn(10);
		when(m4.getScore(AuthenticationAttribute.PUBLIC_KEY_LENGTH)).thenReturn(2);
		when(m4.getScore(AuthenticationAttribute.STRENGTH)).thenReturn(7);
		
		// mock metodi trasmissione
		TransmissionMethod tm1 = mock(TransmissionMethod.class);
		when(tm1.getName()).thenReturn("CAST5");
		when(tm1.getScore(TransmissionAttribute.COMPUTATIONAL_COMPLEXITY)).thenReturn(9);
		when(tm1.getScore(TransmissionAttribute.STRENGTH)).thenReturn(3);		
		when(tm1.getScore(TransmissionAttribute.EFFICIENCY)).thenReturn(10);	
		when(tm1.getScore(TransmissionAttribute.KEY_LENGTH)).thenReturn(7);	

		TransmissionMethod tm2 = mock(TransmissionMethod.class);
		when(tm2.getName()).thenReturn("NOEKEON");
		when(tm2.getScore(TransmissionAttribute.COMPUTATIONAL_COMPLEXITY)).thenReturn(4);
		when(tm2.getScore(TransmissionAttribute.STRENGTH)).thenReturn(0);		
		when(tm2.getScore(TransmissionAttribute.EFFICIENCY)).thenReturn(6);	
		when(tm2.getScore(TransmissionAttribute.KEY_LENGTH)).thenReturn(3);	
		
		TransmissionMethod tm3 = mock(TransmissionMethod.class);
		when(tm3.getName()).thenReturn("BLOWFISH");
		when(tm3.getScore(TransmissionAttribute.COMPUTATIONAL_COMPLEXITY)).thenReturn(1);
		when(tm3.getScore(TransmissionAttribute.STRENGTH)).thenReturn(2);		
		when(tm3.getScore(TransmissionAttribute.EFFICIENCY)).thenReturn(4);	
		when(tm3.getScore(TransmissionAttribute.KEY_LENGTH)).thenReturn(1);	
		
		TransmissionMethod tm4 = mock(TransmissionMethod.class);
		when(tm4.getName()).thenReturn("DES");
		when(tm4.getScore(TransmissionAttribute.COMPUTATIONAL_COMPLEXITY)).thenReturn(0);
		when(tm4.getScore(TransmissionAttribute.STRENGTH)).thenReturn(7);		
		when(tm4.getScore(TransmissionAttribute.EFFICIENCY)).thenReturn(5);	
		when(tm4.getScore(TransmissionAttribute.KEY_LENGTH)).thenReturn(6);	
		
		// mockProbe
		Probe mockProbe = mock(Probe.class);
		when(mockProbe.getSupportedAuthenticationMethods()).thenReturn(Arrays.asList(m1,m2,m3,m4));
		when(mockProbe.getSupportedTransmissionMethods()).thenReturn(Arrays.asList(tm1,tm2,tm3,tm4));
		
		// mock TSD
		TSDDescriptor mockTSD = mock(TSDDescriptor.class);
		
		when(mockTSD.getName()).thenReturn("TSD1");
		when(mockTSD.getAskpKeyPair()).thenReturn(Utils.getKnownASKPKeyPair());
		when(mockTSD.getMemory()).thenReturn(mockTSDMemory);
		
		Map<String, UserIndexMemory> map = new HashMap<String, UserIndexMemory>();
		map.put("host1", mockIdxMem);
		when(mockTSD.getUserIndexMemory()).thenReturn(map);
		
		when(mockTSD.getIndexCertificate()).thenReturn(idxCert);
		when(mockTSD.getProbe()).thenReturn(mockProbe);
		
		// mock tsdmanager
		TSDManager tsdManager = mock(TSDManager.class);
		when(tsdManager.getTSDList()).thenReturn(Arrays.asList(mockTSD));
		
		// mock input utente
		when(mockScanner.nextLine()).thenReturn(
			"1", // entra nella sessione utente
			"0", // seleziono tsd 0
			"host1", // userID
			"ciao", // password
			// policy auth
			"1",
			"2",
			"-1",
			// policy transm
			"1",
			"-1",
			// operazioni utente
			"1", // leggo
			"1", // area di  memoria 1
			"1", // leggo
			"4", // area di  memoria 4 (non esiste)
			"0", // legge area di memora 0
			"2", // operazione di scrittura
			"1", // vuole scrivere valore nell'area 1 (non ha i permessi)
			"nuovo valore",
			"2", // operazione di scrittura
			"0", // vuole scrivere valore nell'area 1 (non ha i permessi)
			"nuovo valore",
			// chiudo sessione utente
			"3",
			// termino simulazione
			"4"
		);
		
		
		
		SimulatorManager simManager = new SimulatorManager(gui, tsdManager);
		simManager.run(); 
		
		// verifica
		
		// verifica auth tsd
		verify(mockAppender).doAppend(argThat(new MessageMatcher(Messages.getString("AuthenticationSilo.AUTH_TSD_DONE"))));
		// verifica auth auth silo
		verify(mockAppender).doAppend(argThat(new MessageMatcher(Messages.getString("AuthenticationSilo.AUTH_AUTH_SILO_DONE"))));
		
		// verifico la policy di autenticazione inserita
		verify(mockAppender).doAppend(argThat(new ExactMessageMatcher("Policy di autenticazione inserita [authenticationPreferences=[STRENGTH, MEMORY_COMPLEXITY]]")));
		// verifico gli score autenticazione calcolati
		verify(mockAppender).doAppend(argThat(new ExactMessageMatcher("Score calcolati {SHA256withRSA/PSS=17, RMD128WITHRSA=10, SHA384WITHRSA=27, SHA384withRSA/PSS=15}")));
		// verifico il metodo scelto e lo score del metodo di autenticazione
		verify(mockAppender).doAppend(argThat(new ExactMessageMatcher("Metodo di autenticazione scelto: SHA384WITHRSA con un punteggio di 27")));
		// verifica auth utente
		verify(mockAppender).doAppend(argThat(new MessageMatcher(Messages.getString("AuthenticationSilo.AUTH_USER_DONE"))));
		
		
		// verifico la policy di trasmissione inserita
		verify(mockAppender).doAppend(argThat(new ExactMessageMatcher("Policy di trasmissione inserita [transmissionPreferences=[EFFICIENCY]]")));
		// verifico gli score di trasmissione calcolati
		verify(mockAppender).doAppend(argThat(new ExactMessageMatcher("Score calcolati {BLOWFISH=4, DES=5, CAST5=10, NOEKEON=6}")));
		// verifico il metodo scelto e lo score del metodo di trasmissione
		verify(mockAppender).doAppend(argThat(new ExactMessageMatcher("Metodo di trasmissione scelto: CAST5 con un punteggio di 10")));
				
		// verico le due operazioni di lettura
		verify(mockAppender, times(2)).doAppend(argThat(new ExactMessageMatcher("Operazione utente scelta: READ")));
		
		// verifico le due operazioni di scrittura
		verify(mockAppender, times(2)).doAppend(argThat(new ExactMessageMatcher("Operazione utente scelta: WRITE")));
		
		// verifico che sia stata impedita una scrittura
		verify(mockAppender).doAppend(argThat(new ExactMessageMatcher("Questo utente non ha permessi di scrittura per l'indirizzo 1, permessi specificati: [0]")));
		
		// verifico avvenuta scrittura in memoria
		assertEquals("nuovo valore", mockTSDMemory.getMemory().get(0));
	}
	
	
	
	
	@Test
	// Scenario 2  Sessione utente
	public void Scenario2() {
		// mock memoria
		TSDMemory mockTSDMemory = mock(TSDMemory.class);
		TreeMap<Integer, String> memory = new TreeMap<Integer, String>();
		memory.put(0, "");
		memory.put(1, "");
		memory.put(2, "");
		when(mockTSDMemory.getMemory()).thenReturn(memory);
		
		// mock permessi utenti
		UserIndexMemory mockIdxMem = mock(UserIndexMemory.class);
		when(mockIdxMem.getRead()).thenReturn(Arrays.asList(0,1));
		when(mockIdxMem.getWrite()).thenReturn(Arrays.asList(0));
		
		// creo HCh
		HCh userHCh = CertificateGenerator.generateHCh("host1", "ciao");
		
		// mock ASCh
		ASCh asch = mock(ASCh.class);
		when(asch.getPublicKey()).thenReturn(Utils.getKnownPublicASKP());
				
		// mock ASCm 
		ASCm ascm = mock(ASCm.class);
		when(ascm.getPublicKey()).thenReturn(Utils.getKnownPublicASKP());
		
		// mock certificate index
		IndexCertificate idxCert = mock(IndexCertificate.class);
		when(idxCert.getAsch()).thenReturn(asch);
		when(idxCert.getAscm()).thenReturn(ascm);
		when(idxCert.getHostCertificates()).thenReturn(Arrays.asList(userHCh));
		
		// mock metodi di autenticazione
		AuthenticationMethod m1 = mock(AuthenticationMethod.class);
		when(m1.getName()).thenReturn("RMD128WITHRSA");
		when(m1.getScore(AuthenticationAttribute.COMPUTATIONAL_COMPLEXITY)).thenReturn(4);
		when(m1.getScore(AuthenticationAttribute.MEMORY_COMPLEXITY)).thenReturn(0);
		when(m1.getScore(AuthenticationAttribute.PUBLIC_KEY_LENGTH)).thenReturn(4);
		when(m1.getScore(AuthenticationAttribute.STRENGTH)).thenReturn(10);

		AuthenticationMethod m2 = mock(AuthenticationMethod.class);
		when(m2.getName()).thenReturn("SHA256withRSA/PSS");
		when(m2.getScore(AuthenticationAttribute.COMPUTATIONAL_COMPLEXITY)).thenReturn(2);
		when(m2.getScore(AuthenticationAttribute.MEMORY_COMPLEXITY)).thenReturn(6);
		when(m2.getScore(AuthenticationAttribute.PUBLIC_KEY_LENGTH)).thenReturn(3);
		when(m2.getScore(AuthenticationAttribute.STRENGTH)).thenReturn(5);
		
		AuthenticationMethod m3 = mock(AuthenticationMethod.class);
		when(m3.getName()).thenReturn("SHA384withRSA/PSS");
		when(m3.getScore(AuthenticationAttribute.COMPUTATIONAL_COMPLEXITY)).thenReturn(0);
		when(m3.getScore(AuthenticationAttribute.MEMORY_COMPLEXITY)).thenReturn(7);
		when(m3.getScore(AuthenticationAttribute.PUBLIC_KEY_LENGTH)).thenReturn(6);
		when(m3.getScore(AuthenticationAttribute.STRENGTH)).thenReturn(1);
		
		// mock metodi trasmissione 
		//(cambio mock da auth method perch� cambia tipo di calcolo per policy vuote)
		TransmissionMethod tm2 = mock(TransmissionMethod.class);
		when(tm2.getName()).thenReturn("NOEKEON");
		when(tm2.getComputationalComplexity()).thenReturn(4);
		when(tm2.getStrength()).thenReturn(0);		
		when(tm2.getEfficiency()).thenReturn(6);	
		when(tm2.getKeyLenght()).thenReturn(3);	
		
		TransmissionMethod tm3 = mock(TransmissionMethod.class);
		when(tm3.getName()).thenReturn("BLOWFISH");
		when(tm3.getComputationalComplexity()).thenReturn(1);
		when(tm3.getStrength()).thenReturn(2);		
		when(tm3.getEfficiency()).thenReturn(4);	
		when(tm3.getKeyLenght()).thenReturn(1);	
		
		TransmissionMethod tm4 = mock(TransmissionMethod.class);
		when(tm4.getName()).thenReturn("DES");
		when(tm4.getComputationalComplexity()).thenReturn(0);
		when(tm4.getStrength()).thenReturn(7);		
		when(tm4.getEfficiency()).thenReturn(5);	
		when(tm4.getKeyLenght()).thenReturn(6);	
		
		// mockProbe
		Probe mockProbe = mock(Probe.class);
		when(mockProbe.getSupportedAuthenticationMethods()).thenReturn(Arrays.asList(m1,m2,m3));
		when(mockProbe.getSupportedTransmissionMethods()).thenReturn(Arrays.asList(tm2,tm3,tm4));
		
		// mock TSD
		TSDDescriptor mockTSD = mock(TSDDescriptor.class);
		
		when(mockTSD.getName()).thenReturn("TSD1");
		when(mockTSD.getAskpKeyPair()).thenReturn(Utils.getKnownASKPKeyPair());
		when(mockTSD.getMemory()).thenReturn(mockTSDMemory);
		
		Map<String, UserIndexMemory> map = new HashMap<String, UserIndexMemory>();
		map.put("host1", mockIdxMem);
		when(mockTSD.getUserIndexMemory()).thenReturn(map);
		
		when(mockTSD.getIndexCertificate()).thenReturn(idxCert);
		when(mockTSD.getProbe()).thenReturn(mockProbe);
		
		// mock tsdmanager
		TSDManager tsdManager = mock(TSDManager.class);
		when(tsdManager.getTSDList()).thenReturn(Arrays.asList(mockTSD));
		
		// mock input utente
		when(mockScanner.nextLine()).thenReturn(
			"1", // entra nella sessione utente
			"0", // seleziono tsd 0
			"host1", // userID
			"ciao", // password
			// policy auth
			"1",
			"2",
			"0",
			"0",
			"-1",
			// policy transm
			"-1",
			// chiudo sessione utente
			"3",
			// termino simulazione
			"4"
		);
		
		
		
		SimulatorManager simManager = new SimulatorManager(gui, tsdManager);
		simManager.run(); 
		
		// verifica
		
		// verifica auth tsd
		verify(mockAppender).doAppend(argThat(new MessageMatcher(Messages.getString("AuthenticationSilo.AUTH_TSD_DONE"))));
		// verifica auth auth silo
		verify(mockAppender).doAppend(argThat(new MessageMatcher(Messages.getString("AuthenticationSilo.AUTH_AUTH_SILO_DONE"))));
		
		// verifico la policy di autenticazione inserita
		verify(mockAppender).doAppend(argThat(new ExactMessageMatcher("Policy di autenticazione inserita [authenticationPreferences=[PUBLIC_KEY_LENGTH, COMPUTATIONAL_COMPLEXITY, STRENGTH, MEMORY_COMPLEXITY]]")));
		// verifico gli score autenticazione calcolati
		verify(mockAppender).doAppend(argThat(new ExactMessageMatcher("Score calcolati {SHA256withRSA/PSS=46, RMD128WITHRSA=42, SHA384withRSA/PSS=37}")));
		// verifico il metodo scelto e lo score del metodo di autenticazione
		verify(mockAppender).doAppend(argThat(new ExactMessageMatcher("Metodo di autenticazione scelto: SHA256withRSA/PSS con un punteggio di 46")));
		// verifica auth utente
		verify(mockAppender).doAppend(argThat(new MessageMatcher(Messages.getString("AuthenticationSilo.AUTH_USER_DONE"))));
		
		
		// verifico la policy di trasmissione inserita
		verify(mockAppender).doAppend(argThat(new ExactMessageMatcher("Policy di trasmissione inserita [transmissionPreferences=[]]")));
		// verifico gli score di trasmissione calcolati
		verify(mockAppender).doAppend(argThat(new ExactMessageMatcher("Score calcolati {BLOWFISH=8, DES=18, NOEKEON=13}")));
		// verifico il metodo scelto e lo score del metodo di trasmissione
		verify(mockAppender).doAppend(argThat(new ExactMessageMatcher("Metodo di trasmissione scelto: DES con un punteggio di 18")));
	}
	
	@Test
	// Scenario 3 Sessione utente
	public void Scenario3() {
		// mock memoria
		TSDMemory mockTSDMemory = mock(TSDMemory.class);
		TreeMap<Integer, String> memory = new TreeMap<Integer, String>();
		memory.put(0, "");
		memory.put(1, "");
		memory.put(2, "");
		when(mockTSDMemory.getMemory()).thenReturn(memory);
		
		// mock permessi utenti
		UserIndexMemory mockIdxMem = mock(UserIndexMemory.class);
		when(mockIdxMem.getRead()).thenReturn(Arrays.asList(0,1));
		when(mockIdxMem.getWrite()).thenReturn(Arrays.asList(0));
		
		// creo HCh
		HCh userHCh = CertificateGenerator.generateHCh("host1", "ciao");
		
		// mock ASCh
		ASCh asch = mock(ASCh.class);
		when(asch.getPublicKey()).thenReturn(Utils.getKnownPublicASKP());
				
		// mock ASCm 
		ASCm ascm = mock(ASCm.class);
		when(ascm.getPublicKey()).thenReturn(Utils.getKnownPublicASKP());
		
		// mock certificate index
		IndexCertificate idxCert = mock(IndexCertificate.class);
		when(idxCert.getAsch()).thenReturn(asch);
		when(idxCert.getAscm()).thenReturn(ascm);
		when(idxCert.getHostCertificates()).thenReturn(Arrays.asList(userHCh));
		
		// mock metodi di autenticazione
		AuthenticationMethod m1 = mock(AuthenticationMethod.class);
		when(m1.getName()).thenReturn("RMD128WITHRSA");
		when(m1.getScore(AuthenticationAttribute.COMPUTATIONAL_COMPLEXITY)).thenReturn(4);
		when(m1.getScore(AuthenticationAttribute.MEMORY_COMPLEXITY)).thenReturn(0);
		when(m1.getScore(AuthenticationAttribute.PUBLIC_KEY_LENGTH)).thenReturn(4);
		when(m1.getScore(AuthenticationAttribute.STRENGTH)).thenReturn(10);

		AuthenticationMethod m2 = mock(AuthenticationMethod.class);
		when(m2.getName()).thenReturn("SHA256withRSA/PSS");
		when(m2.getScore(AuthenticationAttribute.COMPUTATIONAL_COMPLEXITY)).thenReturn(2);
		when(m2.getScore(AuthenticationAttribute.MEMORY_COMPLEXITY)).thenReturn(6);
		when(m2.getScore(AuthenticationAttribute.PUBLIC_KEY_LENGTH)).thenReturn(3);
		when(m2.getScore(AuthenticationAttribute.STRENGTH)).thenReturn(5);
		
		AuthenticationMethod m3 = mock(AuthenticationMethod.class);
		when(m3.getName()).thenReturn("SHA384withRSA/PSS");
		when(m3.getScore(AuthenticationAttribute.COMPUTATIONAL_COMPLEXITY)).thenReturn(0);
		when(m3.getScore(AuthenticationAttribute.MEMORY_COMPLEXITY)).thenReturn(7);
		when(m3.getScore(AuthenticationAttribute.PUBLIC_KEY_LENGTH)).thenReturn(6);
		when(m3.getScore(AuthenticationAttribute.STRENGTH)).thenReturn(1);
		
		
		// mockProbe
		Probe mockProbe = mock(Probe.class);
		when(mockProbe.getSupportedAuthenticationMethods()).thenReturn(Arrays.asList(m1,m2,m3));
		when(mockProbe.getSupportedTransmissionMethods()).thenReturn(new ArrayList<TransmissionMethod>());
		
		// mock TSD
		TSDDescriptor mockTSD = mock(TSDDescriptor.class);
		
		when(mockTSD.getName()).thenReturn("TSD1");
		when(mockTSD.getAskpKeyPair()).thenReturn(Utils.getKnownASKPKeyPair());
		when(mockTSD.getMemory()).thenReturn(mockTSDMemory);
		
		Map<String, UserIndexMemory> map = new HashMap<String, UserIndexMemory>();
		map.put("host1", mockIdxMem);
		when(mockTSD.getUserIndexMemory()).thenReturn(map);
		
		when(mockTSD.getIndexCertificate()).thenReturn(idxCert);
		when(mockTSD.getProbe()).thenReturn(mockProbe);
		
		// mock tsdmanager
		TSDManager tsdManager = mock(TSDManager.class);
		when(tsdManager.getTSDList()).thenReturn(Arrays.asList(mockTSD));
		
		// mock input utente
		when(mockScanner.nextLine()).thenReturn(
			"1", // entra nella sessione utente
			"0", // seleziono tsd 0
			"4" // termino simulazione
		);
		
		
		
		SimulatorManager simManager = new SimulatorManager(gui, tsdManager);
		simManager.run(); 
		
		// verifica
		
		// verifica auth tsd
		verify(mockAppender).doAppend(argThat(new MessageMatcher(Messages.getString("ProbeSilo.NO_TRANSMISSION_METHODS"))));
		// verifica auth auth silo
		verify(mockAppender).doAppend(argThat(new MessageMatcher(Messages.getString("SimulatorManager.DISCOVERY_ERROR"))));
	}
}
