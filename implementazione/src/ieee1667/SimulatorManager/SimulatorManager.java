package ieee1667.SimulatorManager;

import ieee1667.Messages;
import ieee1667.Constant.*;
import ieee1667.AuthenticationSilo.AuthenticationSilo;
import ieee1667.CertificateGenerator.CertificateGenerator;
import ieee1667.CertificateManager.CertificateManager;
import ieee1667.Logger.Logger;
import ieee1667.MemoryManager.MemoryManager;
import ieee1667.ProbeSilo.ProbeSilo;
import ieee1667.TSDManager.TSDManager;
import ieee1667.common.model.ASCh;
import ieee1667.common.model.ASCm;
import ieee1667.common.model.AuthenticationMethod;
import ieee1667.common.model.AuthenticationPolicy;
import ieee1667.common.model.HCh;
import ieee1667.common.model.KeyPair;
import ieee1667.common.model.PrivateKey;
import ieee1667.common.model.PublicKey;
import ieee1667.common.model.TSDDescriptor;
import ieee1667.common.model.TransmissionMethod;
import ieee1667.common.model.TransmissionPolicy;
import ieee1667.gui.GUI;

import java.security.Key;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.crypto.SecretKey;

/**
 * Manager, si occupa della gestione e del coordinamento di tutta la simulazione
 * 
 * @author mauro
 */
public class SimulatorManager implements Runnable {

	private TSDManager tsdManager;
	private AuthenticationSilo authSilo;
	private MemoryManager memoryManager;
	private ProbeSilo probeSilo;
	private CertificateManager certificateManager;
	private GUI gui;

	/**
	 * Chiave simmetrica da utilizzare per per cifrare i dati da inviare al
	 * memory manager
	 */
	private Key memoryTransferSymmetricKey;

	/**
	 * Metodo crittografico da usare per criptare la trasmissione simmetrica
	 */
	private String transmissionMethod;

	/**
	 * Costruttore di default
	 */
	public SimulatorManager() {
		this(new GUI());
	}

	/**
	 * Costruttore utilizzato a scopo di test, viene passago un componente gui
	 * da utilizzare
	 * 
	 * @param gui
	 *            il componente gui da utilizzare, solitamente contiene degli
	 *            elementi creati come mock
	 */
	public SimulatorManager(GUI gui) {
		this.tsdManager = new TSDManager();
		this.authSilo = new AuthenticationSilo();
		this.memoryManager = new MemoryManager(this);
		this.probeSilo = new ProbeSilo();
		this.certificateManager = new CertificateManager();
		this.gui = gui;
	}
	
	/**
	 * Costruttore utilizzato a scopo di test, viene passago un componente gui
	 * da utilizzare
	 * 
	 * @param gui
	 *            il componente gui da utilizzare, solitamente contiene degli
	 *            elementi creati come mock
	 * @param tsdManager
	 * 			  il componente tsd manager da utilizzare, solitamente contiene elementi creati come mock
	 */
	public SimulatorManager(GUI gui, TSDManager tsdManager) {
		this.tsdManager = tsdManager;
		this.authSilo = new AuthenticationSilo();
		this.memoryManager = new MemoryManager(this);
		this.probeSilo = new ProbeSilo();
		this.certificateManager = new CertificateManager();
		this.gui = gui;
	}
	

	@Override
	/**
	 * Avvia il loop di richiesta operazioni
	 */
	public void run() {

		OPERATION_CHOICE choice;

		do {
			choice = this.gui.askOperation();

			switch (choice) {
				case USER_SESSION:
					this.newUserSession();
					break;
				
				case ADMIN_SESSION:
					this.gui.showMessage("Non ancora implementato"); //$NON-NLS-1$
					break;
				
				case MANAGE_TSD:
					this.gui.showMessage("Non ancora implementato"); //$NON-NLS-1$
					break;
				
				default:
					break;					

			}

		} while (choice != OPERATION_CHOICE.NONE);
	}

	/**
	 * Sequenza di operazioni per la sessione utente
	 */
	private void newUserSession() {

		Logger.log(Messages.getString("SimulatorManager.BEGIN_USER_SESSION")); //$NON-NLS-1$

		// 1. otteniamo la lista dei TSD presenti
		List<TSDDescriptor> tsdList = tsdManager.getTSDList();

		// 2. scelta TSD
		TSDDescriptor tsd = gui.askTSD(tsdList);
		Logger.log(Messages.getString("SimulatorManager.CHOOSE_TSD"), tsd); //$NON-NLS-1$

		// 3. inizializzo componenti
		this.probeSilo.initialize(tsd);
		this.authSilo.initialize(tsd);
		this.memoryManager.initialize(tsd);
		this.certificateManager.initialize(tsd);

		// 4. discovery
		Logger.log(Messages.getString("SimulatorManager.START_DISCOVERY")); //$NON-NLS-1$
		boolean discoveryResult = this.probeSilo.discovery();

		if (discoveryResult) {
			Logger.log(Messages.getString("SimulatorManager.DISCOVERY_DONE")); //$NON-NLS-1$
			gui.showMessage("Discovery completato con successo"); //$NON-NLS-1$
		} else {
			Logger.log(Messages.getString("SimulatorManager.DISCOVERY_ERROR")); //$NON-NLS-1$
			gui.showMessage("errore durante il discovery, chiudo sessione utente"); //$NON-NLS-1$
			return;
		}

		// 5. Autenticazione TSD
		boolean authTSDResult = this.authenticateTSD(tsd);
		if (authTSDResult) {
			Logger.log(Messages.getString("SimulatorManager.TSD_AUTH_DONE")); //$NON-NLS-1$
			gui.showMessage("Autenticazione TSD completata con successo"); //$NON-NLS-1$
		} else {
			Logger.log(Messages.getString("SimulatorManager.TSD_AUTH_ERROR")); //$NON-NLS-1$
			gui.showMessage("errore durante l'autenticazione del TSD, chiudo sessione utente"); //$NON-NLS-1$
			return;
		}

		// 6. Autenticazione Utente
		// interfaccia -> richiedere HostID e chiave segreta
		String userID = gui.askUserID();
		String secretKey = gui.askPassword();
		boolean authUserResult = this.authenticateUser(tsd, userID, secretKey);
		if (authUserResult) {
			Logger.log(Messages.getString("SimulatorManager.USER_AUTH_DONE")); //$NON-NLS-1$
			gui.showMessage("Autenticazione utente completata con successo"); //$NON-NLS-1$
		} else {
			Logger.log(Messages.getString("SimulatorManager.USER_AUTH_ERROR")); //$NON-NLS-1$
			gui.showMessage("errore durante l'autenticazione utente, chiudo sessione utente"); //$NON-NLS-1$
			return;
		}

		// 7. Inizializzazione protocollo di trasferimento dati
		this.initializeProtocol(tsd);

		// 8. Trasferimento dati
		USER_OPERATION userOperation = gui.askUserOperation();

		while (userOperation != USER_OPERATION.NONE) {

			Logger.log(
					Messages.getString("SimulatorManager.CHOOSED_USER_SESSION_OPERATION"), userOperation); //$NON-NLS-1$

			if (userOperation == USER_OPERATION.READ) {
				int idx = gui.askMemoryIndex(tsd.getMemory());
				try {

					String cryptedResult = this.memoryManager.readMemory(idx,
							userID);
					String result = this.requestSymmetricDecrypt(cryptedResult,
							this.memoryTransferSymmetricKey,
							this.transmissionMethod);

					if (result != null) {
						Logger.log(
								Messages.getString("SimulatorManager.READ_OPERATION_DONE"), idx, result); //$NON-NLS-1$
						this.gui.showMessage("Valore : " + result); //$NON-NLS-1$
					} else {
						this.gui.showMessage("Impossibile leggere il valore, errore nella cifratura della trasmissione"); //$NON-NLS-1$
						Logger.log("SimulatorManager.READ_CRYPT_ERROR");
					}

				} catch (Exception e) {
					Logger.log(
							Messages.getString("SimulatorManager.READ_ERROR"), e.getMessage()); //$NON-NLS-1$
					this.gui.showMessage(e.getMessage());
				}
			} else {

				int idx = gui.askMemoryIndex(tsd.getMemory());
				String newValue = gui.askNewValueToWrite();
				String cryptedValue = this.requestSymmetricEncryption(newValue,
						this.memoryTransferSymmetricKey,
						this.transmissionMethod);

				if (cryptedValue == null) {
					Logger.log(Messages
							.getString("SimulatorManager.CANT_CRYPT_VALUE_TO_WRITE")); //$NON-NLS-1$
					this.gui.showMessage("Impossibile terminare la scrittura, impossibile cifrare la trasmissione"); //$NON-NLS-1$

				} else {
					try {
						// scrivo
						this.memoryManager.writeMemory(idx, cryptedValue,
								userID);
						// persisto modifiche
						this.tsdManager.updateTSD(tsd);
						this.gui.showMessage("scrittura effetuata con successo"); //$NON-NLS-1$
						Logger.log(Messages.getString("SimulatorManager.WRITE_OPERATION_DONE"));
					} catch (Exception e) {
						Logger.log(
								Messages.getString("SimulatorManager.WRITE_ERROR"), e.getMessage()); //$NON-NLS-1$
						this.gui.showMessage(e.getMessage());
					}
				}
			}

			userOperation = gui.askUserOperation();

		}

		// reset
		this.transmissionMethod = ""; //$NON-NLS-1$
		this.memoryTransferSymmetricKey = null;
		this.probeSilo.reset();
		this.authSilo.reset();
		this.memoryManager.reset();
		this.certificateManager.reset();

		Logger.log(Messages.getString("SimulatorManager.USER_OPERATIONS_DONE")); //$NON-NLS-1$
	}

	/**
	 * Richiede di criptare un dato con algoritmo simmetrico
	 * 
	 * @param data
	 *            il dato da criptare
	 * @param key
	 *            la chiave simmetica
	 * @param method
	 *            il metodo da utilizzare
	 * @return il dato criptato
	 */
	public String requestSymmetricEncryption(String data, Key key, String method) {
		return this.certificateManager.encryptData(data, key, method);
	}

	/**
	 * Richiede di decriptare un dato con algoritmo simmetrico
	 * 
	 * @param data
	 *            il dato da decriptare
	 * @param key
	 *            la chiave simmetica
	 * @param method
	 *            il metodo da utilizzare
	 * @return il dato decriptato
	 */
	public String requestSymmetricDecrypt(String data, Key key, String method) {
		return this.certificateManager.decryptData(data, key, method);
	}

	/**
	 * Cifra una chiave simmetrica con una chiave pubblica utilizzando un
	 * algoritmo asimmetrico
	 * 
	 * @param symmetricKey
	 *            la chiave simmetrica
	 * @param publicKey
	 *            la chiave pubblica
	 * @return una stringa che rappresenta la chiave cifrata
	 */
	public String chiper(SecretKey symmetricKey, PublicKey publicKey) {
		return this.certificateManager.encryptSymmetricKey(symmetricKey,
				publicKey);
	}

	/**
	 * Decifra una chiave simmetrica con una chiave privata utilizzando un
	 * algoritmo asimmetrico
	 * 
	 * @param keyRepresentation
	 *            la rappresentazione cifrata della chiave simmetrica
	 * @param privateKey
	 *            la chiave privata da utilizzare per decifrare la chiave
	 * @return una chiave simmetrica
	 */
	public SecretKey DeChiper(String keyRepresentation, PrivateKey privateKey) {
		return this.certificateManager.decryptSymmetricKey(keyRepresentation,
				privateKey);
	}

	/**
	 * Autentica un tsd
	 * 
	 * @param tsd
	 *            il tsd da autenticare
	 * @return true se l'autenticazione � andata a buon fine, false altrimenti
	 */
	private boolean authenticateTSD(TSDDescriptor tsd) {
		Logger.log(Messages.getString("SimulatorManager.TSD_AUTH_START")); //$NON-NLS-1$
		ASCm ascm = this.certificateManager.getASCm();
		Logger.log(Messages.getString("SimulatorManager.GET_ASCM_DONE"), ascm); //$NON-NLS-1$
		boolean authTSDResult = this.authSilo.requestTSDAuthentication(ascm);

		// se l'autenticazione TSD � fallita non continuo
		if (authTSDResult == false) {
			Logger.log(Messages.getString("SimulatorManager.CANT_AUTH_TSD")); //$NON-NLS-1$
			return false;
		}

		Logger.log(Messages.getString("SimulatorManager.AUTH_SILO_AUTH_START")); //$NON-NLS-1$
		ASCh asch = this.certificateManager.getASCh();
		Logger.log(Messages.getString("SimulatorManager.GET_ASCH_DONE"), asch); //$NON-NLS-1$
		boolean authAuthSiloResult = this.authSilo
				.requestAuthSiloAuthentication(asch);

		if (authAuthSiloResult == false) {
			Logger.log(Messages
					.getString("SimulatorManager.CANT_AUTH_AUTH_SILO")); //$NON-NLS-1$
			return false;
		}

		return true;
	}

	/**
	 * Autentica un utente
	 * 
	 * @param tsd
	 *            il tsd su cui effettuare l'autenticazione
	 * @param userID
	 *            la userID dell'utente da autenticare
	 * @param secretKey
	 *            la password dell'utente
	 * @return true se l'autenticazione � andata a buon fine, false altrimenti
	 */
	private boolean authenticateUser(TSDDescriptor tsd, String userID,
			String secretKey) {
		// ottengo lista metodi supportati dal tsd
		ArrayList<AuthenticationMethod> supportedMethods = this.probeSilo
				.getAuthenticationMethods();

		// chiedo politica
		AuthenticationPolicy policy = gui.askAuthenticationPolicy();
		Logger.log(
				Messages.getString("SimulatorManager.AUTH_POLICY_INSERTED"), policy); //$NON-NLS-1$

		// calcolo scores
		HashMap<String, Integer> scores = this
				.calculateAuthenticationMethodsScore(policy, supportedMethods);
		Logger.log(
				Messages.getString("SimulatorManager.SCORE_CALCULATED"), scores); //$NON-NLS-1$

		// sceglo il migliore
		String methodName = this.chooseAuthenticationMethod(scores);
		Logger.log(
				Messages.getString("SimulatorManager.AUTH_METHOD_CHOOSED"), methodName, scores.get(methodName)); //$NON-NLS-1$

		List<HCh> hchList = this.certificateManager.getHChList();
		Logger.log(Messages.getString("SimulatorManager.GET_HCH_LIST"), hchList); //$NON-NLS-1$

		// trovo l'HCh dell'utente
		HCh userHCh = null;
		int hchIndex = -1;
		// ricerca HCh per host inserito
		for (int i = 0; (i < hchList.size() && userHCh == null); i++) {
			if (hchList.get(i).getUserID().equals(userID)) {
				userHCh = hchList.get(i);
				hchIndex = i;
			}
		}

		if (userHCh != null) {
			Logger.log(
					Messages.getString("SimulatorManager.HCH_FOUND"), userHCh); //$NON-NLS-1$
		} else {
			Logger.log(
					Messages.getString("SimulatorManager.HCH_NOT_FOUND"), userID); //$NON-NLS-1$
			return false;
		}

		// ottengo chiave privata
		PrivateKey pk = this.certificateManager.getPrivateKeyForHCh(userHCh,
				secretKey);
		if (pk == null) {
			Logger.log(Messages.getString("SimulatorManager.CANT_AUTH_USER")); //$NON-NLS-1$
			return false;
		}

		boolean authUserResult = this.authSilo.requestUserAuthentication(
				userID, pk, userHCh, hchIndex, methodName);
		return authUserResult;
	}

	/**
	 * A partire dagli score calcolati si ottiene il metodo crittografico di
	 * autenticazione migliore
	 * 
	 * @param scores
	 *            gli score calcolati
	 * @return il nome dell'algoritmo
	 */
	private String chooseAuthenticationMethod(HashMap<String, Integer> scores) {
		String bestMethodName = ""; //$NON-NLS-1$
		int maxScore = -1;
		for (String authMethod : scores.keySet()) {
			int score = scores.get(authMethod);
			if (score > maxScore) {
				maxScore = score;
				bestMethodName = authMethod;
			}
		}

		return bestMethodName;
	}

	/**
	 * Calcola gli score per ognuno dei metodi crittografici di autenticazione
	 * basandosi sulla policy
	 * 
	 * @param policy
	 *            la policy
	 * @param supportedMethods
	 *            i metodi di autenticazione
	 * @return una mappa in cui la chiave � il nome (univoco) dell'algoritmo e
	 *         il valore � lo score calcolato
	 */
	private HashMap<String, Integer> calculateAuthenticationMethodsScore(
			AuthenticationPolicy policy,
			ArrayList<AuthenticationMethod> supportedMethods) {

		HashMap<String, Integer> scores = new HashMap<String, Integer>();

		for (AuthenticationMethod method : supportedMethods) {

			int score = 0;
			if (policy.getAuthenticationPreferences().size() == 0) {
				score = method.getComputationalComplexity() +
						method.getMemoryComplexity() +
						method.getPublicKeyLenght() +
						method.getStrength();
			} else {
				for (int i = 0; i < policy.getAuthenticationPreferences().size(); i++) {
					int methodAttrScore = method.getScore(policy
							.getAuthenticationPreferences().get(i));
					score += methodAttrScore * (i + 1);
				}				
			}
			scores.put(method.getName(), score);
		}

		return scores;
	}

	/**
	 * Calcola gli score dei metodi crittografici di trasmissione basandosi
	 * sulla policy
	 * 
	 * @param policy
	 *            la policy
	 * @param supportedTransmissionMethod
	 *            i metodi di trasmissione
	 * @return una mappa in cui la chiave � il nome (univoco) dell'algoritmo e
	 *         il valore � lo score calcolato
	 */
	private HashMap<String, Integer> calculateTransmissionMethodsScore(
			TransmissionPolicy policy,
			ArrayList<TransmissionMethod> supportedTransmissionMethod) {

		HashMap<String, Integer> scores = new HashMap<String, Integer>();

		for (TransmissionMethod method : supportedTransmissionMethod) {

			int score = 0;
			if (policy.getTransmissionPreferences().size() == 0) {
				score = method.getComputationalComplexity() +
						method.getEfficiency() +
						method.getKeyLenght() +
						method.getStrength();
			} else {
				for (int i = 0; i < policy.getTransmissionPreferences().size(); i++) {
					int methodAttrScore = method.getScore(policy
							.getTransmissionPreferences().get(i));
					score += methodAttrScore * (i + 1);
				}	
			}
			scores.put(method.getName(), score);
		}
		return scores;
	}

	/**
	 * A partire dagli score calcolati si ottiene il metodo crittografico di
	 * trasmissione migliore
	 * 
	 * @param scores
	 *            gli score calcolati
	 * @return il nome dell'algoritmo
	 */
	private String chooseTransmissionMethod(HashMap<String, Integer> scores) {
		String bestMethodName = ""; //$NON-NLS-1$
		int maxScore = -1;
		for (String authMethod : scores.keySet()) {
			int score = scores.get(authMethod);
			if (score > maxScore) {
				maxScore = score;
				bestMethodName = authMethod;
			}
		}

		return bestMethodName;
	}

	/**
	 * Inizializza il protocollo di lettura scrittura sul tsd
	 * 
	 * @param tsd
	 *            il tsd da utilizzare
	 * @return true se l'inizializzazione � andata a buon fine, false altrimenti
	 */
	private boolean initializeProtocol(TSDDescriptor tsd) {
		// genero la coppia di chiavi
		KeyPair keyPair = CertificateGenerator.generateAsymmetricKeyPair();
		// comunico al memory manager la chiave privata
		this.memoryManager.setPrivateKey(keyPair.getPrivateKey());

		// ottengo la lista dei metodi simmetrici supportati dal tsd
		ArrayList<TransmissionMethod> supportedTransmissionMethod = this.probeSilo
				.getTransmissionMethod();

		// chiedo politica
		TransmissionPolicy policy = gui.askTransmissionPolicy();
		Logger.log(
				Messages.getString("SimulatorManager.TRANSM_POLICY_INSERTED"), policy); //$NON-NLS-1$

		// calcolo scores
		HashMap<String, Integer> scores = this
				.calculateTransmissionMethodsScore(policy,
						supportedTransmissionMethod);
		Logger.log(
				Messages.getString("SimulatorManager.SCORE_CALCULATED"), scores); //$NON-NLS-1$

		// sceglo il migliore
		String methodName = this.chooseTransmissionMethod(scores);
		Logger.log(
				Messages.getString("SimulatorManager.TRANSM_METHOD_CHOOSED"), methodName, scores.get(methodName)); //$NON-NLS-1$

		SecretKey symmKey = CertificateGenerator
				.generateSymmetricKey(methodName);
		this.memoryTransferSymmetricKey = symmKey;
		this.transmissionMethod = methodName;

		String keyRepresentation = this.chiper(symmKey, keyPair.getPublicKey());
		this.memoryManager
				.sendCipherSymmetricKey(keyRepresentation, methodName);
		return true;
	}
}
