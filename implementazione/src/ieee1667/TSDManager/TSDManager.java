package ieee1667.TSDManager;

import ieee1667.common.model.TSDDescriptor;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

/**
 * Manager, si occupa della gestione dei dispositivi (CRUD)
 * @author mauro
 */
public class TSDManager {

	/**
	 * Sessione di lavoro di Hibernate
	 */
    private Session hibernateSession;
    
    /**
     * Costruttore di default
     */
    public TSDManager () {
        Configuration cfg = new Configuration().configure("hibernate.cfg.xml");
        ServiceRegistry serviceRegistry = new ServiceRegistryBuilder().applySettings(cfg.getProperties()).buildServiceRegistry();        
        hibernateSession = cfg.buildSessionFactory(serviceRegistry).openSession();
    }
    
    /**
     * Torna una lista di tutti i TSD disponibili
     * @return la lista di tutti i TSD disponibili
     */
    public List<TSDDescriptor> getTSDList() {
        Query listQuery = hibernateSession.createQuery("from TSDDescriptor");
        return listQuery.list();
    }
    
    /**
     * Persiste le modifiche apportate ad un TSD
     * @param tsd il TSD modificato
     */
    public void updateTSD(TSDDescriptor tsd) {
        Transaction t = this.hibernateSession.beginTransaction();
        this.hibernateSession.update(tsd);
        t.commit();
    }
    
    /**
     * Salva un nuovo TSD
     * @param tsd
     */
    public void saveTSD(TSDDescriptor tsd) {
        Transaction t = this.hibernateSession.beginTransaction();
        this.hibernateSession.save(tsd);
        t.commit();
    }
}
