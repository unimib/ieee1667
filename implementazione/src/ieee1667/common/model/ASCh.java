package ieee1667.common.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToOne;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.StandardToStringStyle;

/**
 * Certificato ASCh
 * @author mauro
 */
@Entity
public class ASCh extends Certificate {
    @OneToOne(cascade=CascadeType.ALL)
    private PublicASKP publicKey;
    
    @Override
    public String toString() {
        StandardToStringStyle style = new StandardToStringStyle();
        style.setFieldSeparator(", ");
        style.setUseClassName(false);
        style.setUseIdentityHashCode(false);
        return new ReflectionToStringBuilder(this, style).toString();
    }  

    public PublicASKP getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(PublicASKP publicKey) {
        this.publicKey = publicKey;
    }


	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}

	
    
    
}
