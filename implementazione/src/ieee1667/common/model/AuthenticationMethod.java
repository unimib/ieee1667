package ieee1667.common.model;

import javax.persistence.Entity;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.StandardToStringStyle;

/**
 * Metodo di crittografia per l'autenticazione
 * @author mauro
 */
@Entity
public class AuthenticationMethod extends CryptographicMethod {

	public enum AuthenticationAttribute {
		COMPUTATIONAL_COMPLEXITY,
		MEMORY_COMPLEXITY,
		PUBLIC_KEY_LENGTH,
		STRENGTH
	};
	
	private int computationalComplexity;
	private int memoryComplexity;
	private int publicKeyLenght;
	private int strength;
	
	public int getComputationalComplexity() {
		return computationalComplexity;
	}
	public void setComputationalComplexity(int computationalComplexity) {
		this.computationalComplexity = computationalComplexity;
	}
	public int getMemoryComplexity() {
		return memoryComplexity;
	}
	public void setMemoryComplexity(int memoryComplexity) {
		this.memoryComplexity = memoryComplexity;
	}
	public int getPublicKeyLenght() {
		return publicKeyLenght;
	}
	public void setPublicKeyLenght(int publicKeyLenght) {
		this.publicKeyLenght = publicKeyLenght;
	}
	public int getStrength() {
		return strength;
	}
	public void setStrength(int strength) {
		this.strength = strength;
	}
	
	/**
	 * Torna il valore di un determinato attributo
	 * @param attr l'attributo
	 * @return il valore dell'attributo richiesto
	 */
	public int getScore(AuthenticationAttribute attr) {
		switch (attr) {
		case COMPUTATIONAL_COMPLEXITY:
			return this.getComputationalComplexity();

		case MEMORY_COMPLEXITY:
			return this.getMemoryComplexity();

		case PUBLIC_KEY_LENGTH:
			return this.getPublicKeyLenght();
			
		case STRENGTH:
			return this.getStrength();

		default:
			return 0;
		}
	}
	
    @Override
    public String toString() {
        StandardToStringStyle style = new StandardToStringStyle();
        style.setFieldSeparator(", ");
        style.setUseClassName(false);
        style.setUseIdentityHashCode(false);
        return new ReflectionToStringBuilder(this, style).toString();
    }
	
	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}
	
	
}
