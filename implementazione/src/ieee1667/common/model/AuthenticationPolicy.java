package ieee1667.common.model;

import ieee1667.common.model.AuthenticationMethod.AuthenticationAttribute;

import java.util.ArrayList;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.StandardToStringStyle;

/**
 * Rappresenta una policy per la scelta di un metodo di autenticazione
 * @author mauro
 *
 */
public class AuthenticationPolicy extends Policy {

	/**
	 * lista dei valori della policy. Si assume che prima vengono inseriti quelli meno importanti e poi quelli più
	 * importanti. 
	 * Eg index 0 -> complessità computazionale e index 1-> complessità in memoria. Il secondo è più importante del primo
	 */
	private ArrayList<AuthenticationAttribute> authenticationPreferences;

	
	
	public AuthenticationPolicy(ArrayList<AuthenticationAttribute> authenticationPreferences) {
		this.authenticationPreferences = authenticationPreferences;
	}

	public ArrayList<AuthenticationAttribute> getAuthenticationPreferences() {
		return authenticationPreferences;
	}
	
    @Override
    public String toString() {
        StandardToStringStyle style = new StandardToStringStyle();
        style.setFieldSeparator(", ");
        style.setUseClassName(false);
        style.setUseIdentityHashCode(false);
        return new ReflectionToStringBuilder(this, style).toString();
    }
    
	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}
}
