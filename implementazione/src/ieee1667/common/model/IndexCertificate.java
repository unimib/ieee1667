package ieee1667.common.model;

import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.StandardToStringStyle;

/**
 * Rappresenta l'insieme dei cerificati presenti in un determinato tsd
 * @author mauro
 */
@Entity
public class IndexCertificate extends Model {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
    
    @OneToOne(cascade=CascadeType.ALL)
    private ASCm ascm;
    
    @OneToOne(cascade=CascadeType.ALL)
    private PCp pcp;
    
    @OneToOne(cascade=CascadeType.ALL)
    private ASCh asch;
    
    @OneToMany(cascade=CascadeType.ALL)
    private List<HCh> hostCertificates;
    
    
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ASCm getAscm() {
        return ascm;
    }

    public void setAscm(ASCm ascm) {
        this.ascm = ascm;
    }

    public PCp getPcp() {
        return pcp;
    }

    public void setPcp(PCp pcp) {
        this.pcp = pcp;
    }

    public ASCh getAsch() {
        return asch;
    }

    public void setAsch(ASCh asch) {
        this.asch = asch;
    }

    public List<HCh> getHostCertificates() {
        return hostCertificates;
    }

    public void setHostCertificates(List<HCh> hostCertificates) {
        this.hostCertificates = hostCertificates;
    }
    
    
    @Override
    public String toString() {
        StandardToStringStyle style = new StandardToStringStyle();
        style.setFieldSeparator(", ");
        style.setUseClassName(false);
        style.setUseIdentityHashCode(false);
        return new ReflectionToStringBuilder(this, style).toString();
    }
    
	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}
    
}
