package ieee1667.common.model;

import ieee1667.Logger.Logger;
import java.security.KeyFactory;
import java.security.spec.EncodedKeySpec;
import java.security.spec.PKCS8EncodedKeySpec;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.StandardToStringStyle;
import org.bouncycastle.util.encoders.Base64;

/**
 * Chiave privata
 * @author mauro
 */
@Entity
public class PrivateKey  extends Model {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;

    @Column(columnDefinition="LONGTEXT")
    String value;
    
    @OneToOne(mappedBy="privateKey", cascade=CascadeType.ALL)
    private KeyPair keyPair;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public KeyPair getKeyPair() {
        return keyPair;
    }

    public void setKeyPair(KeyPair keyPair) {
        this.keyPair = keyPair;
    }
    
    public java.security.PrivateKey getJavaVersion() {
        try {
            EncodedKeySpec spec = new PKCS8EncodedKeySpec(Base64.decode(this.getValue()));
            KeyFactory kf = KeyFactory.getInstance("RSA");
            java.security.PrivateKey pk = kf.generatePrivate(spec); 
            return pk;
        } catch (Exception e) {
            Logger.log("Impossibile creare la chiave privata", e.getMessage());
            return null;
        }
    }
    
    @Override
    public String toString() {
        StandardToStringStyle style = new StandardToStringStyle();
        style.setFieldSeparator(", ");
        style.setUseClassName(false);
        style.setUseIdentityHashCode(false);
        return new ReflectionToStringBuilder(this, style).toString();
    }
    
	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}
}
