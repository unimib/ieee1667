package ieee1667.common.model;

import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.StandardToStringStyle;

/**
 * Classe che rappresenta il probe di un TSD
 * @author mauro
 */

@Entity
public class Probe extends Model {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;

    
    @ManyToMany(cascade = {CascadeType.ALL})
    @JoinTable(name="Probe_supportedAuthenticationMethods")
    private List<AuthenticationMethod> supportedAuthenticationMethods;
    
    @ManyToMany(cascade = {CascadeType.ALL})
    @JoinTable(name="Probe_supportedTransmissionMethods")
    private List<TransmissionMethod> supportedTransmissionMethods;
    
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<AuthenticationMethod> getSupportedAuthenticationMethods() {
        return supportedAuthenticationMethods;
    }

    public void setSupportedAuthenticationMethods(List<AuthenticationMethod> supportedAuthenticationMethods) {
        this.supportedAuthenticationMethods = supportedAuthenticationMethods;
    }

    public List<TransmissionMethod> getSupportedTransmissionMethods() {
        return supportedTransmissionMethods;
    }

    public void setSupportedTransmissionMethods(List<TransmissionMethod> supportedTransmissionMethods) {
        this.supportedTransmissionMethods = supportedTransmissionMethods;
    }
    
    
 
    @Override
    public String toString() {
        StandardToStringStyle style = new StandardToStringStyle();
        style.setFieldSeparator(", ");
        style.setUseClassName(false);
        style.setUseIdentityHashCode(false);
        return new ReflectionToStringBuilder(this, style).toString();
    }
    
	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}
    
}
