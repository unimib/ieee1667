package ieee1667.common.model;


import java.util.HashMap;
import java.util.Map;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.StandardToStringStyle;

/**
 * Modello che rappresenta un TSD nell'applicazione
 * @author mauro
 */
@Entity
public class TSDDescriptor extends Model {
    
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
    
    private String name;
    
    @OneToOne(cascade=CascadeType.ALL)
    private TSDMemory memory;
    
    @OneToOne(cascade=CascadeType.ALL)
    private Probe probe;
    
    @OneToOne(cascade=CascadeType.ALL)
    private IndexCertificate indexCertificate;
    
    @OneToMany(cascade=CascadeType.ALL)
    private Map<String, UserIndexMemory> userIndexMemory;
    
    @OneToOne(cascade=CascadeType.ALL)
    private KeyPair askpKeyPair;    
    
    public TSDDescriptor () {
        this.memory = new TSDMemory();
        this.probe = new Probe();
        this.indexCertificate = new IndexCertificate();
        this.userIndexMemory = new HashMap<String, UserIndexMemory>();
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
        
    public TSDMemory getMemory() {
        return memory;
    }

    public void setMemory(TSDMemory memory) {
        this.memory = memory;
    }

    public Probe getProbe() {
        return probe;
    }

    public void setProbe(Probe probe) {
        this.probe = probe;
    }

    public IndexCertificate getIndexCertificate() {
        return indexCertificate;
    }

    public void setIndexCertificate(IndexCertificate indexCertificate) {
        this.indexCertificate = indexCertificate;
    }

    public Map<String, UserIndexMemory> getUserIndexMemory() {
        return userIndexMemory;
    }

    public void setUserIndexMemory(HashMap<String, UserIndexMemory> userIndexMemory) {
        this.userIndexMemory = userIndexMemory;
    }

    public KeyPair getAskpKeyPair() {
		return askpKeyPair;
	}

	public void setAskpKeyPair(KeyPair askpKeyPair) {
		this.askpKeyPair = askpKeyPair;
	}
	
	

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
    public String toString() {
        StandardToStringStyle style = new StandardToStringStyle();
        style.setFieldSeparator(", ");
        style.setUseClassName(false);
        style.setUseIdentityHashCode(false);
        return new ReflectionToStringBuilder(this, style).toString();
    }
	
	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}
    
    
    
}
