package ieee1667.common.model;

import java.util.Map;
import java.util.TreeMap;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.StandardToStringStyle;

/**
 * Rappresentazione della memoria di un TSD
 * @author mauro
 */
@Entity
public class TSDMemory extends Model {
    
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
    
    @ElementCollection
    @CollectionTable( name = "TSDMemory_elements", joinColumns = @JoinColumn( name = "id" ) )
    @Column( name = "memory" )
    private Map<Integer, String> memory;
    
    /**
     * Costruttore di default
     */
    public TSDMemory() {
        this.memory = new TreeMap<Integer, String>();
    }



    public Map<Integer, String> getMemory() {
        return memory;
    }

    public void setMemory(TreeMap<Integer, String> memory) {
        this.memory = memory;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        StandardToStringStyle style = new StandardToStringStyle();
        style.setFieldSeparator(", ");
        style.setUseClassName(false);
        style.setUseIdentityHashCode(false);
        return new ReflectionToStringBuilder(this, style).toString();
    }
    
	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}
    
    
    
}
