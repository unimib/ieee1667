package ieee1667.common.model;

import javax.persistence.Entity;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.StandardToStringStyle;

/**
 * Classe che rappresenta un metodo di crittografia per la trasmissione dati
 * @author mauro
 */
@Entity
public class TransmissionMethod extends CryptographicMethod {
	public enum TransmissionAttribute {
		COMPUTATIONAL_COMPLEXITY,
		EFFICIENCY,
		KEY_LENGTH,
		STRENGTH
	};
	
	private int computationalComplexity;
	private int efficiency;
	private int keyLenght;
	private int strength;
	public int getComputationalComplexity() {
		return computationalComplexity;
	}
	public void setComputationalComplexity(int computationalComplexity) {
		this.computationalComplexity = computationalComplexity;
	}
	public int getEfficiency() {
		return efficiency;
	}
	public void setEfficiency(int efficiency) {
		this.efficiency = efficiency;
	}
	public int getKeyLenght() {
		return keyLenght;
	}
	public void setKeyLenght(int keyLenght) {
		this.keyLenght = keyLenght;
	}
	public int getStrength() {
		return strength;
	}
	public void setStrength(int strength) {
		this.strength = strength;
	}
	
	
	/**
	 * Torna il valore di un determinato attributo
	 * @param attr l'attributo
	 * @return il valore dell'attributo richiesto
	 */
	public int getScore(TransmissionAttribute attr) {
		switch (attr) {
		case COMPUTATIONAL_COMPLEXITY:
			return this.getComputationalComplexity();

		case EFFICIENCY:
			return this.getEfficiency();

		case KEY_LENGTH:
			return this.getKeyLenght();
			
		case STRENGTH:
			return this.getStrength();

		default:
			return 0;
		}
	}
	
    @Override
    public String toString() {
        StandardToStringStyle style = new StandardToStringStyle();
        style.setFieldSeparator(", ");
        style.setUseClassName(false);
        style.setUseIdentityHashCode(false);
        return new ReflectionToStringBuilder(this, style).toString();
    }   
    
	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}
	
	
	    
}
