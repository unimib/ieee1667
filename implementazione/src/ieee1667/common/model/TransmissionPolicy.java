package ieee1667.common.model;

import ieee1667.common.model.TransmissionMethod.TransmissionAttribute;

import java.util.ArrayList;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.StandardToStringStyle;

/**
 * Rappresenta una policy per la scelta di un metodo di trasmissione
 * @author mauro
 *
 */
public class TransmissionPolicy extends Policy {

	/**
	 * lista dei valori della policy. Si assume che prima vengono inseriti quelli meno importanti e poi quelli pi��
	 * importanti. 
	 * Eg index 0 -> complessit�� computazionale e index 1-> complessit�� in memoria. Il secondo �� pi�� importante del primo
	 */
	private ArrayList<TransmissionAttribute> transmissionPreferences;

	
	
	public TransmissionPolicy(ArrayList<TransmissionAttribute> transmissionPreferences) {
		this.transmissionPreferences = transmissionPreferences;
	}

	public ArrayList<TransmissionAttribute> getTransmissionPreferences() {
		return transmissionPreferences;
	}
	
    @Override
    public String toString() {
        StandardToStringStyle style = new StandardToStringStyle();
        style.setFieldSeparator(", ");
        style.setUseClassName(false);
        style.setUseIdentityHashCode(false);
        return new ReflectionToStringBuilder(this, style).toString();
    }
    
	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}
}
