/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ieee1667.common.model;

import java.util.List;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.StandardToStringStyle;

/**
 * Classe che rappresenta i permessi di un certo utente sulla memoria di un dispositivo
 * @author mauro
 */
@Entity
public class UserIndexMemory extends Model {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
    
    @ElementCollection
    @CollectionTable( name = "userIndexMemory_read", joinColumns = @JoinColumn( name = "id" ) )
    @Column( name = "readColumn" )
    private List<Integer> read;
    
    @ElementCollection
    @CollectionTable( name = "userIndexMemory_write", joinColumns = @JoinColumn( name = "id" ) )
    @Column( name = "writeColumn" )
    private List<Integer> write;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<Integer> getRead() {
        return read;
    }

    public void setRead(List<Integer> read) {
        this.read = read;
    }

    public List<Integer> getWrite() {
        return write;
    }

    public void setWrite(List<Integer> write) {
        this.write = write;
    }
    
    @Override
    public String toString() {
        StandardToStringStyle style = new StandardToStringStyle();
        style.setFieldSeparator(", ");
        style.setUseClassName(false);
        style.setUseIdentityHashCode(false);
        return new ReflectionToStringBuilder(this, style).toString();
    }
    
	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}
    
}
