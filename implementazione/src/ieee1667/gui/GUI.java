package ieee1667.gui;

import ieee1667.Constant.OPERATION_CHOICE;
import ieee1667.Constant.USER_OPERATION;
import ieee1667.common.model.AuthenticationMethod;
import ieee1667.common.model.AuthenticationMethod.AuthenticationAttribute;
import ieee1667.common.model.AuthenticationPolicy;
import ieee1667.common.model.TSDDescriptor;
import ieee1667.common.model.TSDMemory;
import ieee1667.common.model.TransmissionMethod;
import ieee1667.common.model.TransmissionMethod.TransmissionAttribute;
import ieee1667.common.model.TransmissionPolicy;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * Questa classe viene utilizzata per permettere di richiedere informazioni all'utente e per visualizzare messaggi
 * @author mauro
 *
 */
public class GUI {
	
	private PrintStream out;
	private IEEE1667Scanner reader;
	
	/**
	 * Costruttore di default
	 */
	public GUI() {
		this(new IEEE1667Scanner());
	}
	
	/**
	 * Costruttore utilizzato solitamente a scopo di test
	 * @param reader un reader da utilizzare per acquisire i dati dall'utente (solitament � un oggetto mock)
	 */
	public GUI(IEEE1667Scanner reader) {
		this.out = System.out;
		this.reader = reader;
	}
	
	/**
	 * Richiede all'utente di specificare un tsd
	 * @param tsdList
	 * @return il tsd scelto
	 */
	public TSDDescriptor askTSD(List<TSDDescriptor> tsdList) {
		
		this.out.println("Scegli il tsd da utilizzare");
		for(int i=0; i<tsdList.size(); i++) {
			this.out.println(i+" - " +tsdList.get(i).getName()); // sostituire con cosa?
		}
		
		return tsdList.get(askIndex(0, tsdList.size()-1));
	}
	
	/**
	 * Visualizza un messaggio all'utente
	 * @param message il messaggio da visualizzare
	 */
	public void showMessage(String message) {
		this.out.println(message);
	}
	
	
	/**
	 * Richiede all'utente di inserire un numero compreso tra min e max
	 * @param min
	 * @param max
	 * @return un numero tra min e max
	 */
	private int askIndex(int min, int max) {
		boolean done = false;
		int choice = min-1;
		do {
			this.out.print("Scelta: ");
			try {
				choice = Integer.parseInt(this.reader.nextLine().trim());
			} catch (Exception e) {
				this.out.println("\nScelta non valida");
				continue;
			}
			if (choice >= min && choice <= max) {
				done = true;
			} else {
				this.out.println("\nScelta non valida");
			}
		} while(!done);
		return choice;
	}
	
	/**
	 * Richiede l'operazione utente da svolgere
	 * @return l'operazione da svolgere
	 */
	public USER_OPERATION askUserOperation() {
		this.out.println("\nChe operazione vuoi eseguire?");
		this.out.println("1 - leggi");
		this.out.println("2 - scrivi");
		this.out.println("3 - nessuna");
		int choice = askIndex(1,3);
		
		if (choice == 1) return USER_OPERATION.READ;
		else if(choice == 2) return USER_OPERATION.WRITE;
		else return USER_OPERATION.NONE;
	}

	/**
	 * Richiede l'user id
	 * @return l'userID
	 */
	public String askUserID() {
		String userID = "";
		boolean validUserID = false;
		
		do {
			
			this.out.print("Inserire l'userID\n");
			userID = this.reader.nextLine().trim();
			
			//controlli sull'input
			if(userID.equals("")) continue;
			if(userID.contains(" ")) continue;
			if(!userID.matches("[a-zA-Z0-9 ]*")) continue;
			
			validUserID = true;
			
		} while (!validUserID);
		
		return userID;
	}
	
	/**
	 * Richiede la password
	 */
	public String askPassword() {
		String password = "";
		boolean validPassword = false;
		
		do {
			
			this.out.print("Inserire la password\n");
			password = this.reader.nextLine().trim();
			
			//controlli sull'input
			if(password.equals("")) continue;
			if(password.contains(" ")) continue;
			
			validPassword = true;
			
		} while (!validPassword);
		
		return password;
	}

	/**
	 * Chieder un index a partire dalla memoria del tsd
	 * @param memory la memoria del tsd da leggere/scrivere
	 * @return l'indice della memoria in cui leggere o scrivere
	 */
	public int askMemoryIndex(TSDMemory memory) {
		Set<Integer> idxs = memory.getMemory().keySet();
		int max = Collections.max(idxs);
		int min = Collections.min(idxs);
		this.out.println("Inserisci un identificatore per la memoria (da "+min+" a +"+max+")");
		return this.askIndex(min, max);
	}

	/**
	 * Richiede all'utente di inserire un nuovo valore da inserire in una cella di memoria
	 * @return il nuovo valore da inserire in memoria
	 */
	public String askNewValueToWrite() {
		//fix
		this.out.print("Inserire il nuovo valore da inserire nell'area di memoria\n");
		return this.reader.nextLine().trim();
	}

	/**
	 * Richiede all'utente di inserire una policy per la scelta del metodo di autenticazione
	 * @return una policy di autenticazione
	 */
	public AuthenticationPolicy askAuthenticationPolicy() {
		
		this.out.println("Inserisci quali attributi considerare nella scelta del metodo crittografico di autenticazione (scegli prima i pi� rilevanti)");
		
		ArrayList<AuthenticationAttribute> userAttrs = new ArrayList<AuthenticationAttribute>();
		ArrayList<AuthenticationAttribute> allAttrs = new ArrayList<AuthenticationAttribute>(Arrays.asList(AuthenticationMethod.AuthenticationAttribute.values()));
		
		int choose = -1;
		do {
			this.out.println("Scegli prossimo attributo");
			this.out.println("-1 - Termina selezione");
			for(int i=0; i<allAttrs.size(); i++) {
				this.out.println(i+" - " +allAttrs.get(i));
			}
			choose = askIndex(-1, allAttrs.size()-1);
			if(choose >= 0) {
				userAttrs.add(allAttrs.get(choose));
				allAttrs.remove(choose);
			} 
		} while(choose >= 0);
		
		/* Adeguo l'ordine, prima i meno importanti poi i pi� importanti */
		Collections.reverse(userAttrs);
		return new AuthenticationPolicy(userAttrs);
	}

	/**
	 * Richiede all'utente di inserire una policy per la scelta del metodo di trasmissione
	 * @return una policy di trasmissione
	 */
	public TransmissionPolicy askTransmissionPolicy() {
		this.out.println("Inserisci quali attributi considerare nella scelta del metodo crittografico di trasmissione (scegli prima i pi� rilevanti)");
		
		ArrayList<TransmissionAttribute> userAttrs = new ArrayList<TransmissionAttribute>();
		ArrayList<TransmissionAttribute> allAttrs = new ArrayList<TransmissionAttribute>(Arrays.asList(TransmissionMethod.TransmissionAttribute.values()));
		
		int choose = -1;
		do {
			this.out.println("Scegli prossimo attributo");
			this.out.println("-1 - Termina selezione");
			for(int i=0; i<allAttrs.size(); i++) {
				this.out.println(i+" - " +allAttrs.get(i));
			}
			choose = askIndex(-1, allAttrs.size()-1);
			
			if(choose >= 0) {
				userAttrs.add(allAttrs.get(choose));
				allAttrs.remove(choose);
			}
		} while(choose >= 0);
		
		/* Adeguo l'ordine, prima i meno importanti poi i pi� importanti */
		Collections.reverse(userAttrs);
		return new TransmissionPolicy(userAttrs);
	}

	/**
	 * Richiede l'operazione da svolgere
	 * @return una operazione
	 */
	public OPERATION_CHOICE askOperation() {
		this.out.println("\nChe operazione vuoi eseguire?");
		this.out.println("1 - Sessione utente");
		this.out.println("2 - Sessione amministratore");
		this.out.println("2 - Gestisci TSD");
		this.out.println("4 - nessuna");
		int choice = askIndex(1,4);
		
		if (choice == 1) return OPERATION_CHOICE.USER_SESSION;
		else if(choice == 2) return OPERATION_CHOICE.ADMIN_SESSION;
		else if(choice == 3) return OPERATION_CHOICE.MANAGE_TSD;
		else return OPERATION_CHOICE.NONE;
	}
}
