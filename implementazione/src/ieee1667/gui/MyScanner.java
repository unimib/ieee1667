package ieee1667.gui;

import java.util.Scanner;

/**
 * Classe helper.
 * Incapsula il metodo nextLine di Scanner.
 * Ha il solo scopo di permettere il testing degli input utente attraverso Mockito.
 * In particolare Mockito non permette di effettuare mock di classi final (e quindi non � possibile
 * effettuare il mock di java.util.Scanner, che veniva in precedenza utilizzata nella classe GUI)
 * @author Mauro
 *
 */
public class MyScanner {

	private Scanner scanner;
	
	/**
	 * Costruttore di default
	 */
	public MyScanner() {
		this.scanner = new Scanner(System.in);
	}
	
	/**
	 * Torna una stringa inserita dall'utente. In particolare chiama
	 * il metodo java.util.Scanner.nextLine
	 * @return una stringa
	 */
	public String nextLine() {
		return this.scanner.nextLine();
	}
	
	
	
	
}
