package ieee1667.gui;

import static org.junit.Assert.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;

import ieee1667.common.model.AuthenticationPolicy;
import ieee1667.common.model.AuthenticationMethod.AuthenticationAttribute;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


@RunWith( MockitoJUnitRunner.class )
public class TestAuthenticationPolicyChoice {

	@Mock private IEEE1667Scanner mockScanner;
	private GUI gui;
	
	@Before
	public void setUp() {
		gui = new GUI(mockScanner);
	}
	
	@Test
	// 2.2.0
	public void shouldReturnPolicy() {
		when(mockScanner.nextLine()).thenReturn("1", "2", "-1");
		AuthenticationPolicy policy = gui.askAuthenticationPolicy();
		
		ArrayList<AuthenticationAttribute> attr = new ArrayList<AuthenticationAttribute>();
		attr.add(AuthenticationAttribute.STRENGTH);
		attr.add(AuthenticationAttribute.MEMORY_COMPLEXITY);
		AuthenticationPolicy expectedPolicy = new AuthenticationPolicy(attr);
		
		assertEquals(expectedPolicy, policy);
		verify(mockScanner, times(3)).nextLine();
	}
	
	@Test
	// 2.2.1
	public void shouldReturnFullPolicy() {
		when(mockScanner.nextLine()).thenReturn("0", "0", "0", "0", "-1");
		AuthenticationPolicy policy = gui.askAuthenticationPolicy();
		
		ArrayList<AuthenticationAttribute> attr = new ArrayList<AuthenticationAttribute>();
		attr.add(AuthenticationAttribute.STRENGTH);
		attr.add(AuthenticationAttribute.PUBLIC_KEY_LENGTH);
		attr.add(AuthenticationAttribute.MEMORY_COMPLEXITY);
		attr.add(AuthenticationAttribute.COMPUTATIONAL_COMPLEXITY);
		
		AuthenticationPolicy expectedPolicy = new AuthenticationPolicy(attr);
		
		assertEquals(expectedPolicy, policy);
		verify(mockScanner, times(5)).nextLine();
	}
	
	@Test
	// 2.2.2
	public void shouldReturnPolicyWithOneElement() {
		when(mockScanner.nextLine()).thenReturn("0", "-1");
		AuthenticationPolicy policy = gui.askAuthenticationPolicy();
		
		ArrayList<AuthenticationAttribute> attr = new ArrayList<AuthenticationAttribute>();
		attr.add(AuthenticationAttribute.COMPUTATIONAL_COMPLEXITY);
		
		AuthenticationPolicy expectedPolicy = new AuthenticationPolicy(attr);
		
		assertEquals(expectedPolicy, policy);
		verify(mockScanner, times(2)).nextLine();
	}	
	
	@Test
	// 2.2.3
	public void shouldAskAgainOnEmptyInput() {
		when(mockScanner.nextLine()).thenReturn("-1");
		AuthenticationPolicy policy = gui.askAuthenticationPolicy();
		
		ArrayList<AuthenticationAttribute> attr = new ArrayList<AuthenticationAttribute>();
		
		AuthenticationPolicy expectedPolicy = new AuthenticationPolicy(attr);
		
		assertEquals(expectedPolicy, policy);
		verify(mockScanner, times(1)).nextLine();
	}
	
	@Test
	// 2.2.4
	public void shouldAskAgainOnWrongInput() {
		when(mockScanner.nextLine()).thenReturn("0", "10", "0", "-1");
		AuthenticationPolicy policy = gui.askAuthenticationPolicy();
		
		ArrayList<AuthenticationAttribute> attr = new ArrayList<AuthenticationAttribute>();
		attr.add(AuthenticationAttribute.MEMORY_COMPLEXITY);
		attr.add(AuthenticationAttribute.COMPUTATIONAL_COMPLEXITY);
		
		AuthenticationPolicy expectedPolicy = new AuthenticationPolicy(attr);
		
		assertEquals(expectedPolicy, policy);
		verify(mockScanner, times(4)).nextLine();
	}
}
