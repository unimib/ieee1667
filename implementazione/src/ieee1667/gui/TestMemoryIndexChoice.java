package ieee1667.gui;

import static org.junit.Assert.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.TreeMap;

import ieee1667.common.model.TSDMemory;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith( MockitoJUnitRunner.class )
public class TestMemoryIndexChoice {

	@Mock private IEEE1667Scanner mockScanner;
	private GUI gui;
	private TSDMemory tsdMemory;
	
	@Before
	public void setUp() {
		gui = new GUI(mockScanner);
		
		//creo memoria
		tsdMemory = new TSDMemory();
		TreeMap<Integer, String> map = new TreeMap<Integer, String>();
		for(int i=0; i<10; i++) {
			map.put(i, "memoria " + i);
		}
		
		tsdMemory.setMemory(map);
	}
	
	
	@Test
	// 2.7.0
	public void shouldReturnMemoryIndex() {
		when(mockScanner.nextLine()).thenReturn("1");
		int idx = gui.askMemoryIndex(tsdMemory);
		
		//controllo che il risultato sia corretto
		assertEquals("memoria "+idx, tsdMemory.getMemory().get(1));
		
		//controllo che sia stato richiesto un solo dato aall'utente
		verify(mockScanner, times(1)).nextLine();
	}
	
	@Test
	// 2.7.2
	public void shoudlAskAgainMemoryIndexOnEmptyInput() {
		when(mockScanner.nextLine()).thenReturn("", "1");
		int idx = gui.askMemoryIndex(tsdMemory);
		
		//controllo che il risultato sia corretto
		assertEquals("memoria "+idx, tsdMemory.getMemory().get(1));
		
		//controllo che sia stato richiesto un solo dato aall'utente
		verify(mockScanner, times(2)).nextLine();	
	}
	
	@Test
	// 2.7.3
	public void shoudlAskAgainMemoryIndexOnIndexNotInMemory() {
		when(mockScanner.nextLine()).thenReturn("20", "1");
		int idx = gui.askMemoryIndex(tsdMemory);
		
		//controllo che il risultato sia corretto
		assertEquals("memoria "+idx, tsdMemory.getMemory().get(1));
		
		//controllo che sia stato richiesto un solo dato aall'utente
		verify(mockScanner, times(2)).nextLine();		
	}
	
	@Test
	// 2.7.4
	public void shoudlAskAgainMemoryIndexOnInputWithWhitespaces() {
		when(mockScanner.nextLine()).thenReturn("1 0", "3");
		int idx = gui.askMemoryIndex(tsdMemory);
		
		//controllo che il risultato sia corretto
		assertEquals("memoria "+idx, tsdMemory.getMemory().get(3));
		
		//controllo che sia stato richiesto il dato
		verify(mockScanner, times(2)).nextLine();		
	}
	
	@Test
	// 2.7.5
	public void shoudlAskAgainMemoryIndexOnInputWithSpecialChar() {
		when(mockScanner.nextLine()).thenReturn("10@", "1");
		int idx = gui.askMemoryIndex(tsdMemory);
		
		//controllo che il risultato sia corretto
		assertEquals("memoria "+idx, tsdMemory.getMemory().get(1));
		
		//controllo che sia stato richiesto il dato
		verify(mockScanner, times(2)).nextLine();	
	}

}
