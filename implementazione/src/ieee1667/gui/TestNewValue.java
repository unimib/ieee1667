package ieee1667.gui;

import static org.junit.Assert.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith( MockitoJUnitRunner.class )
public class TestNewValue {

	@Mock private IEEE1667Scanner mockScanner;
	private GUI gui;
	
	@Before
	public void setUp() {
		gui = new GUI(mockScanner);
	}
	
	@Test
	// 2.8.0
	public void shouldAcceptStringWithNoWhiteSpacesAndSpecialChar() {
		when(mockScanner.nextLine()).thenReturn("validInput");
		String input = gui.askNewValueToWrite();
		
		//controllo che il risultato sia corretto
		assertEquals("validInput", input);
		
		//controllo che sia stato richiesto un solo dato all'utente
		verify(mockScanner, times(1)).nextLine();
	}
	
	@Test
	// 2.8.1
	public void shouldAcceptStringWithWhiteSpaces() {
		when(mockScanner.nextLine()).thenReturn("this is a vaid input");
		String input = gui.askNewValueToWrite();
		
		//controllo che il risultato sia corretto
		assertEquals("this is a vaid input", input);
		
		//controllo che sia stato richiesto un solo dato all'utente
		verify(mockScanner, times(1)).nextLine();
	}
	
	@Test
	// 2.8.2
	public void shouldAcceptStringWithSpecialChar() {
		when(mockScanner.nextLine()).thenReturn("mail@domain.it");
		String input = gui.askNewValueToWrite();
		
		//controllo che il risultato sia corretto
		assertEquals("mail@domain.it", input);
		
		//controllo che sia stato richiesto un solo dato all'utente
		verify(mockScanner, times(1)).nextLine();	
	}
	
	@Test
	// 2.8.4
	public void shoudlAcceptEmptyInput() {
		when(mockScanner.nextLine()).thenReturn("");
		String input = gui.askNewValueToWrite();
		
		//controllo che il risultato sia corretto
		assertEquals("", input);
		
		//controllo che sia stato richiesto un solo dato all'utente
		verify(mockScanner, times(1)).nextLine();		
	}
}
