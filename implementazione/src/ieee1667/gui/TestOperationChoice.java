package ieee1667.gui;

import static org.junit.Assert.*;

import ieee1667.Constant.OPERATION_CHOICE;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.*;

@RunWith( MockitoJUnitRunner.class )
public class TestOperationChoice {

	@Mock private IEEE1667Scanner mockScanner;
	private GUI gui;
	
	
	@Before
	public void setUp() {
		gui = new GUI(mockScanner);
	}
	
	
	@Test
	// 2.0.0
	public void shouldStartUserSession() {
		when(mockScanner.nextLine()).thenReturn("1");
		OPERATION_CHOICE choice = gui.askOperation();
		
		//controllo che il risultato sia corretto
		assertEquals(OPERATION_CHOICE.USER_SESSION, choice);
		//controllo che sia stato richiesto un solo dato all'utente
		verify(mockScanner, times(1)).nextLine();
	}
	
	@Test
	// 2.0.1
	public void shouldStartAdminSession() {
		when(mockScanner.nextLine()).thenReturn("2");
		OPERATION_CHOICE choice = gui.askOperation();
		
		//controllo che il risultato sia corretto
		assertEquals(OPERATION_CHOICE.ADMIN_SESSION, choice);
		//controllo che sia stato richiesto un solo dato all'utente
		verify(mockScanner, times(1)).nextLine();
	}

	@Test
	// 2.0.2
	public void shouldStartManageTSD() {
		when(mockScanner.nextLine()).thenReturn("3");
		OPERATION_CHOICE choice = gui.askOperation();
		
		//controllo che il risultato sia corretto
		assertEquals(OPERATION_CHOICE.MANAGE_TSD, choice);
		//controllo che sia stato richiesto un solo dato all'utente
		verify(mockScanner, times(1)).nextLine();
	}
	
	@Test
	// 2.0.3
	public void shoudlAskAgainInputOnEmptyInput() {
		when(mockScanner.nextLine()).thenReturn("", "1");
		gui.askOperation();
		
		//controllo che sia stato richiesto l'input dopo che è stato inserito un input vuoto
		verify(mockScanner, times(2)).nextLine();		
	}
	
	@Test
	// 2.0.4
	public void shoudlAskAgainInputOnWrongInput() {
		when(mockScanner.nextLine()).thenReturn("wrong_input", "5", "1");
		gui.askOperation();
		
		//controllo che sia stato richiesto l'input dopo che è stato inserito un input vuoto
		verify(mockScanner, times(3)).nextLine();		
	}
	
}
