package ieee1667.gui;

import static org.junit.Assert.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import ieee1667.common.model.TSDDescriptor;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith( MockitoJUnitRunner.class )
public class TestTSDChoice {

	@Mock private IEEE1667Scanner mockScanner;
	private GUI gui;
	private List<TSDDescriptor> tsdList;
	
	@Before
	public void setUp() {
		gui = new GUI(mockScanner);
		tsdList = new ArrayList<TSDDescriptor>();
		
		// genero 10 tsd random
		for(int i=0; i<10; i++) {
			TSDDescriptor tsd = new TSDDescriptor();
			// ho solo bisogno del nome
			tsd.setName("TSD numero "+i);
			tsdList.add(tsd);
		}
		
	}
	
	
	@Test
	// 2.1.0
	public void shouldStartUserSession() {
		when(mockScanner.nextLine()).thenReturn("1");
		TSDDescriptor tsd = gui.askTSD(tsdList);
		
		//controllo che il risultato sia corretto
		assertEquals(tsdList.get(1), tsd);
		
		//controllo che sia stato richiesto un solo dato all'utente
		verify(mockScanner, times(1)).nextLine();
	}
	
	@Test
	// 2.1.1
	public void shouldStartUserSessionOnInputWithWhiteSpaces() {
		when(mockScanner.nextLine()).thenReturn("  1  ");
		TSDDescriptor tsd = gui.askTSD(tsdList);
		
		//controllo che il risultato sia corretto
		assertEquals(tsdList.get(1), tsd);
		
		//controllo che sia stato richiesto un solo dato all'utente
		verify(mockScanner, times(1)).nextLine();
	}
	
	@Test
	// 2.1.3
	public void shoudlAskAgainInputOnEmptyInput() {
		when(mockScanner.nextLine()).thenReturn("", "2");
		TSDDescriptor tsd = gui.askTSD(tsdList);
		
		//controllo che il risultato sia corretto
		assertEquals(tsdList.get(2), tsd);
		
		//controllo che sia stato richiesto il dato
		verify(mockScanner, times(2)).nextLine();	
	}
	
	@Test
	// 2.1.4
	public void shouldAskAgainInputOnWrongInput() {
		when(mockScanner.nextLine()).thenReturn("20", "2");
		TSDDescriptor tsd = gui.askTSD(tsdList);
		
		//controllo che il risultato sia corretto
		assertEquals(tsdList.get(2), tsd);
		
		//controllo che sia stato richiesto il dato
		verify(mockScanner, times(2)).nextLine();			
	}
	
	@Test
	// 2.1.5
	public void shoudlAskAgainInputOnNonNumericInput() {
		when(mockScanner.nextLine()).thenReturn("wrong_input", "-1", "1");
		TSDDescriptor tsd = gui.askTSD(tsdList);
		
		//controllo che il risultato sia corretto
		assertEquals(tsdList.get(1), tsd);
		
		//controllo che sia stato richiesto il dato
		verify(mockScanner, times(3)).nextLine();		
	}
	
	

}
