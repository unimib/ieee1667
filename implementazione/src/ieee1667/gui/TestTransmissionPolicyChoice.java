package ieee1667.gui;

import static org.junit.Assert.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;

import ieee1667.common.model.TransmissionPolicy;
import ieee1667.common.model.TransmissionMethod.TransmissionAttribute;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


@RunWith( MockitoJUnitRunner.class )
public class TestTransmissionPolicyChoice {

	@Mock private IEEE1667Scanner mockScanner;
	private GUI gui;
	
	@Before
	public void setUp() {
		gui = new GUI(mockScanner);
	}
	
	@Test
	// 2.5.0
	public void shouldReturnPolicy() {
		when(mockScanner.nextLine()).thenReturn("1", "2", "-1");
		TransmissionPolicy policy = gui.askTransmissionPolicy();
		
		ArrayList<TransmissionAttribute> attr = new ArrayList<TransmissionAttribute>();
		attr.add(TransmissionAttribute.STRENGTH);
		attr.add(TransmissionAttribute.EFFICIENCY);
		TransmissionPolicy expectedPolicy = new TransmissionPolicy(attr);
		
		assertEquals(expectedPolicy, policy);
		verify(mockScanner, times(3)).nextLine();
	}
	
	@Test
	// 2.5.1
	public void shouldReturnFullPolicy() {
		when(mockScanner.nextLine()).thenReturn("0", "0", "0", "0", "-1");
		TransmissionPolicy policy = gui.askTransmissionPolicy();
		
		ArrayList<TransmissionAttribute> attr = new ArrayList<TransmissionAttribute>();
		attr.add(TransmissionAttribute.STRENGTH);
		attr.add(TransmissionAttribute.KEY_LENGTH);
		attr.add(TransmissionAttribute.EFFICIENCY);
		attr.add(TransmissionAttribute.COMPUTATIONAL_COMPLEXITY);
		
		TransmissionPolicy expectedPolicy = new TransmissionPolicy(attr);
		
		assertEquals(expectedPolicy, policy);
		verify(mockScanner, times(5)).nextLine();
	}
	
	@Test
	// 2.5.2
	public void shouldReturnPolicyWithOneElement() {
		when(mockScanner.nextLine()).thenReturn("0", "-1");
		TransmissionPolicy policy = gui.askTransmissionPolicy();
		
		ArrayList<TransmissionAttribute> attr = new ArrayList<TransmissionAttribute>();
		attr.add(TransmissionAttribute.COMPUTATIONAL_COMPLEXITY);
		
		TransmissionPolicy expectedPolicy = new TransmissionPolicy(attr);
		
		assertEquals(expectedPolicy, policy);
		verify(mockScanner, times(2)).nextLine();
	}	

	@Test
	// 2.5.3
	public void shouldAskAgainOnEmptyInput() {
		when(mockScanner.nextLine()).thenReturn("-1");
		TransmissionPolicy policy = gui.askTransmissionPolicy();
		
		ArrayList<TransmissionAttribute> attr = new ArrayList<TransmissionAttribute>();
		
		TransmissionPolicy expectedPolicy = new TransmissionPolicy(attr);
		
		assertEquals(expectedPolicy, policy);
		verify(mockScanner, times(1)).nextLine();
	}

	@Test
	// 2.5.4
	public void shouldAskAgainOnWrongInput() {
		when(mockScanner.nextLine()).thenReturn("0", "10", "0", "-1");
		TransmissionPolicy policy = gui.askTransmissionPolicy();
		
		ArrayList<TransmissionAttribute> attr = new ArrayList<TransmissionAttribute>();
		attr.add(TransmissionAttribute.EFFICIENCY);
		attr.add(TransmissionAttribute.COMPUTATIONAL_COMPLEXITY);
		
		TransmissionPolicy expectedPolicy = new TransmissionPolicy(attr);
		
		assertEquals(expectedPolicy, policy);
		verify(mockScanner, times(4)).nextLine();
	}
}
