package ieee1667.gui;

import static org.junit.Assert.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith( MockitoJUnitRunner.class )
public class TestUserIDChoice {

	@Mock private IEEE1667Scanner mockScanner;
	private GUI gui;
	
	@Before
	public void setUp() {
		gui = new GUI(mockScanner);
	}
	
	
	@Test
	// 2.3.0
	public void shouldReturnUserID() {
		when(mockScanner.nextLine()).thenReturn("validUserID");
		String userID = gui.askUserID();
		
		//controllo che il risultato sia corretto
		assertEquals("validUserID", userID);
		
		//controllo che sia stato richiesto un solo dato all'utente
		verify(mockScanner, times(1)).nextLine();
	}
	
	@Test
	// 2.3.1
	public void shoudlAskAgainUserIDOnEmptyInput() {
		when(mockScanner.nextLine()).thenReturn("", "validUserID");
		String userID = gui.askUserID();
		
		//controllo che il risultato sia corretto
		assertEquals("validUserID", userID);
		
		//controllo che sia stato richiesto il dato
		verify(mockScanner, times(2)).nextLine();	
	}
	
	@Test
	// 2.3.2
	public void shoudlAskAgainUserIDOnInputWithSpecialChar() {
		when(mockScanner.nextLine()).thenReturn("wrong@input", "validUserID");
		String userID = gui.askUserID();
		
		//controllo che il risultato sia corretto
		assertEquals("validUserID", userID);
		
		//controllo che sia stato richiesto il dato
		verify(mockScanner, times(2)).nextLine();		
	}
	
	@Test
	// 2.3.3
	public void shoudlAskAgainUserIDOnInputWithWhitespaces() {
		when(mockScanner.nextLine()).thenReturn("wrong input", "validUserID");
		String userID = gui.askUserID();
		
		//controllo che il risultato sia corretto
		assertEquals("validUserID", userID);
		
		//controllo che sia stato richiesto il dato
		verify(mockScanner, times(2)).nextLine();		
	}
	
	@Test
	// 2.3.4
	public void shoudlAskAgainUserIDOnInputWithWhitespacesAndSpecialChar() {
		when(mockScanner.nextLine()).thenReturn("wrong @ input", "validUserID");
		String userID = gui.askUserID();
		
		//controllo che il risultato sia corretto
		assertEquals("validUserID", userID);
		
		//controllo che sia stato richiesto il dato
		verify(mockScanner, times(2)).nextLine();		
	}

}
