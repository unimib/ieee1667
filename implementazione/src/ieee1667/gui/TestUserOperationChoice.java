package ieee1667.gui;

import static org.junit.Assert.*;

import ieee1667.Constant.USER_OPERATION;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.*;

@RunWith( MockitoJUnitRunner.class )
public class TestUserOperationChoice {

	@Mock private IEEE1667Scanner mockScanner;
	private GUI gui;
	
	
	@Before
	public void setUp() {
		gui = new GUI(mockScanner);
	}
	
	
	@Test
	// 2.6.0
	public void shouldStartReadOperation() {
		when(mockScanner.nextLine()).thenReturn("1");
		USER_OPERATION choice = gui.askUserOperation();
		
		//controllo che il risultato sia corretto
		assertEquals(USER_OPERATION.READ, choice);
		//controllo che sia stato richiesto un solo dato all'utente
		verify(mockScanner, times(1)).nextLine();
	}
	
	@Test
	// 2.6.1
	public void shouldStartWriteOperation() {
		when(mockScanner.nextLine()).thenReturn("2");
		USER_OPERATION choice = gui.askUserOperation();
		
		//controllo che il risultato sia corretto
		assertEquals(USER_OPERATION.WRITE, choice);
		//controllo che sia stato richiesto un solo dato all'utente
		verify(mockScanner, times(1)).nextLine();
	}
	
	@Test
	// 2.6.2
	public void shoudlEndUserOperation() {
		when(mockScanner.nextLine()).thenReturn("3");
		USER_OPERATION choice = gui.askUserOperation();
		
		//controllo che il risultato sia corretto
		assertEquals(USER_OPERATION.NONE, choice);
		//controllo che sia stato richiesto un solo dato all'utente
		verify(mockScanner, times(1)).nextLine();		
	}
	
	@Test
	// 2.6.3
	public void shoudlAskAgainInputOnEmptyInput() {
		when(mockScanner.nextLine()).thenReturn("", "1");
		gui.askUserOperation();
		
		//controllo che sia stato richiesto l'input dopo che è stato inserito un input vuoto
		verify(mockScanner, times(2)).nextLine();		
	}
	
	@Test
	// 2.6.4
	public void shoudlAskAgainInputOnWrongInput() {
		when(mockScanner.nextLine()).thenReturn("wrong_input", "4", "1");
		gui.askUserOperation();
		
		//controllo che sia stato richiesto l'input dopo che è stato inserito un input vuoto
		verify(mockScanner, times(3)).nextLine();		
	}
	
}
