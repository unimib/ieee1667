package ieee1667.gui;

import static org.junit.Assert.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith( MockitoJUnitRunner.class )
public class TestUserPrivateKeyChoice {

	@Mock private IEEE1667Scanner mockScanner;
	private GUI gui;
	
	@Before
	public void setUp() {
		gui = new GUI(mockScanner);
	}
	
	
	@Test
	// 2.4.0
	public void shouldReturnPrivateKey() {
		when(mockScanner.nextLine()).thenReturn("validPrivateKey");
		String privateKey = gui.askPassword();
		
		//controllo che il risultato sia corretto
		assertEquals("validPrivateKey", privateKey);
		
		//controllo che sia stato richiesto un solo dato all'utente
		verify(mockScanner, times(1)).nextLine();
	}
	
	@Test
	// 2.4.1
	public void shouldReturnPrivateKeyWithSpecialChar() {
		when(mockScanner.nextLine()).thenReturn("valid@PrivateKey");
		String privateKey = gui.askPassword();
		
		//controllo che il risultato sia corretto
		assertEquals("valid@PrivateKey", privateKey);
		
		//controllo che sia stato richiesto un solo dato all'utente
		verify(mockScanner, times(1)).nextLine();		
	}
	
	@Test
	// 2.4.2
	public void shoudlAskAgainPrivateKeyOnEmptyInput() {
		when(mockScanner.nextLine()).thenReturn("", "validPrivateKey");
		String privateKey = gui.askPassword();
		
		//controllo che il risultato sia corretto
		assertEquals("validPrivateKey", privateKey);
		
		//controllo che sia stato richiesto il dato
		verify(mockScanner, times(2)).nextLine();	
	}
	
	
	@Test
	// 2.4.3
	public void shoudlAskAgainPrivateKeyOnInputWithWhitespaces() {
		when(mockScanner.nextLine()).thenReturn("wrong input", "validPrivateKey");
		String privateKey = gui.askPassword();
		
		//controllo che il risultato sia corretto
		assertEquals("validPrivateKey", privateKey);
		
		//controllo che sia stato richiesto il dato
		verify(mockScanner, times(2)).nextLine();		
	}
}
