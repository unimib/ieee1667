package test.util;

import org.mockito.ArgumentMatcher;

import ch.qos.logback.classic.spi.LoggingEvent;

/**
 * Classe utilizzata per verificare che un appender di Logback abbia effettivamente ricevuto
 * una determinata stringa. La verifica viene effettuata sul messaggio formattato
 * @author Mauro
 *
 */
public class ExactMessageMatcher extends ArgumentMatcher<LoggingEvent> {
	
	private String messageToCheck;
	
	/**
	 * Costruttore
	 * @param messageToCheck la stringa da verificare. Viene utilizzata la versione non formattata del messaggio
	 */
    public ExactMessageMatcher(String messageToCheck) {
		super();
		this.messageToCheck = messageToCheck;
	}

	public boolean matches(Object arg) {
		return ((LoggingEvent)arg).getFormattedMessage().equals(this.messageToCheck);
    }
}